<!doctype html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121215869-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121215869-1');
</script>
<script>
;(function(){
  function id(v){return document.getElementById(v); }
  function loadbar() {
    var ovrl = id("overlay"),
        prog = id("progress"),
        stat = id("progstat"),
        img = document.images,
        c = 0;
        tot = img.length;

    function imgLoaded(){
      c += 1;
      var perc = ((100/tot*c) << 0) +"%";
      prog.style.width = perc;
      stat.innerHTML = "Loading "+ perc;
      if(c===tot) return doneLoading();
    }
    function doneLoading(){
      ovrl.style.opacity = 0;
      setTimeout(function(){ 
        ovrl.style.display = "none";
      }, 1200);
    }
    for(var i=0; i<tot; i++) {
      var tImg     = new Image();
      tImg.onload  = imgLoaded;
      tImg.onerror = imgLoaded;
      tImg.src     = img[i].src;
    }    
  }
  document.addEventListener('DOMContentLoaded', loadbar, false);
}());
</script>
<meta charset="utf-8">
<meta name="author" content="YuPa">
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">

<title>YuPa - Travel _ Free</title>
<link rel="canonical" href="https://yupa.asia/" />

<link rel="icon" href="img/favicon.ico"  type="image/x-icon"   />
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link rel="stylesheet" href="css/main.css" type="text/css">
<link rel="stylesheet" href="css/slider-pro.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
<div id="overlay">
 <div class="center-food"><img src="img/loading.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>
<!--- Facebook Plugin --->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0&appId=2101026363514817&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--- End of Facebook --->
<header id="header" class="header header--fixed" role="banner">
  <div class="top-div darkblue-bg width-100 overflow-hidden">
    <div class="left-logo">
      <a href="#"><img src="img/logo.png" class="logo-img img-width-100 "></a>
    </div>
    <div class="menu-link-div3 com2-view" id="top-menu">
     <div class="index-link-div link-div active ow3-menu"><a href="#first" class="white-text a-link">Great ways to travel</a></div>
     <div class="index-link-div link-div ow3-menu"><a href="#second" class="white-text a-link">Yupa Social Media</a></div>
     <div class="index-link-div link-div ow3-menu"><a href="#third" class="white-text a-link">Download Now</a></div>
     <div class="index-link-div link-div link-div-last ow3-menu"><a href="#forth" class="white-text a-link">Contact Us</a></div>
    </div>
    <div class="menu-link-div mobile2-view" id="top-menu2">
     <div class="index-link-div link-div left active ow3-mobile-menu"><a href="#first" class="white-text a-link">Travel</a></div>
     <div class="index-link-div link-div left ow3-mobile-menu"><a href="#second" class="white-text a-link">Social</a></div>
     <div class="index-link-div link-div left ow3-mobile-menu"><a href="#third" class="white-text a-link">Download</a></div>
     <div class="index-link-div link-div left ow3-mobile-menu"><a href="#forth" class="white-text a-link">Contact</a></div>
    </div>    
    <div class="right-app hover-a">
      <div class="mobile-div center"><a href="#"><img src="img/mobile.png" class="mobile-img"></a></div>
       <div class="mobile-text-div white-text center ow3-mobile-text"><a href="#" class="white-text"> Get the App</a></div>
    </div>
  </div>
</header>

<div class="clear"></div>
<div  id="first">
<div id="example1" class="slider-pro com-view" >
  <div class="sp-slides com-view">
    <div class="sp-slide">
    <img src="img/slide1.jpg" class="banner-img">
    <div class="sp-layer sp-white sp-padding red-first-box" data-horizontal="50" data-vertical="50" data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
	  <h1 class="white-h1">Convenient</h1>
      <p class="first-box-text white-text">Wish to manage everything in one app while travelling?</p>
      <a href="#" class="hover-button"><div class="white-button c-font">Try YuPa Now</div></a>
	</div>
    </div>
    <div class="sp-slide">
    <img src="img/slide2.jpg" class="banner-img">
     <div class="sp-layer sp-white sp-padding red-first-box" data-horizontal="50" data-vertical="50" data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
	  <h1 class="white-h1">Smart Itinerary</h1>
      <p class="first-box-text white-text">Hassle-free itinerary planning with YuPa App</p>
      <a href="#" class="hover-button"><div class="white-button c-font">Try YuPa Now</div></a>
	</div>
    
    </div>
    <div class="sp-slide">
    <img src="img/slide3.jpg" class="banner-img">
     <div class="sp-layer sp-white sp-padding red-first-box" data-horizontal="50" data-vertical="50" data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
	  <h1 class="white-h1">All-in-one Travelling Guide</h1>
      <p class="first-box-text white-text">With YuPa you will never miss any tourist attractions near you!</p>
      <a href="#" class="hover-button"><div class="white-button c-font">Try YuPa Now</div></a>
	</div>
    </div>
  </div>
</div>


<div class="clear"></div>
   <div id="slideshow" class="mobile-view">
   <div class="slideshow-div">
     <a href="#"><img src="img/slide1-m.jpg" class="slideshow-img"></a>
   </div>
   <div class="slideshow-div">
     <a href="#"><img src="img/slide2-m.jpg" class="slideshow-img"></a>
   </div>
   <div class="slideshow-div">
     <a href="#"><img src="img/slide3-m.jpg" class="slideshow-img"></a>
   </div>   
   </div>
<div class="clear"></div> 
 
<div></div>
<div class="bg-forest width-100 overflow-hidden">
  <h1 class="darkblue-text h1 bg1-h1 c-font center">Latest Travelling Tips</h1>
  <div class="cms-news-container">
     
    <a href="" class="hover-a">
     <div class="cms-box1">
       <img src="img/cms1-550.jpg" class="img-width-100">
       <div class="cms-transparent-box">
          <p class="cms-box-date">08/05/2018</p>
          <p class="cms-box-title">Top 10 Spots for Viewing the Sunrise</p>
       </div>
     </div>
    </a>
    
    <a href="" class="hover-a">
     <div class="cms-box1">
       <img src="img/cms2-550.jpg" class="img-width-100">
       <div class="cms-transparent-box">
          <p class="cms-box-date">07/05/2018</p>
          <p class="cms-box-title">The Delicious Hidden Mexican Food</p>
       </div>
     </div>
    </a>
    
    <a href="" class="hover-a">
     <div class="cms-box1">
       <img src="img/cms3-550.jpg" class="img-width-100">
       <div class="cms-transparent-box">
          <p class="cms-box-date">06/05/2018</p>
          <p class="cms-box-title">10 Free Stay for Travellers</p>
       </div>
     </div>     
    </a>
    
  </div>
  
    <a href="">
     <div class="view-all-button">View All</div>
    </a>
         
</div>
<div class="bg1-bottom width-100"></div>
<div class="bg2-top width-100"></div>
<div class="bg1 width-100 overflow-hidden">
      <h1 class="darkblue-text h1 bg1-h1 c-font center">What makes us Special?</h1>
     
      <div class="width-60 right r-none r-width img1-div">
          <img src="img/img1.png" class="img-width-100 v1920">
          <img src="img/img1-1500.png" class="img-width-100 v1500">
          <img src="img/img1-800.png" class="img-width-100 v800">
          <img src="img/img1-500.png" class="img-width-100 v500">
      </div>
      <div class="width-40 left r-none r-width center bg1-div1">
           <p class="body-text2 title"><b class="c-font">Travel worry-free</b></p>
           <p class="body-text text-p">Manage your stay, itinerary and transportation all in your hand</p>
       </div>
</div>  
<div class="bg1-bottom width-100"></div>
<div class="bg2-top width-100"></div>
<div class="bg2 width-100 overflow-hidden">
    <div class="width-40 r-none r-width right center bg2-div1">
           <p class="body-text2 c-font title"><b class="c-font">See the best of the world</b></p>
           <p class="body-text text-p">Make sure you don’t miss out any attractive hotspot nearby your destination</p>
      </div>
</div>
<div class="bg1-bottom20 width-100"></div>
<div class="bg2-top20 width-100"></div>
<div class="bg3 width-100 overflow-hidden">
    <div class="width-50 r-none r-width right">
        <img src="img/img2.jpg" class="img-width-100">
    </div>  
    <div class="width-40 left r-none r-width center bg3-div1">
           <p class="body-text2 c-font title"><b class="c-font">Keep up with your schedule</b></p>
           <p class="body-text text-p">Get to know when to depart so you won’t miss out what you’ve planned</p>
    </div>
    <div class="clear"></div>
    <div class="height-100-clear width-100"></div>  
    
    <div class="width-50 r-none r-width left">
        <img src="img/img3.jpg" class="img-width-100">
    </div>
    <div class="width-40 right r-none r-width center bg3-div1 bg3-div2">
           <p class="body-text2 c-font title"><b class="c-font">Need something free?</b></p>
           <p class="body-text text-p">Take up part time job to earn a free stay or meal while travelling</p>
    </div>  
    <div class="clear"></div>  
    <div class="height-100-clear width-100"></div> 
    <div class="width-50 r-none r-width right">
        <img src="img/img4.jpg" class="img-width-100">
    </div>    
    <div class="width-40 r-none r-width left center bg3-div1 bg3-div3">
           <p class="body-text2 c-font title"><b class="c-font">Find a travel buddy</b></p>
           <p class="body-text text-p">Get match with a friendly companion to travel together</p>
    </div>
         
</div>
</div>
<div id="second">
<div class="bg1-bottom20 width-100"></div>
<div class="bg2-top20 width-100"></div>
<div class="bg4 width-100 overflow-hidden">
    <h1 class="darkblue-text h1 bg1-h1 c-font center">Check us out on Instagram and Facebook</h1>
    <div class="width-60 center margin-auto">
          <p class="body-text2 c-font"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><b class="c-font">@hello_yupa</b></a></p>
          <p class="body-text3">Follow us on Instagram for more travelling tips!</p>
    </div>
    <div class="clear"></div>
   <div class="width-full">
      <div class="three-div">
        <div class="insta-small left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta1.jpg" class="img-width-100"></a></div>
        <div class="insta-small left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta2.jpg" class="img-width-100"></div>
        <div class="insta-small left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta3.jpg" class="img-width-100"></div>
        <div class="insta-small left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta4.jpg" class="img-width-100"></div>        
      </div>
        <div class="insta-big left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta-big.jpg" class="img-width-100"></a></div>
      <div class="three-div">        
         <div class="insta-small2 left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank">
          <img src="img/insta5.jpg" class="img-width-100 insta5">
          <img src="img/insta10.jpg" class="img-width-100 insta10">
         </a>
        </div>
        <div class="insta-small2 left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta6.jpg" class="img-width-100"></a></div>
        <div class="insta-small2 left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta7.jpg" class="img-width-100"></a></div>
        <div class="insta-small2 left im1"><a href="https://www.instagram.com/hello_yupa/" class="hover-a" target="_blank"><img src="img/insta8.jpg" class="img-width-100"></a></div>  
       </div> 
     </div> 
    <div class="height-100-clear clear reduce-100"></div>
    <div class="width-60 m80 center margin-auto">
          <p class="body-text2 c-font"><b class="c-font">Facebook</b></p>
          <p class="body-text3">Like our Facebook page for more travelling spot recommendation!</p>
          <!--<img src="img/fb.jpg" class="img-width-80 m100">-->
         <div style="width:100%;">
          <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fyupatravel&tabs=timeline&width=500px&height=750&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2101026363514817" class="fb-iframe" width="500px" height="750" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
              <div class="fb-page mobile-fb"
              data-href="https://www.facebook.com/yupatravel" 
              data-width="340"
              data-hide-cover="false"
              data-show-facepile="true"></div>
    </div>
    </div>
<style>
.mobile-fb{
	display:none;}
@media all and (max-width: 1200px){
.bg4{
	padding-bottom: 50px;
	
	}	
}
@media all and (max-width: 655px){
.fb-iframe{
	display:none;}	
.mobile-fb{
	display:block;}
}
@media all and (max-width: 450px){
.mobile-fb{
	width:200px !important;
	margin: 0 auto;}
}
/*
@media all and (max-width: 525px){
.fb-iframe {
    width: 300px;
    height: 400px;
}
.bg4{
	padding-bottom: 50px;
	}	
}
@media all and (max-width: 400px){
.fb-iframe {
    width: 250px;
    height: 300px;
}
}
@media all and (max-width: 350px){
.fb-iframe {
    width: 200px;
    height: 250px;
}	
}*/
</style>          
   </div>
</div>
<div id="third">
<div class="img-width-100"><a href="#" class="hover-a3"><img src="img/fb-preview.jpg" class="img-width-100"></a></div>
</div> 
<div class="clear"></div>

<div id="forth">
<div class="img-width-100 bg5 overflow-hidden">
    <div class="white-box overflow-hidden">
    <div class="img-width-100 horizontal1"><img src="img/horizontal2.png" class="horizontal-img"></div>
    <div class="vertical"><img src="img/vertical2.png" class="img-height-100"></div>
    <div class="vertical envelope-mview"><img src="img/vertical2.png" class="img-height-100"></div> 
       <div class="left-div">
        <h2 class="darkblue-text h1 c-font text-left no-margin envelope-contactus">Contact Us</h2>
        
        <div class="border-right">
          <img src="img/logo2.png" class="logo2 front">
          <p class="body-text front contact-text form-text">Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.</p>
          <img src="img/address.png" class="phone-img front gps-img">
          <p class="body-text front address-p form-text web-p">No.18, Lorong Aman, Georgetown, 10350 Pulau Pinang</p>
          <span class="body-text front address-p form-text mobile-span">No.18, Lorong Aman, Georgetown, 10350 Pulau Pinang</span>
          <br>
          <img src="img/tel.png" class="phone-img front"><span class="body-text front form-text">04 - 227 0766</span><br>
          <img src="img/mail.png" class="phone-img front"><span class="body-text front form-text">lets.yupa@gmail.com</span>
          <p class="body-text2 c-font connect-p front">Connect with us</p>
          <div class="envelope-social">
           <a class="social-a" href="https://www.facebook.com/yupatravel" target="_blank"><img src="img/fb-icon.png" class="left social-icon front social1"><img src="img/fb-icon2.png" class="left social-icon front social2"></a>
           <a href="https://www.instagram.com/hello_yupa/" class="social-a" target="_blank"><img src="img/insta-icon.png" class="left social-icon front social1">
           <img src="img/insta-icon2.png" class="left social-icon front social2">
           </a>
           <a href="https://www.youtube.com/channel/UCqp9iRDHYwXCN-pBI6-CBvg" target="_blank" class="social-a" href="#"><img src="img/youtube.png" class="left social-icon front social1"><img src="img/youtube2.png" class="left social-icon front social2"></a><br>
          </div>
          <p class="body-text2 c-font google-p connect-p front">Get the App</p>
          <a href="#"><img src="img/googleplay.png" class="googleplay-img front"></a>
          <p class="body-text front"><a href="#" class="darkblue-text darker-hover form-text">View our terms and policies, services agreement</a></p>
          <img src="img/symbol.png" class="sun">
        </div>
       </div>
       
       <div class="right-div left">
            <img src="img/stamp.png" class="stamp-img right">
            <div class="clear envelope-clear"></div>
            <form id="contactform" method="post" action="index.php" class="form-class">
			  <input type="text" name="name" placeholder="Your Name" class="input-name body-text img-width-100 input contact-text2 form-text" ><br>
			  <input type="email" name="email" placeholder="Email" class="input-name body-text img-width-100 input contact-text2 form-text" ><br>
			  <input type="text" name="telephone" placeholder="Contact Number" class="input-name body-text img-width-100 input contact-text2 form-text" ><br>
			  <p class="body-text msg-color contact-text2 form-text">Message:</p>
			  <textarea name="comments" placeholder="Type your message here" class="input-message body-text img-width-100 contact-text2 form-text" ></textarea>
			  <input type="radio" name="contact-option" value="contact-more-info" class="radio1 left" required><p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
			  <input type="radio" name="contact-option" value="contact-on-request" class="radio1 left"  required><p class="opt-msg left"> I just want to be contacted based on my request/ inquiry</p>
			  <br>
			   
			  <input type="submit" name="submit" value="Send" class="input-submit c-font white-text">
			</form>
       </div>
    
       <div class="vertical envelope-hide"><img src="img/vertical2.png" class="img-height-100"></div>
       
       <div class="img-width-100 horizontal1 btm-horizontal"><img src="img/horizontal2.png" class="horizontal-img"></div>
    </div>
</div>
<div class="darkblue-bg white-text center footer2">@ 2018 YuPa, All Rights Reserved.</div></div>
<!---->

  
<style>
#example1, #example2{
	width:100% !important;
	max-width:100% !important;
	overflow:hidden !important;
	height:auto !important;
	max-height:600px !important;
	cursor:url(img/openhand.png) !important;
	margin-top:61px;
}
.slider-pro{
	width:100% !important;
	max-width:100% !important;
	}
#example1:hover, #example2:hover{
	cursor:url(img/openhand.png) !important;
	}
.sp-image-container{
	cursor:url(img/openhand.png) !important;
	}
.sp-image-container:hover{
	cursor:url(img/openhand.png) !important;
	}	
.sp-image{
	cursor:url(img/openhand.png) !important;
	width:100% !important;
	height:auto !important;
	margin:0 !important;}
.sp-image:hover{
	cursor:url(img/openhand.png) !important;}	
	
.sp-mask{
	width:100% !important;
	height:550px !important;}
.sp-image-container{
	width:100% !important;
	height:600px !important;}
.sp-button{
	display:none !important;}


.sp-horizontal .sp-next-arrow {
    right: 3% !Important;}
.sp-horizontal .sp-previous-arrow {
    left: 3% !Important;}

.mobile-view{
	display:none;}
#slideshow, #slideshow2 {

  position: relative;
  width: 100%;
  height:760px;
  margin-top:30px;
  margin-bottom:0;


}
.slideshow-img{
  width: 100%;	
}
#slideshow > div {
  position: absolute;
  width:100% ;

}
#slideshow2 > div {
  position: absolute;
  width:100% ;

}
.red-first-box {
    background-color:rgba(0, 70, 140, 0.4) !important;
    right: 10% !important;
    left: initial !important;
    float: right !important;
	top: 150px !important;
	text-align: center;
	padding-left: 30px;
    padding-right: 30px;
	padding-bottom: 40px;
	width: 480px !important;
	border-radius: 10px;
}
.white-h1 {
    color: white;
	margin-bottom: 10px;
}
.first-box-text {
    font-size: 21px !important;
    font-weight: 300;
    margin-top: 0;
}
.white-button {
    background-color: white;
    width: 300px;
    text-align: center;
    margin: 0 auto;
    border-radius: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    font-size: 27px;
    color: #00468c;
}
.white-button:hover{
	background-color:#00468c !important;
	color:white !important;}
	
	
/****Slick***/
.slider {
        width: 85%;
        margin: 50px auto;
    }

.slick-slide {
      margin: 0px 20px;
    }

.slick-slide img {
      width: 100%;
    }

.slick-prev:before,
.slick-next:before {
      color: black;
    }


.slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
.slick-active {
      opacity: .5;
    }

.slick-current {
      opacity: 1;
    }
.slick-prev {
    left: 25px;
    position: absolute;
    z-index: 9;}
.slick-next {
    right: 25px;
}
.slick-prev:before, .slick-next:before {
    color: white;
}
.slick-next:before {
	content: url(img/right.png) !Important;
	opacity: 0.3;
}
.slick-prev:before {
	content: url(img/left.png) !Important;
	opacity: 0.3;
}

@media all and (max-width: 1600px){
.sp-horizontal .sp-next-arrow {
	margin-top:-50px;
}	
.sp-horizontal .sp-previous-arrow {
	margin-top:-50px;
}

#example1, #example2{
	margin-top: 61px;
	}
.red-first-box{
	top: 100px !important;
	}	
}
@media all and (max-width: 1410px){
.red-first-box{
	top: 80px !important;
	}	
}
@media all and (max-width: 1400px){
.sp-horizontal .sp-next-arrow {
	margin-top:-80px;
}	
.sp-horizontal .sp-previous-arrow {
	margin-top:-80px;
}	
}
@media all and (max-width: 1390px){
.white-h1{
	font-size: 24px;
    margin-top: 5px;
	}
.first-box-text {
    font-size: 18px !important;}
.white-button{
	font-size: 21px;
	width: 250px;
	}
.red-first-box{
	padding-bottom: 25px;
	width: 420px !important;
	right: 7.5% !important;
    top: 100px !important;
}
}
@media all and (max-width: 1270px){
.red-first-box {
    top: 80px !important;
	width: 400px !important;
}	
}
@media all and (max-width: 1120px){
#example1, #example2 {
    margin-top: 58px;
}
}
@media all and (max-width: 1100px){
.sp-horizontal .sp-next-arrow {
	margin-top:-120px;
}	
.sp-horizontal .sp-previous-arrow {
	margin-top:-120px;
}	
.slider{
	margin: 30px auto;
	}
.red-first-box {
    top: 60px !important;
	width: 360px !important;}	
}
@media all and (max-width: 1070px){

#example1, #example2 {
    margin-top: 50px;
}

}
@media all and (max-width: 1025px){

.white-h1 {
    font-size: 21px;
    margin-top: 0px;
}
.first-box-text {
    font-size: 16px !important;
}
.white-button {
    font-size: 18px;}

}

@media all and (max-width: 930px){
.sp-horizontal .sp-next-arrow {
	margin-top:-160px;
}	
.sp-horizontal .sp-previous-arrow {
	margin-top:-160px;
}

}
@media all and (max-width: 900px){
.red-first-box {
    top: 40px !important;
    width: 340px !important;
}	
}
@media all and (max-width: 870px){
.slider {
    width: 90%;
	margin: 20px auto;}
}

@media all and (max-width: 780px){
.red-first-box {
    top: 10px !important;}	
}

@media all and (max-width: 760px){
.com-view{
	display:none;}	
.mobile-view{
	display:block;}	
#slideshow, #slideshow2{
    background-color: #82abd7;}
}
@media all and (max-width: 680px){
#slideshow, #slideshow2{
	height: 700px;
	}
}
@media all and (max-width: 660px){
.slider {
    width: 100%;}
}
@media all and (max-width: 640px){
#slideshow, #slideshow2 {
    height: 640px;
}

}
@media all and (max-width: 580px){
#slideshow, #slideshow2 {
    height: 570px;
}
}
@media all and (max-width: 560px){
.slider{
	margin-bottom:0 !important;}

}
@media all and (max-width: 520px){
#slideshow, #slideshow2 {
    height: 470px;
}

}
@media all and (max-width: 500px){
.slider {
    width: 90%;
}
}
@media all and (max-width: 430px){
#slideshow, #slideshow2 {
    height: 400px;
}

}
@media all and (max-width: 360px){
#slideshow, #slideshow2 {
    height: 320px;
}
}
</style>

<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#00468c;
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>  
<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="js/headroom.js"></script>
<script>
(function() {
    var header = new Headroom(document.querySelector("#header"), {
        tolerance: 5,
        offset : 205,
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
    });
    header.init();

}());
</script>
<script type="text/javascript" src="js/jquery.sliderPro.min.js"></script>
<script type="text/javascript">
	$( document ).ready(function( $ ) {
		$( '#example1' ).sliderPro({
			arrows: true,
			buttons: true,
			waitForLayers: true,
			autoplay: true,
			autoScaleLayers: true,
			breakpoints: {
				500: {
					thumbnailWidth: 120,
					thumbnailHeight: 50
				}
			}
		});
	});
</script>
<script type="text/javascript">
$(document).scroll(function() {

  var scrollTop = $(window).scrollTop();
        if (scrollTop >= 200 ) {
            $('#menu').addClass("empty-view");
        }
        else{
            $('#menu').removeClass("empty-view");
        }
});
</script>
<script type="text/javascript">
$(document).scroll(function() {

  var scrollTop = $(window).scrollTop();
        if (scrollTop >= 200 ) {
            $('#menu2').addClass("no-empty-view2");
        }
        else{
            $('#menu2').removeClass("no-empty-view2");
        }
});
</script>
<script src="js/slick.js" type="text/javascript"></script>
 <script type="text/javascript">
    $(document).on('ready', function() {
	 $(".lazy").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true
      });
    });	
		
</script>
<script>
// Cache selectors
var lastId,
    topMenu = $("#top-menu"),
    topMenuHeight = topMenu.outerHeight(),
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
  $('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 300);
  e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;
   
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";
   
   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href='#"+id+"']").parent().addClass("active");
   }                   
});
</script>
<script>
// Cache selectors
var lastId2,
    topMenu2 = $("#top-menu2"),
    topMenu2Height = topMenu2.outerHeight()+15,
    // All list items
    menuItems2 = topMenu2.find("a"),
    // Anchors corresponding to menu items
    scrollItems2 = menuItems2.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems2.click(function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenu2Height+1;
  $('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 300);
  e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenu2Height;
   
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";
   
   if (lastId2 !== id) {
       lastId2 = id;
       // Set/remove active class
       menuItems2
         .parent().removeClass("active")
         .end().filter("[href='#"+id+"']").parent().addClass("active");
   }                   
});
</script>
<script>
$("#slideshow > div:gt(0)").hide();

setInterval(function() {
  $('#slideshow > div:first')
    .fadeOut(2000)
    .next()
    .fadeIn(2000)
    .end()
    .appendTo('#slideshow');
}, 3000);				
</script>


<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "lets.yupa@gmail.com";
    $email_subject = "User reply from yupa.asia";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone']) ||
        !isset($_POST['comments'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; // required
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 

 
  if(strlen($comments) < 2) {
    $error_message .= 'The message you entered do not appear to be valid.\n';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you for contacting us. We will be in touch with you very soon.")</script>';
header("Location: https://yupa.asia/"); /* Redirect browser */
exit();
?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>
