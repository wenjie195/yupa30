
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
<?php include 'header.php'; ?>
    <meta property="og:url" content="https://yupa.asia/" />
    <meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
    <meta property="og:title" content="YuPa - Edit Profile" />
    <meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
    <meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
    <meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
    <title>YuPa - Edit Profile</title>
    <link rel="canonical" href="https://yupa.asia/" />

    <!-- Add additional services that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>

<!--------------------------------------------------------------------------------------------------------------------------------------->

    <!-- add to document <head> -->
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet">

    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>

    <script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.js"></script>


    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css"
          rel="stylesheet">

    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-crop/dist/filepond-plugin-image-crop.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-transform/dist/filepond-plugin-image-transform.js"></script>

    <script src="js/authUser.js"></script>

    <!--------------------------------------------------------------------------------------------------------------------------------------->
    <style>
        .filepond--file-action-button > svg > path{
            color:white;
        }
        .filepond--file-status-main{
            color:white;
        }
        .filepond--file-status-sub{
            color:white;
        }
        .filepond--file-info-main{
            color:white;
        }
        .filepond--file-info-sub{
            color:white;
        }
        .filepond--panel-root {
            background-color: #fff;
        }
    </style>




</head>

<body>
<?php include 'menu-loggedin.php'; ?>
<div class="grey-bg overflow-hidden">
    <div class="white-bg overflow-hidden">
        <div class="edit-profile-div">

            <div class="edit-profile-div1">
                <img id="profile-image-upload" src="img/profile-pic.jpg" class="img-width-100" type="file" onclick="profile_image_upload();" style="border-radius: 50%">
                <input id="profile-image-upload-input" type="file" style="display: none" accept="image/*">
<!--                <div class="edit-profile-div2">-->
<!--                    <form action="#">-->
<!--                        <div class="file-field input-field pointer edit-profile-input">-->
<!--                            <div class="btn ow-btn pointer darker-bg-hover upload-button" onclick="profile_image_upload();">-->
<!--                                <input id="profile-image-upload-input2" type="file">-->
<!--                                <span class="white-text photo-span pointer">Upload Photo</span>-->
<!--                            </div>-->
<!--                            <div class="file-path-wrapper none">-->
<!--                                <input class="file-path validate" type="text">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </form>-->
<!--                </div>-->
            </div>


            <div class="input-field">
                <input  id="first_name" type="text" class="input-font icon-input" required><img src="img/pencil1.png" class="pencil-position pencil-size pencil1">
                <label for="first_name" class="input-font">Name</label>
                <div id="div1"></div>
            </div>

            <div class="input-field">
                <input  id="email" type="email" class="input-font" required>
                <label for="email" class="input-font">Email</label>
                <div id="div2"></div>
            </div>

            <div class="input-field">
                <input  id="contact_number" type="number" class="input-font" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                <label for="contactnumber" class="input-font">Contact Number</label>
                <div id="div5"></div>
            </div>

            <div class="input-field">
                <select id="gender">
                    <option value="Male" selected>Male</option>
                    <option value="Female">Female</option>
                </select>
                <label>Gender</label>
            </div>
         

            <p class="spacing-p"></p>
            <div class="input-field">
                <input type="date" id="birthday"  placeholder="dd/mm/yyyy" class="icon-input" required><img src="img/cake.png" class="pencil-position pencil-size pencil1">
                <label for="birthday" class="input-font">Birthday</label>
                <div id="div6"></div>
            </div>

            <p class="spacing-p2"></p>
            <div class="input-field">
                <select id="country" name="country" onchange="getState(this.value);" >
                    <?php
                    require_once('dbconn.php');
                    $sql = "SELECT * FROM countries";
                    $result = mysqli_query($conn, $sql);
                    echo "<option disabled selected>Please Select Country</option>";
                    while($row = mysqli_fetch_assoc($result)){
                        ?>
                        <option id="<?php echo $row['id'] ?>" value="<?php echo $row['name'] ?>"><?php echo $row['name'] ?></option><?php } ?>
                </select>
                <label>Country</label>
            </div>

            <p class="spacing-p"></p>
            <div class="input-field">
                <select id="state">
                    <?php
                    echo "<option disabled selected>Please Select State</option>";
                    ?>
                </select>
                <label>State</label>
            </div>

            <p class="label-actor">Hobby</p>
<!--            <div class="chips-autocomplete">-->
                        <select id="hobby" style="display:none" multiple ="">
                            <?php
                            require_once('dbconn.php');
                            $sql = "SELECT * FROM hobby";
                            $result = mysqli_query($conn, $sql);
                            while($row = mysqli_fetch_assoc($result)){?>
                                <option id="<?php echo $row['id'] ?>" value="<?php echo $row['hobby'] ?>"><?php echo $row['hobby'] ?></option><?php } ?>
                        </select>

            <div id="div7"></div>

            <p class="label-actor">Preferred Part-time Job</p>
<!--            <div class="chips-autocomplete2"></div-->
                        <select id="prefer_part_time_job" style="display:none" multiple ="">
                            <?php
                            require_once('dbconn.php');
                            $sql = "SELECT * FROM jobscope";
                            $result = mysqli_query($conn, $sql);
                            while($row = mysqli_fetch_assoc($result)){?>
                                <option id="<?php echo $row['id'] ?>" value="<?php echo $row['job_scope'] ?>"><?php echo $row['job_scope'] ?></option><?php } ?>
                        </select>
            <div id="div8"></div>
        </div>

                    <!--button-->
            <div class="centerise gap">
                <button class="button-a new-div-width" onclick="Submit_btn();" ><a class="waves-effect waves-light btn-large diy-blue-wave confirm-a div-100" href="#" >Submit</a></button></div>
            <p class="centerise logout-p">
                <button class="button-a blue-hover" onclick="logout()"><a class="confirm-a blue-hover">Logout</></button></p>
            <p class="centerise logout-p">
                <button class="button-a blue-hover" onclick="reset_pass()"><a class="confirm-a blue-hover">Reset Password</></button></p>
        </div>
    </div>

<script>
    let jobSelected = [];
    $(document).ready(function()
    {
        var config = {
            apiKey: "AIzaSyClABGbMDQlo5XtA2QxAisefXL9a6-Z8xc",
            authDomain: "yupa-app.firebaseapp.com",
            databaseURL: "https://yupa-app.firebaseio.com",
            storageBucket: "yupa-app.appspot.com",
            messagingSenderId: "25656510649",
            projectId: "yupa-app"
        };
        firebase.initializeApp(config);

    });

    function profile_image_upload(){
        $('#profile-image-upload-input').trigger('click');
        openImageDialog.browse();
    }

    onProfileImageChange();
    function onProfileImageChange(){
        $("input[type=file]").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#profile-image-upload").attr("src",e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
    }

    initializeFilepondPlugin();
    function initializeFilepondPlugin(){
        FilePond.registerPlugin(
            // encodes the file as base64 data
            FilePondPluginFileEncode,

            // validates files based on input type
            FilePondPluginFileValidateType,

            // corrects mobile image orientation
            FilePondPluginImageExifOrientation,

            // previews the image
            FilePondPluginImagePreview,

            // crops the image to a certain aspect ratio
            FilePondPluginImageCrop,

            // resizes the image to fit a certain size
            FilePondPluginImageResize,

            // applies crop and resize information on the client
            FilePondPluginImageTransform
        );

        const pond = FilePond.create(
            document.querySelector('#profile-image-upload-input'),
            {
                labelIdle: `Upload Profile  <span class="filepond--label-action">Browse</span>`,
                imageCropAspectRatio: '1:1',
                imageResizeTargetWidth: 1280,
                imageResizeTargetHeight: 720,
                imageTransformOutputQuality: 70,
                acceptedFileTypes:['image/*'],
                allowImagePreview:false,
                labelFileProcessingError:'Error During Compressing',
                labelFileProcessingAborted:'Compressing Cancelled',
                labelFileProcessing:'Compressing Image',
                labelFileProcessingComplete:'Done Compressing',
                server: {
                    process: (fieldName, file, metadata, load, error, progress, abort) => {
                    console.log(error);
        //Below CODE is use to PREVIEW image
        const image = new Image();
        image.src = URL.createObjectURL(file);
        // document.body.appendChild(image);
        $('#profile-image-upload').attr("src", image.src);
        $('#profile-image-upload-input').attr("src", image.src);
        myImage = file;
        load();
                    }
                }
            }
            );
        //Remove listener
        document.addEventListener('FilePond:removefile', e => {
            $('#profile-image-upload').attr("src", "img/profile-pic.jpg");
        $('#profile-image-upload-input').attr("src", "");
    });

        openImageDialog = pond;
    }



    function getState(){
        var countryId = $('#country option:selected').attr("id");
        clearState();
        $.ajax({
            type: "POST",
            url: "SearchState.php",
            data: {countryId : countryId},
            success :  function(data){
                console.log(data);
                var result = jQuery.parseJSON(data);
                for(var i = 0; i<result.length;i++){
                    console.log(result[i]);
                    $('#state').append('<option value="'+result[i]+'">'+result[i]+'</option>');
                }
                initiateSelect();
                //append state list
            },
            error: function(error){console.log(error);
            }
        });
    }
            // Different select option, this is a plugin, so after append the option to the select --
            // need to call this function again to initiate, else after append option, the option not show in select
            function initiateSelect(){
                document.addEventListener('DOMContentLoaded', function() {
                    var elems = document.querySelectorAll('select');
                    var instances = M.FormSelect.init(elems, options);
                });
                // Or with jQuery
                $(document).ready(function(){
                    $('select').formSelect();
                });
            }
            function clearState() {
                $('#state')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option disabled selected>Please Select State</option>');
            }

            function logout() {
                firebase.auth().signOut().then(function() {
                alert("Logged Out");
                window.location = 'login.php';
                }).catch(function(error) {
                    alert(error);
                });
            }
            function reset_pass(){
                const email = document.getElementById("email").value;
                var auth = firebase.auth();
                auth.sendPasswordResetEmail(email).then(function() {
                    alert("Reset Email Password have been sent to your Email");
                    window.location = 'login.php';
                }).catch(function(error) {
                    alert("Password are needed");
                });
            }

            $("#hobby").on('change', function() {
                console.log($('#hobby').val());
//                 re-initialize material-select
//                $('#hobby').material_select();
            });

            $("#prefer_part_time_job").on('change', function() {
                jobSelected = $('#prefer_part_time_job').val() ;
                console.log($('#prefer_part_time_job').val());
//                 re-initialize material-select
//                $('#prefer_part_time_job').material_select();

            });



            function Submit_btn() {
                var jobObject = {};
                for(let i = 0; jobSelected.length;i++){
                    jobObject[jobSelected[i]] = true;
                }
                var db;
                db = firebase.firestore();
                db.settings({timestampsInSnapshots: true});

                const name = document.getElementById("first_name").value;
                const email = document.getElementById("email").value;
                const contact_number = document.getElementById("contact_number").value;
                const gender = document.getElementById("gender").value;
                const birthday = document.getElementById("birthday").value;
                var saperateValue = birthday.split("-");
                const country = document.getElementById("country").value;
                const state = document.getElementById("state").value;
                const hobby = document.getElementById("hobby").value;
                const prefer_part_time_job = document.getElementById("prefer_part_time_job").value;
//              misake pg me
                $('#hobby').click(function () {
                    var selected_hobby_Val = $('#hobby li:selected').val();
                    console.log(selected_hobby_Val);
                });

                $('#prefer_part_time_job').click(function () {
                    var selected_job_Val = $('#prefer_part_time_job li:selected').val();
                    console.log(selected_job_Val);
                });


                var complete = true;

                if (name === "") {
                    document.getElementById("div1").innerHTML = "Enter your username*";
                    document.getElementById("first_name").style.borderColor = "red";
                    document.getElementById("div1").style.color = "red";
                } else {
                    document.getElementById("div1").style.display = "none";
                    document.getElementById("first_name").style.borderColor = "black";
                }

                if (email === "") {
                    document.getElementById("div2").innerHTML = "Enter your email*";
                    document.getElementById("email").style.borderColor = "red";
                    document.getElementById("div2").style.color = "red";
                } else {
                    document.getElementById("div2").style.display = "none";
                    document.getElementById("email").style.borderColor = "black";
                }

                if (contact_number === "") {
                    document.getElementById("div5").innerHTML = "Enter your contact number*";
                    document.getElementById("contact_number").style.borderColor = "red";
                    document.getElementById("div5").style.color = "red";
                } else if (contact_number.length < 10) {
                    document.getElementById("div5").innerHTML = "Contact number at least must have 10 numbers!*";
                    document.getElementById("contact_number").style.borderColor = "red";
                    document.getElementById("div5").style.color = "red";
                } else {
                    document.getElementById("div5").style.display = "none";
                    document.getElementById("contact_number").style.borderColor = "black";
                }

                if (birthday === "") {
                    document.getElementById("div6").innerHTML = "Enter your birthday date*";
                    document.getElementById("birthday").style.borderColor = "red";
                    document.getElementById("div6").style.color = "red";
                } else {
                    document.getElementById("div6").style.display = "none";
                    document.getElementById("birthday").style.borderColor = "black";
                }
//...............................................................................................................
                //optional!!!!
//                if (hobby === "") {
//                    document.getElementById("div7").innerHTML = "Select your hobby*";
//                    document.getElementById("hobby").style.borderColor = "red";
//                    document.getElementById("div7").style.color = "red";
//                } else {
//                    document.getElementById("div7").style.display = "none";
//                    document.getElementById("hobby").style.borderColor = "black";
//                }
//
//                if (prefer_part_time_job === "") {
//                    document.getElementById("div8").innerHTML = "Pick your prefer part time job*";
//                    document.getElementById("prefer_part_time_job").style.borderColor = "red";
//                    document.getElementById("div8").style.color = "red";
//
//                } else {
//                    document.getElementById("div8").style.display = "none";
//                    document.getElementById("prefer_part_time_job").style.borderColor = "black";
//                }
//...............................................................................................................
                if (complete) {

                    $.ajax({
                        type: "POST",
                        url: "check_country.php",
                        data: {country_name : country},
                        dataType:"json",
                        success :  function(data){
                            console.log(data);
                            if(data === "Exist"){
//                               alert("country Yes");

                                //****************add need ajax for check_state*********************//
                                $.ajax({
                                    type: "POST",
                                    url: "check_state.php",
                                    data: {state_name : state},
                                    dataType: "json",
                                    success: function(data){
                                        console.log(data);
                                        if(data === "Exist"){
//                                           alert("state Yes");
                                            storeUserDetailToFirestore(db,name,email,contact_number,gender,saperateValue[2],saperateValue[1],saperateValue[0],country,state,hobby,prefer_part_time_job);
                                        }else{
                                            alert("state no");
                                        }
                                    }
                                });
                                //*****************************************************************//
                            }else{
                                alert("1no");
                            }
                        },
                        error: function(error){console.log(error);
                        }
                    });
                }
                //361755

                    function storeUserDetailToFirestore(db,name,email,contact_number,gender,dobDay,dobMonth,dobYear,country,state,hobby,prefer_part_time_job){
                        firebase.auth().onAuthStateChanged(function (isUserExist) {
                            var user;
                            if (!isUserExist) {
                                console.log("go to login page");
                                alert("User is not signed in");
                            } else {
                                user = firebase.auth().currentUser;

//                                console.log(user.uid);
//                                alert(user.uid);
//                                console.log("User is signed in");
//                                alert("sign-in");

//                                firebase.auth().currentUser.sendEmailVerification().then(function () {
//                                    var actionCodeSettings = {
//                                        url: 'https://yupa-app.firebaseio.com',
//                                        handleCodeInApp: true,
//                                        iOS: {
////                                bundleId: 'com.example.ios'
//                                            bundleId: 'com.apple.mobilemail'
//                                        },
//                                        android: {
//                                            packageName: 'com.vidatechft.yupa',
//                                            installApp: true,
//                                            minimumVersion: '12'
//                                        }
//                                    };
//                                    firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings).then(function () {
//                                        window.localStorage.setItem('emailForSignIn', email);
//                                    })
//                                        .catch(function (error) {
//                                    });
//                                });
                               if(name !== null)
                                db.collection("tester").doc(user.uid).update(
                                    {
                                        name: name,
                                        email: email,
                                        contactNo: parseInt(contact_number),
                                        gender: gender,
                                        dobDay: parseInt(dobDay),
                                        dobMonth: parseInt(dobMonth),
                                        dobYear: parseInt(dobYear),
                                        country: country,
                                        state: state,
                                        hobby: hobby,
                                        prefer_part_time_job: prefer_part_time_job,
                                        id: user.uid,
                                        dateUpdated: firebase.firestore.FieldValue.serverTimestamp()
                                    })
                                    .then(function () {
                                        console.log("Document successfully updated!");
                                        window.location = 'profile.php';

//                                        if (user) {
//                                            user = firebase.auth().currentUser;
//                                            firebase.auth().signOut().then(function() {
//                                                alert("Account have been Log-Out");
//                                                alert("Please Check your Email for verify to Active Your Account");
//                                                window.location = 'profile.php';
//                                            }).catch(function(error) {
//                                                alert(error);
//                                            });
//                                        }
                                    })
                                    .catch(function (error) {
                                        console.error("Error writing document: ", error);
                                    });
                                alert('Enter all content requirement!!!');
                            }
                        });
//                        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
//                            var errorCode = error.code;
//                            var errorMessage = error.message;
//                            if (errorCode === 'auth/weak-password') {
//                                alert('The password is too weak.');
//                            } else {
//                                alert(errorMessage);
//                            }
//                        });
                    }
                }
//                    var user;
//                    firebase.auth().onAuthStateChanged(function (isUserExist) {
//                        if (!isUserExist) {
//                            console.log("go to login page");
//                            alert("User is not signed in");
//                        } else {
//                            console.log("User is signed in");
//                            firebase.auth().currentUser.sendEmailVerification().then(function () {
//                                var actionCodeSettings = {
//                                    url: 'https://yupa-app.firebaseio.com',
//                                    handleCodeInApp: true,
//                                    iOS: {
////                                        bundleId: 'com.example.ios'
//                                        bundleId: 'com.apple.mobilemail'
//                                    },
//                                    android: {
//                                        packageName: 'com.vidatechft.yupa',
//                                        installApp: true,
//                                        minimumVersion: '12'
//                                    }
//                                };
//                                firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings).then(function () {
//                                    window.localStorage.setItem('emailForSignIn', email);
//                                })
//                                    .catch(function (error) {
//
//                                    });
//                            });
//                            db.collection("tester").doc(user.uid).update({
//                                    name: name,
//                                    email: email,
//                                    contactNo: contact_number,
//                                    gender: gender,
//                                    dobDay: saperateValue[2],
//                                    dobMonth: saperateValue[1],
//                                    dobYear: saperateValue[0],
//                                    country: country,
//                                    state: state,
//                                    hobby: hobby,
//                                    prefer_part_time_job: prefer_part_time_job,
//                                    id: user.uid,
//                                    dateUpdated: firebase.firestore.FieldValue.serverTimestamp()
//                                })
//                        .then(function () {
//                            console.log("Document successfully updated!");
//                            window.location = 'profile.php';
//                        })
//                        .catch(function (error) {
//                            console.error("Error writing document: ", error);
//                        });
//                    alert('Please verify your E-mail! Verification code have Sent!');
//                }
//            });
//            firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
//                var errorCode = error.code;
//                var errorMessage = error.message;
//                if (errorCode === 'auth/weak-password') {
//                    alert('The password is too weak.');
//                } else {
//                    alert(errorMessage);
//                }
//            });
//        }


//        document.getElementById('hobby').innerHTML = location.search;
//        $(".chosen_select").chosen();

    //To check the current user is admin or not, put at the last paragraph will be better
    authAdmin();

</script>

<?php include 'footer.php'; ?>
</body>
</html>
