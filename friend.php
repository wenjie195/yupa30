
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa -Friend List" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Friend List</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-loggedin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">

<!----Hide this div if this is not from the user's own view --->   
    <div class="new-friend-request-div">
     <p class="friend-title">New Friend Request</p>
     <div class="profile-div">
    
         <div class="profile-div1">
          <a href="profile.php" class="hover-a">
           <img src="img/rebecca.jpg" class="img-width-100">
         </a>
      
         </div>
         <div class="profile-div2">
           <p class="username-p2 hover-turn-blue"><a href="profile.php"><span class="username-p2 hover-turn-blue">Rebecca Ave</span></a></p>
           <p class="profile-info friend-info hover-a"><a href="profile.php"><span class="profile-info friend-info hover-a">Live in London, United Kingdom</span></a></p>
           <p class="profile-info friend-info"><a href="friend.php" class="blue-hover">200 Friends</a></p>
         </div>
           <button class="button-a accept-button"><div class="add-friend-button pointer darker-bg-hover accept-div"><span class="add-friend-span">Approve</span></div></button>
       
           <button class="button-a"><div class="add-friend-button pointer outline-div decline-div"><span class="add-friend-span unfriend-span">Reject</span></div></button>
         
     </div>

     <div class="profile-div">
    
         <div class="profile-div1">
          <a href="profile.php" class="hover-a">
           <img src="img/rebecca.jpg" class="img-width-100">
          </a>
      
         </div>
         <div class="profile-div2">
           <p class="username-p2 hover-turn-blue"><a href="profile.php"><span class="username-p2 hover-turn-blue">Rebecca Ave</span></a></p>
           <p class="profile-info friend-info hover-a"><a href="profile.php"><span class="profile-info friend-info hover-a">Live in London, United Kingdom</span></a></p>
           <p class="profile-info friend-info"><a href="friend.php" class="blue-hover">200 Friends</a></p>
         </div>
           <button class="button-a accept-button"><div class="add-friend-button pointer darker-bg-hover accept-div"><span class="add-friend-span">Approve</span></div></button>
       
           <button class="button-a"><div class="add-friend-button pointer outline-div decline-div"><span class="add-friend-span unfriend-span">Reject</span></div></button>
         
     </div>
   
   
   </div>
<!---- --->
   <p class="friend-title friend-list">Friend List</p>  
   <!----Hide this input and img if this is not from the user's own view --->   
   <input type="text" class="friend-search-input search-bar-padding" placeholder="Search Friend"><button class="button-a search-button-a"><img src="img/friend-search.png" class="friend-search-icon pointer hover-a"></button>
   <!---- --->
     <div class="profile-div">
    
         <div class="profile-div1">
          <a href="profile.php" class="hover-a">
           <img src="img/rebecca.jpg" class="img-width-100">
         </a>
      
         </div>
         <div class="profile-div2">
           <p class="username-p2 hover-turn-blue"><a href="profile.php"><span class="username-p2 hover-turn-blue">Rebecca Ave</span></a></p>
           <p class="profile-info friend-info hover-a"><a href="profile.php"><span class="profile-info friend-info hover-a">Live in London, United Kingdom</span></a></p>
           <p class="profile-info friend-info"><a href="friend.php" class="blue-hover">200 Friends</a></p>
         </div>
         <a href="#modal3" class="modal-trigger"><div class="add-friend-button pointer outline-div third-div"><button class="button-a"><img src="img/unfriend-blue.png" class="friend-icon unfriend1 friendlist-un"><img src="img/unfriend.png" class="friend-icon unfriend2 friendlist-un"> <span class="add-friend-span unfriend-span">Unfriend</span></button></div></a>

         
     </div>   

     <div class="profile-div">
    
         <div class="profile-div1">
          <a href="profile.php" class="hover-a">
           <img src="img/rebecca.jpg" class="img-width-100">
          </a>

      
         </div>
         <div class="profile-div2">
           <p class="username-p2 hover-turn-blue"><a href="profile.php"><span class="username-p2 hover-turn-blue">Rebecca Ave</span></a></p>
           <p class="profile-info friend-info hover-a"><a href="profile.php"><span class="profile-info friend-info hover-a">Live in London, United Kingdom</span></a></p>
           <p class="profile-info friend-info"><a href="friend.php" class="blue-hover">200 Friends</a></p>
         </div>
         <a href="#modal3" class="modal-trigger"><div class="add-friend-button pointer outline-div third-div"><button class="button-a"><img src="img/unfriend-blue.png" class="friend-icon unfriend1 friendlist-un"><img src="img/unfriend.png" class="friend-icon unfriend2 friendlist-un"> <span class="add-friend-span unfriend-span">Unfriend</span></button></div></a>

         
     </div>      
   
  </div>
</div>


<!-- Modal Structure of Confirming Unfriend-->
<div id="modal3" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title unfriend-title">Are you sure want to unfriend with this user?</h4>
        <p>
    

     
     <div class="centerise"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-line-wave div-100 text-transform-none" href="#">Yes</button></a></button></div> 
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-wave modal-close new-div-width" href="#">No</a></div> 
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
</div>

<?php include 'footer.php'; ?>
</body>
</html>
