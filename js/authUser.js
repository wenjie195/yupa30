let displayName, email, emailVerified, photoURL, isAnonymous, uid, providerData, isAdmin;
// A $( document ).ready() block.

firebase.auth().onAuthStateChanged(function (user) {

    if (user) {
        // User is signed in.
        displayName = user.displayName;
        email = user.email;
        emailVerified = user.emailVerified;
        photoURL = user.photoURL;
        isAnonymous = user.isAnonymous;
        uid = user.uid;
        providerData = user.providerData;

        $('#login').css("display", "none");
        $('#logout').css("display", "block");

        var userRef = db.collection("user").doc(uid);
        userRef.get().then(function (doc) {
            if (doc.exists) {
                console.log("Document Exist" + " Admin = " + doc.data().isAdmin + " All Data = " + doc.data());
                isAdmin = doc.data().isAdmin;
            }
            else {
                console.log("Document Not Exist");

            }
        }).catch(function (error) {
            console.log("Get user document error : ", error);
        });


    } else {
        $.alert({
            title: 'Alert!',
            content: 'Please log in your account!',
            buttons: {
                Ok: function () {
                    window.location = 'login.php';
                }
            },
        });

    }
});


function authAdmin() {
    let authAdminInterval = setInterval(function () {
        if (!uid) {
            return;
        }
        let userRef = db.collection("user").doc(uid);
        userRef.get().then(function (doc) {
            if (doc.exists) {
                isAdmin = doc.data().isAdmin;
            }
            else {
                isAdmin = false;
            }

            clearInterval(authAdminInterval);

            if(!isAdmin){
                $.alert({
                    title: 'Alert!',
                    content: 'You are not admin! You need permission to access this page',
                    buttons: {
                        Ok: function () {
                            window.history.back();
                        }
                    },
                });
            }
        }).catch(function (error) {
            console.log("Get user document error : ", error);
        });

    }, 1000);

}



