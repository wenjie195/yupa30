
function imageModal(imageSrc, imageId){

    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImage'+ imageId);
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    console.log(imageId);
    console.log(imageSrc);
    modal.style.display = "block";
    $('body').css({
        overflow: 'hidden'
    });
    modalImg.src = imageSrc;
    // captionText.innerHTML = this.alt;

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
        $('body').css({
            overflow: 'auto'
        });
    }
}


// logoutUser();
function approveUser(myObject){
    var db = firebase.firestore();

    // Disable deprecated features
    db.settings({
        timestampsInSnapshots: true
    });
    // console.log(myObject.merchantId);
    // console.log(myObject);


    $.confirm({
        title: 'Alert!',
        content: 'Are you sure want to approve <b>'+name+'</b> ?',
        buttons: {
            confirm: function () {
                var documentId = myObject.id;
                var userData;
                if(myObject.type === "merchantShop"){
                    if(myObject.merchantId !== 'undefined'){
                        documentId = myObject.merchantId;
                    }
                }
                else{
                    documentId = myObject.id;
                }
                var approveUserRef;
                if(myObject.type === "bookHomestay"){
                    console.log("Type : " + myObject.type + " Document Id : " + documentId);
                    approveUserRef = db.collection(myObject.type).doc(documentId)
                }
                else{
                    approveUserRef = db.collection("user").doc(documentId)
                }

                if(myObject.type === "host"){
                    userData = {
                        isHost : true
                    };
                }

                if(myObject.type === "employer"){
                    userData = {
                        isEmployer : true
                    };
                }

                if(myObject.type === "merchantShop"){
                    userData = {
                        isMerchant : true
                    };
                }

                if(myObject.type === "bookHomestay"){
                    userData = {
                        actionType : "accept"
                    };
                }

                return approveUserRef.update(
                    userData
                )
                    .then(function() {
                        openSnackbar("Successful Approve!");
                        console.log("Document successfully updated!");
                        if(myObject.type === "bookHomestay"){
                            $("."+documentId).remove();
                            return false;
                        }
                        if(myObject.type === "merchantShop"){
                            documentId = myObject.id;
                        }
                        // else{
                        //     documentId = myObject.id;
                        // }
                        var updateIsReadByUser = db.collection(myObject.type).doc(documentId);

                        return updateIsReadByUser.update({
                            isReadByUser: false,
                            isRejected: null,
                            isApproved:true
                        })
                            .then(function() {
                                $("."+documentId).remove();
                            })
                            .catch(function(error) {
                                // The document probably doesn't exist.
                                console.error("Error updating document: ", error);
                            });
                    })
                    .catch(function(error) {
                        // The document probably doesn't exist.
                        console.error("Error updating document: ", error);
                    });
            },
            cancel: function () {
            }
        }
    });

}

function rejectUser(myObject){
    var db = firebase.firestore();

    // Disable deprecated features
    db.settings({
        timestampsInSnapshots: true
    });

    $.confirm({
        title: 'Alert!',
        content: 'Are you sure want to reject <b>'+myObject.name+'</b> ?',
        buttons: {
            confirm: function () {
                $.confirm({
                    title: 'Remark',
                    content: '' +
                    '<div class="form-group">' +
                    '<label>Enter something here</label>' +
                    '<input id="remark" type="text" placeholder="Enter Remark" class="name form-control" required autofocus/>' +
                    '</div>',

                    buttons: {
                        confirm: function(){
                            var remark = this.$content.find('#remark').val();
                            var rejectUserRef = db.collection(myObject.type).doc(myObject.id);

                            var userData;
                            if(myObject.type === "host"){
                                userData = {
                                    isReadByUser: false,
                                    isApproved: null,
                                    isRejected: true,
                                    remark:remark
                                };
                            }

                            if(myObject.type === "employer"){
                                userData = {
                                    isReadByUser: false,
                                    isApproved: null,
                                    isRejected: true,
                                    remark:remark
                                };
                            }

                            if(myObject.type === "merchantShop"){
                                userData = {
                                    isReadByUser: false,
                                    isApproved: null,
                                    isRejected: true,
                                    remark:remark
                                };
                            }

                            if(myObject.type === "bookHomestay"){
                                userData = {
                                    actionType:"reject",
                                    remark:remark
                                };
                            }

                            return rejectUserRef.update(
                                userData
                            )
                                .then(function() {
                                    openSnackbar("Successful Reject!");
                                    $("."+myObject.id).remove();
                                    console.log("Document successfully updated!");
                                })
                                .catch(function(error) {
                                    // The document probably doesn't exist.
                                    console.error("Error updating document: ", error);
                                });

                        },
                        cancel: function () {
                            //close
                        }
                    }
                });
                // $('#remark').focus();
                this.$content.find("remark").focus();
            },
            cancel: function () {
            }
        }
    });
}

function logoutUser(){
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
    }).catch(function(error) {
        // An error happened.
    });
}

function getUserDetail(type){
    // var config = {
    //     apiKey: 'AIzaSyClABGbMDQlo5XtA2QxAisefXL9a6-Z8xc',
    //     authDomain: 'yupa-app.firebaseapp.com',
    //     databaseURL: "https://yupa-app.firebaseio.com",
    //     storageBucket: "yupa-app.appspot.com",
    //     messagingSenderId: "25656510649",
    //     projectId: 'yupa-app'
    // };
    // firebase.initializeApp(config);
    // // Initialize Cloud Firestore through Firebase
    // var db = firebase.firestore();
    //
    // // Disable deprecated features
    // db.settings({
    //     timestampsInSnapshots: true
    // });

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log("User is login");
            var host = "host";
            var employer = "employer";
            var merchant = "merchantShop";
            var homestay = "bookHomestay";
            const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            var docRef;
            if(type === host){
                docRef = db.collection(type).where("isApproved", "==", false);
                docRef.onSnapshot(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        if(querySnapshot != null){
                            if(!querySnapshot.isEmpty){
                                var id = doc.id;
                                var name = doc.data().name;
                                var email = doc.data().email;
                                var nric = doc.data().nric;
                                var licenseUrl = doc.data().hostLicensePicUrl;
                                var contactNo = doc.data().contactNo;
                                var dateCreated = doc.data().dateCreated.toDate();
                                var getYear = dateCreated.getUTCFullYear();
                                var getMonth = monthNames[dateCreated.getUTCMonth()];
                                var getDay = dateCreated.getUTCDate();

                                const myObject = {
                                    id : id,
                                    name : name,
                                    type : type
                                };

                                if(licenseUrl != null && licenseUrl !== ''){

                                }
                                else{
                                    licenseUrl = 'img/ic_upload_placeholder.png';
                                }

                                $("."+id).remove();

                                $('#main-container-'+type).append(
                                    '<div class="card '+id+'" id="'+id+'">'+
                                    '<img class="myImage" id="myImage'+id+'" src="'+licenseUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                    '<div class="card-container">'+
                                    '<h4><b>'+name+'</b></h4>'+
                                    '<p><b>Email: </b>'+email+'</p>'+
                                    '<p><b>Passport:</b> '+nric+'</p>'+
                                    '<p><b>Contact No:</b> '+contactNo+'</p>'+
                                    '<p><b>Date Created:</b> '+getDay+"/"+getMonth+"/"+getYear+'</p>'+
                                    '</div>'+
                                    '<div>' +
                                    '<button id="btn-accept'+id+'" class="btn-accept" onclick="approveUser();">Accept</button>' +
                                    '<button id="btn-reject'+id+'" class="btn-reject" onclick="rejectUser();">Reject</button>' +
                                    '</div>'+
                                    '</div>');

                                $('#btn-accept'+id).on('click', () => approveUser(myObject));
                                $('#btn-reject'+id).on('click', () => rejectUser(myObject));

                            }
                        }
                    });

                }, function (error) {
                    console.log("Error : " + error);
                });
            }
            if(type === employer){
                docRef = db.collection(type).where("isApproved", "==", false);
                docRef.onSnapshot(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        if(querySnapshot != null){
                            if(!querySnapshot.isEmpty){
                                var id = doc.id;
                                var name = doc.data().name;
                                var email = doc.data().email;
                                var nric = doc.data().nric;
                                var licenseUrl = doc.data().licenseUrl;
                                var contactNo = doc.data().contactNo;
                                var dateCreated = new Date(doc.data().dateCreated.toDate());
                                var getYear = dateCreated.getUTCFullYear();
                                var getMonth = monthNames[dateCreated.getUTCMonth()];
                                var getDay = dateCreated.getUTCDate();

                                const myObject = {
                                    id : id,
                                    name : name,
                                    type : type
                                };

                                if(licenseUrl != null && licenseUrl !== ''){

                                }
                                else{
                                    licenseUrl = 'img/ic_upload_placeholder.png';
                                }

                                $("."+id).remove();

                                $('#main-container-'+type).append(
                                    '<div class="card '+id+'" id="'+id+'">'+
                                    '<img class="myImage" id="myImage'+id+'" src="'+licenseUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                    '<div class="card-container">'+
                                    '<h4><b>'+name+'</b></h4>'+
                                    '<p><b>Email: </b>'+email+'</p>'+
                                    '<p><b>Passport:</b> '+nric+'</p>'+
                                    '<p><b>Contact No:</b> '+contactNo+'</p>'+
                                    '<p><b>Date Created:</b> '+getDay+"/"+getMonth+"/"+getYear+'</p>'+
                                    '</div>'+
                                    '<div>' +
                                    '<button id="btn-accept'+id+'" class="btn-accept" onclick="approveUser();">Accept</button>' +
                                    '<button id="btn-reject'+id+'" class="btn-reject" onclick="rejectUser();">Reject</button>' +
                                    '</div>'+
                                    '</div>');

                                $('#btn-accept'+id).on('click', () => approveUser(myObject));
                                $('#btn-reject'+id).on('click', () => rejectUser(myObject));
                            }
                        }
                    });

                }, function (error) {
                    console.log("Error : " + error);
                });
            }

            if(type === merchant){
                docRef = db.collection(type).where("isPublish", "==", false);
                docRef.onSnapshot(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        db.collection(type).doc(doc.id).collection("proof").doc("proof").onSnapshot(function(snap){
                            if(snap != null){
                                console.log("snap not null");
                                if(!snap.isEmpty){
                                    console.log("snap not empty");
                                    console.log(snap.data().merchantLicensePicUrl);
                                }

                                if(querySnapshot != null){
                                    console.log("Snapshot not null ");
                                    if(!querySnapshot.isEmpty){
                                        console.log("Snapshot not empty ");

                                        var id = doc.id;
                                        var merchantId = doc.data().merchantId;
                                        var name = doc.data().name;
                                        var email = doc.data().email;
                                        var nric = doc.data().nric;
                                        var merchantLicensePicUrl = snap.data().merchantLicensePicUrl;
                                        var contactNo = doc.data().contactNo;
                                        var dateCreated = new Date(doc.data().dateCreated.toDate());
                                        var getYear = dateCreated.getUTCFullYear();
                                        var getMonth = monthNames[dateCreated.getUTCMonth()];
                                        var getDay = dateCreated.getUTCDate();

                                        const myObject = {
                                            id : id,
                                            merchantId : merchantId,
                                            name : name,
                                            type : type
                                        };

                                        if(merchantLicensePicUrl != null && merchantLicensePicUrl !== ''){

                                        }
                                        else{
                                            merchantLicensePicUrl = 'img/ic_upload_placeholder.png';
                                        }

                                        $("."+id).remove();

                                        // const div = $(`
                                        //     <div class="card +id" id="id">
                                        //     <img class="myImage" id="myImage'+id" src="+licenseUrl+" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,id);">
                                        //     <div class="card-container">
                                        //     <h4><b>name</b></h4>
                                        //     <p><b>Email: </b>.email.</p>
                                        //     <p><b>Passport:</b> nric</p>
                                        //     <p><b>Contact No:</b> contactNo</p>
                                        //     <p><b>Date Created:</b>+getDay+"/"+getMonth+"/"+getYear+</p>
                                        //     </div>
                                        //     <div>
                                        //     <button id="btn-accept" class="btn-accept" onclick="approveUser();">Accept</button>' +
                                        //     <button id="btn-reject" class="btn-reject" onclick="rejectUser(\\''+id+'\\',\\''+name+'\\',\\''+type+'\\');">Reject</button>' +
                                        //     </div>
                                        //     </div>
                                        //     `);
                                        //
                                        // div.find('#btn-accept').on('click', () => approveUser(myObject));
                                        // $('#main-container-'+type).append(div);


                                        $('#main-container-'+type).append(
                                            '<div class="card '+id+'" id="'+id+'">'+
                                            '<img class="myImage" id="myImage'+id+'" src="'+merchantLicensePicUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                            '<div class="card-container">'+
                                            '<h4><b>'+name+'</b></h4>'+
                                            '<p><b>Email: </b>'+email+'</p>'+
                                            '<p><b>Passport:</b> '+nric+'</p>'+
                                            '<p><b>Contact No:</b> '+contactNo+'</p>'+
                                            '<p><b>Date Created:</b> '+getDay+"/"+getMonth+"/"+getYear+'</p>'+
                                            '</div>'+
                                            '<div>' +
                                            '<button id="btn-accept'+id+'" class="btn-accept" onclick="approveUser()">Accept</button>' +
                                            '<button id="btn-reject'+id+'" class="btn-reject" onclick="rejectUser();">Reject</button>' +
                                            '</div>'+
                                            '</div>');

                                        $('#btn-accept'+id).on('click', () => approveUser(myObject));
                                        $('#btn-reject'+id).on('click', () => rejectUser(myObject));

                                    }
                                    else{
                                        console.log("Snapshot empty ");
                                    }
                                }
                                else{
                                    console.log("Snapshot  null ");
                                }
                            }
                        });
                    });

                }, function (error) {
                    console.log("Error : " + error);
                });
            }

            if(type === homestay){
                docRef = db.collection(type);
                docRef.onSnapshot(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        if(querySnapshot != null){
                            if(!querySnapshot.isEmpty){
                                var id = doc.id;
                                var firstName = doc.data().firstName;
                                var lastName = doc.data().lastName;
                                var contactNo = doc.data().contactNo;
                                var country = doc.data().country;
                                var dateAction = doc.data().dateAction.toDate();
                                var licenseUrl = doc.data().licenseUrl;
                                var purpose = doc.data().purpose;
                                var remark = doc.data().remark;
                                var dateCreated = doc.data().dateCreated.toDate();
                                var getYear = dateCreated.getUTCFullYear();
                                var getMonth = monthNames[dateCreated.getUTCMonth()];
                                var getDay = dateCreated.getUTCDate();

                                const myObject = {
                                    id : id,
                                    type : type
                                };

                                if(licenseUrl != null && licenseUrl !== ''){

                                }
                                else{
                                    licenseUrl = 'img/ic_upload_placeholder.png';
                                }

                                $("."+id).remove();

                                $('#main-container-'+type).append(
                                    '<div class="card '+id+'" id="'+id+'">'+
                                    '<img class="myImage" id="myImage'+id+'" src="'+licenseUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                    '<div class="card-container">'+
                                    '<h4><b>'+name+'</b></h4>'+
                                    '<p><b>First Name: </b>'+firstName+'</p>'+
                                    '<p><b>Last Name:</b> '+lastName+'</p>'+
                                    '<p><b>Country:</b> '+country+'</p>'+
                                    '<p><b>Date Action:</b> '+dateAction+'</p>'+
                                    '<p><b>Purpose:</b> '+purpose+'</p>'+
                                    '<p><b>Remark:</b> '+remark+'</p>'+
                                    '<p><b>Contact No:</b> '+contactNo+'</p>'+
                                    '<p><b>Date Created:</b> '+getDay+"/"+getMonth+"/"+getYear+'</p>'+
                                    '</div>'+
                                    '<div>' +
                                    '<button id="btn-accept'+id+'" class="btn-accept" onclick="">Accept</button>' +
                                    '<button id="btn-reject'+id+'" class="btn-reject" onclick="rejectUser();">Reject</button>' +
                                    '</div>'+
                                    '</div>');

                                $('#btn-accept'+id).on('click', () => approveUser(myObject));
                                $('#btn-reject'+id).on('click', () => rejectUser(myObject));

                            }
                        }
                    });

                }, function (error) {
                    console.log("Error : " + error);
                });
            }
        }
        else{
            console.log("User is logout");
        }
    });

    // var docRef = db.collection("employer");
    //
    // docRef.get().then(function(doc) {
    //     if (doc.exists) {
    //         console.log("Document data:", doc.data());
    //     } else {
    //         // doc.data() will be undefined in this case
    //         console.log("No such document!");
    //     }
    // }).catch(function(error) {
    //     console.log("Error getting document:", error);
    // });

}

function getListOfItinerary(type){

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log("User is login");
            const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            let docRef = db.collection("itinerary");

            switch (type){
                case "itinerary-published":
                    docRef = docRef.where("isPublic", "==", true);
                    docRef = docRef.where("isPublish", "==", true);
                    docRef = docRef.where("userId", "==", user.uid);
                    docRef = docRef.orderBy("dateCreated", "desc");
                    break;
                case "itinerary-draft":
                    docRef = docRef.where("isPublic", "==", false);
                    docRef = docRef.where("isPublish", "==", false);
                    docRef = docRef.where("userId", "==", user.uid);
                    docRef = docRef.orderBy("dateCreated", "desc");
                    break;
                case "itinerary-requestQuotation":
                    docRef = docRef.where("quotationStatus", "==", "pending");
                    docRef = docRef.orderBy("dateQuotationRequested", "desc");
                    break;
                default:
                    return;
            }

            docRef.onSnapshot(function(querySnapshot) {
                if(type === "itinerary-requestQuotation"){
                    $('#main-container-itinerary-requestQuotation').empty();
                }

                querySnapshot.forEach(function(doc) {
                    if(querySnapshot != null){
                        if(!querySnapshot.isEmpty){
                            let iti = doc.data();

                            let id = doc.id;
                            let tripName = iti.tripName;
                            if(tripName == null){
                                tripName = iti.locationName;
                            }
                            let price = iti.currencyCode + iti.price;
                            if(iti.price == null || iti.price <= 0){
                                if(iti.minBudget === iti.maxBudget){
                                    price = iti.currencyCode + iti.minBudget;
                                }else{
                                    price = iti.currencyCode + iti.minBudget + " - " + iti.currencyCode + iti.maxBudget;
                                }
                            }
                            let durationHour = iti.durationHour + " Hour(s)";
                            if(iti.durationHour == null || iti.durationHour <= 0){
                                durationHour = iti.noofDays + " Day(s)";
                            }
                            let likeCount = iti.likeCount;
                            let itineraryPicUrl = iti.itineraryPicUrl;
                            let locationName = iti.locationName;
                            let dateCreated = iti.dateCreated.toDate();
                            let getYear = dateCreated.getUTCFullYear();
                            let getMonth = monthNames[dateCreated.getUTCMonth()];
                            let getDay = dateCreated.getUTCDate();

                            if(itineraryPicUrl != null && itineraryPicUrl !== ''){

                            }
                            else{
                                itineraryPicUrl = 'img/ic_upload_placeholder.png';
                            }

                            $("."+id).remove();

                            if(type === "itinerary-requestQuotation"){

                                let dateRequested = iti.dateQuotationRequested.toDate();
                                let getYear = dateRequested.getUTCFullYear();
                                let getMonth = monthNames[dateRequested.getUTCMonth()];
                                let getDay = dateRequested.getUTCDate();

                                $('#main-container-'+ type).append(
                                    '<div class="card '+id+'" id="'+id+'">'+
                                    '<img class="myImage" id="myImage'+id+'" src="'+itineraryPicUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                    '<div class="card-container">'+
                                    '<h4><b>'+name+'</b></h4>'+
                                    '<p><b>Trip Name: </b>'+tripName+'</p>'+
                                    '<p><b>Location:</b> '+locationName+'</p>'+
                                    '<p><b>Budget:</b> '+price+'</p>'+
                                    '<p><b>Duration:</b> '+durationHour+'</p>'+
                                    '<p><b>Date Requested:</b> '+getDay+"/"+getMonth+"/"+getYear+'</p>'+
                                    '</div>'+
                                    '<div>' +
                                    '<button id="btn-edit-'+id+'" class="btn-quotation">Send Quotation</button>' +
                                    '</div>'+
                                    '</div>');
                            }else{
                                $('#main-container-'+ type).append(
                                    '<div class="card '+id+'" id="'+id+'">'+
                                    '<img class="myImage" id="myImage'+id+'" src="'+itineraryPicUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                    '<div class="card-container">'+
                                    '<h4><b>'+name+'</b></h4>'+
                                    '<p><b>Trip Name: </b>'+tripName+'</p>'+
                                    '<p><b>Location:</b> '+locationName+'</p>'+
                                    '<p><b>Price:</b> '+price+'</p>'+
                                    '<p><b>Duration:</b> '+durationHour+'</p>'+
                                    '<p><b>No. of Likes:</b> '+likeCount+'</p>'+
                                    '<p><b>Date Created:</b> '+getDay+"/"+getMonth+"/"+getYear+'</p>'+
                                    '</div>'+
                                    '<div>' +
                                    '<button id="btn-edit-'+id+'" class="btn-edit">Edit</button>' +
                                    '<button id="btn-delete-'+id+'" class="btn-delete">Delete</button>' +
                                    '</div>'+
                                    '</div>');
                            }

                            $('#btn-edit-'+id).on('click', () => editItinerary(id));
                            $('#btn-delete-'+id).on('click', () => deleteItinerary(id,tripName));
                        }
                    }
                });

            }, function (error) {
                console.log("Error : " + error);
            });
        }
        else{
            console.log("User is logout");
        }
    });
}

function getPaidItiAndKits(isComplete){

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log("User is login");
            const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            let docRef = db.collection("paymentResult")
                            .where("status", "==", "success")
                            .where("isReadByAdmin","==",isComplete)
                            .orderBy("dateUpdated", "desc");

            if(isComplete){
                docRef = docRef.limit(10);
            }

            docRef.onSnapshot(function(querySnapshot) {
                if(isComplete){
                    $('#main-container-itinerary-completedPaidItiAndKits').empty();
                }else{
                    $('#main-container-itinerary-paidItiAndKits').empty();
                }

                if(querySnapshot != null && !querySnapshot.isEmpty){
                    querySnapshot.forEach(function(doc) {
                        let paymentResult = doc.data();
                        let id = doc.id;
                        let iti = paymentResult.itinerary;
                        let travelKits = paymentResult.travelKits;

                        let isOkToAddToList = false;

                        let name,imgUrl;

                        let datePaid = paymentResult.dateUpdated.toDate();
                        let year = datePaid.getUTCFullYear();
                        let month = monthNames[datePaid.getUTCMonth()];
                        let day = datePaid.getUTCDate();
                        let currencyCode = paymentResult.paymentAmount.currencyCode;
                        let paidAmount = paymentResult.paymentAmount.grandTotal;

                        if(iti){
                            name = iti.tripName;
                            if(name == null){
                                name = iti.locationName;
                            }
                            imgUrl = iti.itineraryPicUrl;

                            isOkToAddToList = true;
                        }else if(travelKits && travelKits.length && travelKits.length > 0){
                            name = travelKits[0].name;
                            imgUrl = travelKits[0].coverPhotoUrl;

                            isOkToAddToList = true;
                        }

                        if(isOkToAddToList){
                            if(!imgUrl || imgUrl == null || imgUrl === ''){
                                imgUrl = 'img/ic_upload_placeholder.png';
                            }

                            $("."+id).remove();
                            let elem;
                            if(isComplete){
                                elem = $('#main-container-itinerary-completedPaidItiAndKits');
                            }else{
                                elem = $('#main-container-itinerary-paidItiAndKits');
                            }

                            elem.append(
                                '<div class="card '+id+'" id="'+id+'">'+
                                '<img class="myImage" id="myImage'+id+'" src="'+imgUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                '<div class="card-container">'+
                                '<p><b>Name: </b>'+name+'</p>'+
                                '<p><b>Amount Paid:</b> '+currencyCode + paidAmount+'</p>'+
                                '<p><b>Date Paid:</b> '+day+"/"+month+"/"+year+'</p>'+
                                '</div>'+
                                '<div>' +
                                '<button id="btn-edit-'+id+'" class="btn-quotation">View Details</button>' +
                                '</div>'+
                                '</div>');

                            $('#btn-edit-'+id).on('click', () => goToReceipt(id));
                        }
                    });
                }

            }, function (error) {
                console.log("Error : " + error);
            });
        }
        else{
            console.log("User is logout");
        }
    });
}

function getPaidHomestayAndKits(isComplete){

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log("User is login");
            const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            let docRef = db.collection("paymentResult")
                .where("status", "==", "success")
                .where("isReadByAdmin","==",isComplete)
                .where("bookingHistory.type", "==", "hs")
                .orderBy("dateUpdated", "desc");

            if(isComplete){
                docRef = docRef.limit(10);
            }

            docRef.onSnapshot(function(querySnapshot) {
                if(isComplete){
                    $('#main-container-homestay-completedPaidHomestayAndKits').empty();
                }else{
                    $('#main-container-homestay-paidHomestayAndKits').empty();
                }

                if(querySnapshot != null && !querySnapshot.isEmpty){
                    querySnapshot.forEach(function(doc) {
                        let paymentResult = doc.data();
                        let id = doc.id;
                        let bookingHistory = paymentResult.bookingHistory;

                        if(bookingHistory){
                            db.collection("accommodation")
                                .doc(bookingHistory.targetId)
                                .get()
                                .then(function(doc2) {
                                    if (doc2.exists) {
                                        let bookingHistory = paymentResult.bookingHistory;
                                        let room = doc2.data();

                                        if(!room){
                                            $("."+id).remove();
                                            return;
                                        }
                                        let departDate = new Date(bookingHistory.checkinDate);
                                        let departDateYear = departDate.getUTCFullYear();
                                        let departDateMonth = monthNames[departDate.getUTCMonth()];
                                        let departDateDay = departDate.getUTCDate();
                                        let formattedDepartDate = departDateDay + "/" + departDateMonth + "/" + departDateYear;

                                        let returnDate = new Date(bookingHistory.checkoutDate);
                                        let returnDateYear = returnDate.getUTCFullYear();
                                        let returnDateMonth = monthNames[returnDate.getUTCMonth()];
                                        let returnDateDay = returnDate.getUTCDate();
                                        let formattedReturnDate = returnDateDay + "/" + returnDateMonth + "/" + returnDateYear;

                                        let imgUrl = room.urlOutlook;
                                        let name = room.accommodationName;
                                        let travelDate = formattedDepartDate + ' - ' + formattedReturnDate;
                                        let datePaid = paymentResult.dateUpdated.toDate();
                                        let year = datePaid.getUTCFullYear();
                                        let month = monthNames[datePaid.getUTCMonth()];
                                        let day = datePaid.getUTCDate();
                                        let currencyCode = paymentResult.paymentAmount.currencyCode;
                                        let paidAmount = paymentResult.paymentAmount.grandTotal;

                                        if(!imgUrl || imgUrl == null || imgUrl === ''){
                                            imgUrl = 'img/ic_upload_placeholder.png';
                                        }

                                        $("."+id).remove();
                                        let elem;
                                        if(isComplete){
                                            elem = $('#main-container-homestay-completedPaidHomestayAndKits');
                                        }else{
                                            elem = $('#main-container-homestay-paidHomestayAndKits');
                                        }

                                        elem.append(
                                            '<div class="card '+id+'" id="'+id+'">'+
                                            '<img class="myImage" id="myImage'+id+'" src="'+imgUrl+'" alt="Avatar" style="width:100%; height:auto; max-height: 300px ; min-height: 300px;" onclick="imageModal(this.src,\''+id+'\');">'+
                                            '<div class="card-container">'+
                                            '<p><b>Name: </b>'+name+'</p>'+
                                            '<p><b>Amount Paid:</b> '+currencyCode + paidAmount+'</p>'+
                                            '<p><b>Travel Date:</b> '+travelDate+'</p>'+
                                            '<p><b>Date Paid:</b> '+day+"/"+month+"/"+year+'</p>'+
                                            '</div>'+
                                            '<div>' +
                                            '<button id="btn-edit-'+id+'" class="btn-quotation">View Details</button>' +
                                            '</div>'+
                                            '</div>');

                                        $('#btn-edit-'+id).on('click', () => goToHomestayReceipt(id));
                                    } else {
                                        // doc2.data() will be undefined in this case
                                        console.log("Error: no such document!");
                                        $("."+id).remove();
                                    }
                                }).catch(function(error) {
                                    console.log("Error:", error);
                                    $("."+id).remove();
                                });
                        }else{
                            console.log("empty booking history");
                            $("."+id).remove();
                        }
                    });
                }

            }, function (error) {
                console.log("Error : " + error);
            });
        }
        else{
            console.log("User is logout");
        }
    });
}

function goToReceipt(id){
    if(id && id != null){
        window.location = 'receipt.php?id=' + id;
    }else{
        window.location = 'receipt.php';
    }
}

function goToHomestayReceipt(id){
    if(id && id != null){
        window.location = 'homestayReceipt.php?id=' + id;
    }else{
        window.location = 'homestayReceipt.php';
    }
}

function editItinerary(id){
    if(id && id != null){
        window.location = 'itinerary.php?itineraryId=' + id;
    }else{
        window.location = 'itinerary.php';
    }
}

function deleteItinerary(id,tripName){
    $.confirm({
        title: 'Alert!',
        content: 'Are you sure want to delete this itinerary <b>'+tripName+'</b>?',
        buttons: {
            confirm: function () {
                return db.collection("itinerary").doc(id).delete()
                    .then(function() {
                        $("."+id).remove();
                        openSnackbar("The itinerary has been deleted!");
                    }).catch(function(error) {
                        openSnackbar("Failed to delete itinerary. Error: " + error);
                    });
            },
            cancel: function () {
            }
        }
    });
}

function openSnackbar(message) {
    var x = document.getElementById("snackbar");
    x.innerText = message;
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}