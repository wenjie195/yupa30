
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Submission</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-loggedin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
       <div class="submission-div center">
            <p class="thankyou-p center">Thank You!</p>
            <p class="submission-p center">Your article has been received and it is currently being processed. We will inform you once it is being published.</p>
            
            <div class="centerise spacing2"><a class="waves-effect waves-light btn-large diy-blue-wave confirm-a new-div-width" href="home.php">Confirm</a></div>

       
       
       </div>






   </div>
</div>


<script>
var textareaa = document.querySelectorAll('input[name="report-comment"]');
var textareab = document.getElementById('textareab');
var tempAddress = "";

for(var i = 0; i < textareaa.length; i++) {
    textareaa[i].addEventListener("change", addressHandler);
}


function addressHandler() {
    if(this.id == "textareaa") {
    textareab.disabled = false;
    textareab.value = tempAddress;
  } else {
    tempAddress = textareab.value;
    textareab.value = "";
    textareab.disabled = true;
  }
}
</script>
<script>
function toggleComment() { 
    var thecomment = document.getElementById('comment');
    var displaySetting = thecomment.style.display;
    var commentButton = document.getElementById('commentButton');				
    if (displaySetting == 'block') { 
      thecomment.style.display = 'none';
      commentButton.innerHTML = '<img src="img/comment.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
    else { 
      thecomment.style.display = 'block';
      commentButton.innerHTML = '<img src="img/comment2.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
  }  
</script>



<script>
function toggleReply() { 
    var thereply = document.getElementById('reply');
    var displaySetting = thereply.style.display;
    var replyButton = document.getElementById('replyButton');				
    if (displaySetting == 'block') { 
      thereply.style.display = 'none';
      replyButton.innerHTML = 'View More Replies';
    }
    else { 
      thereply.style.display = 'block';
      replyButton.innerHTML = 'Hide the Replies';
    }
  }  
</script>

<script>
function toggleReplytwo() { 
    var thereplytwo = document.getElementById('replytwo');
    var displaySetting = thereplytwo.style.display;
    var replyButtontwo = document.getElementById('replyButtontwo');				
    if (displaySetting == 'block') { 
      thereplytwo.style.display = 'none';
    }
    else { 
      thereplytwo.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplythree() { 
    var thereplythree = document.getElementById('replythree');
    var displaySetting = thereplythree.style.display;
    var replyButtonthree = document.getElementById('replyButtonthree');				
    if (displaySetting == 'block') { 
      thereplythree.style.display = 'none';
    }
    else { 
      thereplythree.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplyfour() { 
    var thereplyfour = document.getElementById('replyfour');
    var displaySetting = thereplyfour.style.display;
    var replyButtonfour = document.getElementById('replyButtonfour');				
    if (displaySetting == 'block') { 
      thereplyfour.style.display = 'none';
      replyButtonfour.innerHTML = 'View More Comments';    }
    else { 
      thereplyfour.style.display = 'block';
      replyButtonfour.innerHTML = 'Hide Some Comments';
    }
  }  
</script>


<script>
function toggleReplyfive() { 
    var thereplyfive = document.getElementById('replyfive');
    var displaySetting = thereplyfive.style.display;
    var replyButtonfive = document.getElementById('replyButtonfive');				
    if (displaySetting == 'block') { 
      thereplyfive.style.display = 'none';
    }
    else { 
      thereplyfive.style.display = 'block';
    }
  }  
</script>
<?php include 'footer.php'; ?>
</body>
</html>
