
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<!--- Make the image be a variable that fill in the article's cover photo-->
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<!--- Make the title be a variable that fill in the real article title-->
<meta property="og:title" content="YuPa - Article Title" />
<!--- Make the description be a variable that fill in the first paragraph of the article.-->
<meta name="description" content="Article first paragraph.">
<meta property="og:description" content="Article first paragraph." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<!--- Make the title be a variable that fill in the real article title-->
<title>YuPa - Article Title</title>
<!--- Make the title be a variable that fill in the real web address-->
<link rel="canonical" href="https://yupa.asia/" />

    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>
</head>

<body>
<?php include 'menu.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden ow-article-white-bg">
      <img src="img/cms0.jpg" class="img-width-100">
      <div class="article-start-div">
        <div class="artitle-date-div">  
          <p class="article-date"><span class="article-date-span article-span">07/05/2018</span>   <span class="article-time-span article-span">11:00</span>    <a href="home.php" class="category-a"><span class="article-span blue-hover">Travel</span></a></p>
        </div>
      
        <div class="artitle-icon-div">  
           <span class="like-span2 article-span-icon">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a article-icon-img"><img src="img/like2.png" class="like-span-png like2a article-icon-img"></button></span>
           <span class="article-span-icon article-bookmark-span"><span class="cms1-span bookmark-span article-bookmark-span">3k </span><button class="button-a"><img src="img/star.png" class="icon-png bookmark1 article-icon-img"><img src="img/star2.png" class="icon-png bookmark2 article-icon-img"></button></span>
           <!-- Only display this icon when it is not author's view-->
           <a href="#modal2" class="modal-trigger"><span class="flag-span article-span-icon article-flag-span"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1 article-icon-img pointer"><img src="img/flag2.png" class="flag-height flag2 article-icon-img pointer"></button></span></a>
           <!-- Only display this icon when it is author's view, if the user update the article, need send to admin approve first before posting the updated content-->
           <!--<span class="article-span-icon author-pen-span"><img src="img/pen.png" class="icon-png pen1 pen-icon"><img src="img/pen2.png" class="icon-png pen2 pen-icon"></span>-->
        
        </div>
        
        <div class="clear"></div>
        
        <p class="article-title-p">Article Title</p>
        <p class="article-author-p">by <a href="profile.php" class="pointer blue-hover">Jucy Lim</a></p>
        
        <div class="article-content-div">
            <p class="article-content-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum ante a turpis laoreet pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed et pharetra elit, quis vehicula lorem. Ut eleifend, libero sit amet pellentesque aliquet, dui lorem convallis ligula, vitae porttitor lacus purus eu leo. Praesent a tempus ante. Integer sed aliquet est, nec aliquam massa. Morbi dictum dictum diam, eu convallis tortor finibus sit amet. Integer ante leo, auctor eu consectetur eu, accumsan at justo. Vestibulum a vulputate metus, sit amet aliquam arcu. Vivamus arcu orci, dapibus nec nibh ut, rhoncus tempus sem. Proin rhoncus aliquet tellus, in ultrices felis fermentum at. Fusce vulputate nulla et diam placerat tempus.
            </p>
            <img src="img/cms-content.jpg" class="img-width-100 article-content-img">
            <p class="image-description center">Image description.</p>
            <img src="img/cms-content2.jpg" class="img-width-100 article-content-img">
            <p class="image-description center">Image description.</p>            
            <p class="article-content-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum ante a turpis laoreet pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed et pharetra elit, quis vehicula lorem. Ut eleifend, libero sit amet pellentesque aliquet, dui lorem convallis ligula, vitae porttitor lacus purus eu leo. Praesent a tempus ante. Integer sed aliquet est, nec aliquam massa. Morbi dictum dictum diam, eu convallis tortor finibus sit amet. Integer ante leo, auctor eu consectetur eu, accumsan at justo. Vestibulum a vulputate metus, sit amet aliquam arcu. Vivamus arcu orci, dapibus nec nibh ut, rhoncus tempus sem. Proin rhoncus aliquet tellus, in ultrices felis fermentum at. Fusce vulputate nulla et diam placerat tempus.
            </p>
            <p class="article-content-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum ante a turpis laoreet pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed et pharetra elit, quis vehicula lorem. Ut eleifend, libero sit amet pellentesque aliquet, dui lorem convallis ligula, vitae porttitor lacus purus eu leo. Praesent a tempus ante. Integer sed aliquet est, nec aliquam massa. Morbi dictum dictum diam, eu convallis tortor finibus sit amet. Integer ante leo, auctor eu consectetur eu, accumsan at justo. Vestibulum a vulputate metus, sit amet aliquam arcu. Vivamus arcu orci, dapibus nec nibh ut, rhoncus tempus sem. Proin rhoncus aliquet tellus, in ultrices felis fermentum at. Fusce vulputate nulla et diam placerat tempus.
            </p>
        
          <div class="article-action-div">
            <div class="left-like-div article-action-inner-div">
               <span class="big-like-span pointer"><button class="button-a pointer"><img src="img/big-like.png" class="big-like1 big-icon"><img src="img/big-like2.png" class="big-like2 big-icon">
                <p class="big-like-p big-like-p1 grey-to-blue">12k</p></button>
               </span>
            </div>
            
            <div class="right-like-div article-action-inner-div">
               <span class="big-star-span pointer"><button class="button-a pointer"><img src="img/big-star.png" class="big-star1 big-icon"><img src="img/big-star2.png" class="big-star2 big-icon">
                <p class="big-like-p big-share-p1 grey-to-blue">3k</p></button>
               </span>
            </div>
          </div>
          
          
          <p class="share-p">Share:</p>
          <!-- AddToAny BEGIN -->
         <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
           <a class="a2a_button_copy_link"></a>
           <a class="a2a_button_facebook"></a>
           <a class="a2a_button_twitter"></a>
           <a class="a2a_button_google_plus"></a>
           <a class="a2a_button_pinterest"></a>
           <a class="a2a_button_wechat"></a>
           <a class="a2a_button_whatsapp"></a>
           <a class="a2a_button_google_gmail"></a>
           <a class="a2a_button_linkedin"></a>
           <a class="a2a_button_line"></a>
           <a class="a2a_button_email"></a>
           <a class="a2a_button_outlook_com"></a>
           <a class="a2a_button_sms"></a>
           <a class="a2a_button_viber"></a>
           <a class="a2a_button_yahoo_mail"></a>
           <a class="a2a_button_sina_weibo"></a>
           <a class="a2a_button_tumblr"></a>
           <a class="a2a_button_kakao"></a>
           <a class="a2a_button_qzone"></a>
           <a class="a2a_button_telegram"></a>
           <a class="a2a_button_douban"></a>
           <a class="a2a_button_mixi"></a>
           <a class="a2a_button_renren"></a>
         </div>
         <script async src="https://static.addtoany.com/menu/page.js"></script>

          <div class="clear"></div>
          <p class="share-p ow-share-p">Highly Recommend to You:</p>

            <div class="cms-news-container ow-cms-news-container">
     
            <a href="article.php" class="hover-a">
            <div class="cms-box1 ow-cms-box1">
            <img src="img/cms1-550.jpg" class="img-width-100">
                <div class="cms-transparent-box ow-cms-transparent-box">
                <p class="cms-box-date2 ow-article-box-date">08/05/2018</p>
                <p class="cms-box-title2 ow-box-title">Top 10 Spots for Viewing the Sunrise</p>
             </div>
            </div>
           </a>
    
           <a href="article.php" class="hover-a">
           <div class="cms-box1 ow-cms-box1">
           <img src="img/cms2-550.jpg" class="img-width-100">
           <div class="cms-transparent-box ow-cms-transparent-box">
          <p class="cms-box-date2 ow-article-box-date">07/05/2018</p>
          <p class="cms-box-title2 ow-box-title">The Delicious Hidden Mexican Food</p>
           </div>
           </div>
           </a>
    
           <a href="article.php" class="hover-a">
           <div class="cms-box1 ow-cms-box1">
           <img src="img/cms3-550.jpg" class="img-width-100">
           <div class="cms-transparent-box ow-cms-transparent-box">
           <p class="cms-box-date2 ow-article-box-date">06/05/2018</p>
           <p class="cms-box-title2 ow-box-title">10 Free Stay for Travellers</p>
           </div>
           </div>     
           </a>
    
           </div>
          <div class="clear"></div>
          <p class="share-p">Opinions (5):</p>
        
         
          <div class="comment-div ow-comment-div overflow-hidden">
                   
          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input ow-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment">Submit</button>
            </div>
            </div> 
      
         </div>
          
          
          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php" class="user-a hover-turn-blue">Joe March</a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span class="reply-p" onclick="toggleReply()" id="replyButton">View More Replies</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><button class="button-a"><a href="#modal1" class="modal-trigger"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></a></button></span>
            </div>
            </div> 

           
           
           <div class="reply-comment" id="reply" >
           
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php" class="user-a hover-turn-blue">John Smith</a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
          </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><button class="button-a"><a href="#modal1" class="modal-trigger"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></a></button></span>
            </div>
            
           </div> 
           
           <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php" class="user-a hover-turn-blue">Joe March</a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
          </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><button class="button-a"><a href="#modal1" class="modal-trigger"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></a></button></span>
            </div>
            
           </div> 
           <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input ow-reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>
           </div>
           
          </div>          
         </div>

          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php" class="user-a hover-turn-blue">Joe March</a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span onclick="toggleReplytwo()" id="replyButtontwo" class="reply-p">Reply</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><button class="button-a"><a href="#modal1" class="modal-trigger"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></a></button></span>
            </div>
            </div> 


           <div class="reply-comment" id="replytwo" >      
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input ow-reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>           
            </div>      
           </div>
         </div>

          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php" class="user-a hover-turn-blue">Joe March</a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span onclick="toggleReplythree()" id="replyButtonthree" class="reply-p">Reply</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <a href="#modal1" class="modal-trigger"><span class="flag-span"><button class="button-a pointer"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></span></a>
            </div>
            </div> 


           <div class="reply-comment" id="replythree" >      
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input ow-reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>           
            </div>      
           </div>
         </div>


         
           <div class="one-comment-div" id="replyfour" >
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php" class="user-a hover-turn-blue">Joe March</a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span onclick="toggleReplyfive()" id="replyButtonfive" class="reply-p">Reply</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><button class="button-a pointer"><a href="#modal1" class="modal-trigger"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></a></button></span>
            </div>
            </div> 


           <div class="reply-comment" id="replyfive" >      
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input ow-reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>           
            </div>      
           </div>
         </div>        
         <p class="toggle-p ow-toggle-p"><span onclick="toggleReplyfour()" id="replyButtonfour" class="reply-p">View More Comments</span>
         </p>
         
        </div>
        </div>      
       </div>

   </div>
</div>


<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title">Reason of Report this Comment</h4>
        <p>
    <form action="#" id="flag-comment" name="flag-comment">
     <label>
      <input class="with-gap" name="report-comment" type="radio" checked />
      <span class="form-span">Choose Your Reason</span>
    </label>   
    
      <div class="comment-select-div form-input2">
       <select id="report-comment" name="report-comment" >
       
        <option value="It is a spam.">It is a spam.</option>
        <option value="It contains offensive words and profanity.">It contains offensive words and profanity.</option>        
        <option value="It is cyberbullying or harassment.">It is cyberbullying or harassment.</option>
        <option value="It is a hate speech.">It is a hate speech.</option> 
        <option value="It is a fake statement.">It is a fake statement.</option>
        <option value="it is a scam or misleading.">it is a scam or misleading.</option> 
        <option value="It is a personal safety threat.">It is a personal safety threat.</option>
       </select>
       </div>
       
     <label>
      <input class="with-gap" name="report-comment" type="radio" id="textareaa" />
      <span class="form-span">Write a reason</span>
      <textarea class="modal-textarea" name="report-comment"  disabled id="textareab" ></textarea>
    </label>        
       
     </form>

     <div class="centerise"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave div-100" href="#">Submit</a></button></div>
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave modal-close new-div-width text-transform-none" href="#">Cancel</a></div>  
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
    </div>

<!-- Modal Structure -->
<div id="modal2" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title">Reason of Report this Article</h4>
        <p>
    <form action="#" id="flag-comment4" name="flag-comment4">
     <label>
      <input class="with-gap" name="report-comment4" type="radio" checked />
      <span class="form-span">Choose Your Reason</span>
    </label>   
    
      <div class="comment-select-div form-input2">
       <select id="report-comment4" name="report-comment4" >
       
        <option value="It is repated with other similar article.">It is repated with other similar article.</option>
        <option value="It contains offensive words and profanity.">It contains offensive words and profanity.</option>        
        <option value="It is cyberbullying or harassment.">It is cyberbullying or harassment.</option>
        <option value="The content is unsuitable to publish on YuPa website.">The content is unsuitable to publish on YuPa website.</option>   
        <option value="It has a copyright issue.">It has a copyright issue.</option>   
        <option value="It contains too much advertisements.">It contains too much advertisements.</option>              
        <option value="It is a hate speech.">It is a hate speech.</option> 
        <option value="It is a fake statement.">It is a fake statement.</option>
        <option value="it is a scam or misleading.">it is a scam or misleading.</option> 
        <option value="It is a personal safety threat.">It is a personal safety threat.</option>
       </select>
       </div>
       
     <label>
      <input class="with-gap" name="report-comment4" type="radio" id="textareaa4" />
      <span class="form-span">Write a reason</span>
      <textarea class="modal-textarea" name="report-comment4"  disabled id="textareab4" ></textarea>
    </label>        
       
     </form>

     <div class="centerise"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave div-100" href="#">Submit</a></button></div>
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave modal-close new-div-width" href="#">Cancel</a></div>  
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
    </div>
<script>
function toggleComment() { 
    var thecomment = document.getElementById('comment');
    var displaySetting = thecomment.style.display;
    var commentButton = document.getElementById('commentButton');				
    if (displaySetting == 'block') { 
      thecomment.style.display = 'none';
      commentButton.innerHTML = '<img src="img/comment.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
    else { 
      thecomment.style.display = 'block';
      commentButton.innerHTML = '<img src="img/comment2.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
  }  
</script>



<script>
function toggleReply() { 
    var thereply = document.getElementById('reply');
    var displaySetting = thereply.style.display;
    var replyButton = document.getElementById('replyButton');				
    if (displaySetting == 'block') { 
      thereply.style.display = 'none';
      replyButton.innerHTML = 'View More Replies';
    }
    else { 
      thereply.style.display = 'block';
      replyButton.innerHTML = 'Hide the Replies';
    }
  }  
</script>

<script>
function toggleReplytwo() { 
    var thereplytwo = document.getElementById('replytwo');
    var displaySetting = thereplytwo.style.display;
    var replyButtontwo = document.getElementById('replyButtontwo');				
    if (displaySetting == 'block') { 
      thereplytwo.style.display = 'none';
    }
    else { 
      thereplytwo.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplythree() { 
    var thereplythree = document.getElementById('replythree');
    var displaySetting = thereplythree.style.display;
    var replyButtonthree = document.getElementById('replyButtonthree');				
    if (displaySetting == 'block') { 
      thereplythree.style.display = 'none';
    }
    else { 
      thereplythree.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplyfour() { 
    var thereplyfour = document.getElementById('replyfour');
    var displaySetting = thereplyfour.style.display;
    var replyButtonfour = document.getElementById('replyButtonfour');				
    if (displaySetting == 'block') { 
      thereplyfour.style.display = 'none';
      replyButtonfour.innerHTML = 'View More Comments';    }
    else { 
      thereplyfour.style.display = 'block';
      replyButtonfour.innerHTML = 'Hide Some Comments';
    }
  }  
</script>


<script>
function toggleReplyfive() { 
    var thereplyfive = document.getElementById('replyfive');
    var displaySetting = thereplyfive.style.display;
    var replyButtonfive = document.getElementById('replyButtonfive');				
    if (displaySetting == 'block') { 
      thereplyfive.style.display = 'none';
    }
    else { 
      thereplyfive.style.display = 'block';
    }
  }  
</script>
<script src="js/db.js"></script>

<script>
    firebase.auth().onAuthStateChanged(function(user) {

        if (user) {
            console.log(user.uid);
        } else {
            window.location = 'login.php';
        }
    });
</script>

<?php include 'footer.php'; ?>
</body>
</html>
