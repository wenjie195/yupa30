
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Flag List" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Flag List</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-admin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
     
     <div class="tab-div flag-tab-div">
      <div class="tab-menu">
       <ul class="tabs tab-menu-ul">
        <li class="tab tab-menu-li pen-li"><a class="active tab-a" href="#pending"><img src="img/pending1.png" class="tab-pen1 tab-icon"><img src="img/pending2.png" class="tab-pen2 tab-icon"><span class="tab-span"> Pending</span></a></li>
        <li class="tab tab-menu-li star-li"><a class="tab-a" href="#complete"><img src="img/tick.png" class="tab-star1 tab-icon"><img src="img/tick2.png" class="tab-star2 tab-icon"><span class="tab-span"> Checked</span></a></li>
       </ul>
      </div>
      
      <!----Tab 1--->
      <div id="pending" class="contribution-div tab-small-div">
          <input type="text" class="friend-search-input flag-search-input search-bar-padding" placeholder="Search"><button class="button-a search-button-a"><img src="img/friend-search.png" class="friend-search-icon pointer hover-a"></button>
          <div class="category-div ow-category-div">
            <span class="category-span">Category: </span>
            <select class="category-select">
             <option value="All">All</option>
             <option value="Travel">Travel</option>
             <option value="Stay">Stay</option>
             <option value="Food">Food</option>
             <option value="Job">Job</option>
            </select>
         </div>
         <div class="sortby-div ow-sortby-div">         
           <span class="sortby-span view-small2 flag-left">Sort by: </span>  
           <select class="category-sort">
             <option value="Latest Flag">Latest Flag</option>
             <option value="Oldest Flag">Oldest Flag</option>
           </select> 
           <span class="sortby-span view-big2">Sort by: </span>        
        </div>

        <div class="notification-div flag-div">
        <a href="article-flag.php#flag-comment">
           <p class="notification-content hover-a pointer">User Rebecca Neville flagged Joe March’s comment on an article titled “The Hidden Delicious Mexican Food” as spam.</p>
           <p class="notification-date hover-a pointer">09/05/2018 12:00</p>
        </a>
    
       <a href="article-flag.php#flag-comment">
           <p class="notification-content hover-a pointer">User Rebecca Neville flagged Joe March’s comment on an article titled “The Hidden Delicious Mexican Food” as spam.</p>
           <p class="notification-date hover-a pointer">07/05/2018 12:00</p> 
       </a>  
    
       <a href="article-flag.php#flag-comment">
           <p class="notification-content hover-a pointer">User Rebecca Neville flagged Joe March’s comment on an article titled “The Hidden Delicious Mexican Food” as spam.</p>
           <p class="notification-date hover-a pointer">07/05/2018 11:00</p> 
       </a>      
    
      </div>




      
      </div>
      
      <!--- Tab 2 --->
      <div id="complete" class="bookmark-div tab-small-div">
          <input type="text" class="friend-search-input flag-search-input search-bar-padding" placeholder="Search"><button class="button-a search-button-a"><img src="img/friend-search.png" class="friend-search-icon pointer hover-a"></button>
          <div class="category-div ow-category-div">
            <span class="category-span">Category: </span>
            <select class="category-select">
             <option value="All">All</option>
             <option value="Travel">Travel</option>
             <option value="Stay">Stay</option>
             <option value="Food">Food</option>
             <option value="Job">Job</option>
            </select>
         </div>
         <div class="sortby-div ow-sortby-div">         
           <span class="sortby-span view-small2 flag-left">Sort by: </span>  
           <select class="category-sort">
             <option value="Latest Flag">Latest Flag</option>
             <option value="Oldest Flag">Oldest Flag</option>
           </select> 
           <span class="sortby-span view-big2">Sort by: </span>        
        </div>

        <div class="notification-div flag-div">
        <a href="article-flag.php#flag-comment">
           <p class="notification-content hover-a pointer">User Alice Tan flagged Joe March’s comment on an article titled “The Hidden Delicious Mexican Food” as spam.</p>
           <p class="notification-date hover-a pointer">09/05/2018 12:00</p>
        </a>
    
       <a href="article-flag.php#flag-comment">
           <p class="notification-content hover-a pointer">User Rebecca Neville flagged Joe March’s comment on an article titled “The Hidden Delicious Mexican Food” as spam.</p>
           <p class="notification-date hover-a pointer">07/05/2018 12:00</p> 
       </a>  
    
       <a href="article-flag.php#flag-comment">
           <p class="notification-content hover-a pointer">User Rebecca Neville flagged Joe March’s comment on an article titled “The Hidden Delicious Mexican Food” as spam.</p>
           <p class="notification-date hover-a pointer">07/05/2018 11:00</p> 
       </a>      
    
      </div>

      
      </div>
    </div>
 
  
   
   
   </div>
</div>



<?php include 'footer.php'; ?>
</body>
</html>
