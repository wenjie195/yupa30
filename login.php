
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Travel _ Free</title>
<link rel="canonical" href="https://yupa.asia/" />
    <!-- Add additional services that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-functions.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>
</head>

<body>
<?php include 'menu.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
     <div class="login-div">
       <div class="centerise"><a href="index.php" class="hover-a"><img src="img/big-logo.png" class="big-logo"></a></div>
       <p class="login-p bold">Login</p>
       <input type="email" id="Loginemail" placeholder="Email" class="login-input login-input-width">

       <input type="password" id="Loginpassword" placeholder="Password" class="login-input login-input-width">

       <button class="login-input-width blue-login pointer darker-bg-hover" type="submit" id="btnLogin" onclick="login()">Login</button>
       <p class="forgot-p pointer center"><a href="#modal4" class="modal-trigger blue-hover">Forgot Password</a></p>
       <p class="register-here-p centerise">Not yet join us? Register <a href="register.php" class="here-a blue-hover">here.</a></p>
    </div>
   </div>
</div>




<!-- Modal Structure of Forgot Password 1-->
<div id="modal4" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title unfriend-title">Key in Your Email</h4>
        <p>
       <input type="email" placeholder="Email" class="login-input login-input-width ow-login-input">    

     
     <div class="centerise email-gap"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave div-100" >Submit</button></a></button></div> 
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave modal-close text-transform-none new-div-width" href="#">Cancel</a></div> 
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
</div>


<script>
    $('#logout').css("display", "none");

    $(document).ready(function(){
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyClABGbMDQlo5XtA2QxAisefXL9a6-Z8xc",
            authDomain: "yupa-app.firebaseapp.com",
            databaseURL: "https://yupa-app.firebaseio.com",
            projectId: "yupa-app",
            storageBucket: "yupa-app.appspot.com",
            messagingSenderId: "25656510649"
        };
        firebase.initializeApp(config);

    });

    function login() {
        var db = firebase.firestore();

        db.settings({
            timestampsInSnapshots: true
        });

    const email = document.getElementById("Loginemail").value;
    const pass = document.getElementById("Loginpassword").value;

    if (email != "" && pass != "") {
        firebase.auth().signInWithEmailAndPassword(email, pass)
            .then(function() {
                firebase.auth().onAuthStateChanged(function(user) {
                    if (user) {
                        var user = firebase.auth().currentUser;

                        if (user != null) {
                            var emailVerified = user.emailVerified;

                            if (emailVerified) {
                                var uid = user.uid;
                                alert("Logged In");

                                window.location = 'itineraryCMS.php';

                            } else {
                                alert("Please Verify Your Account Through Email");
                            }
                        }
                    }
                });
            })

            .catch(function (error) {

                alert(error);
            });
    } else {
        alert("Please Enter Email or Password");
    }
}

</script>

<?php include 'footer.php'; ?>
</body>
</html>
