<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php include 'header.php'; ?>
    <meta property="og:url" content="https://yupa.asia/"/>
    <meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg"/>
    <meta property="og:title" content="YuPa - Travel _ Free"/>
    <meta name="description"
          content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
    <meta property="og:description"
          content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers."/>
    <meta name="keywords"
          content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
    <title>Homestay Receipt</title>
    <link rel="canonical" href="https://yupa.asia/"/>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-storage.js"></script>
    <!--    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>-->
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>

    <!--    Initiate firestore connection-->
    <script src="js/db.js?version=1.0.2"></script>
    <script src="js/FixImageFileClearWhenNotSelect.js?version=1.0.2"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/date.js?version=1.0.2"></script>

    <script src="js/authUser.js?version=1.0.2"></script>

    <style>
        body {
            background-color: #f7f7f7;
        }

        #main-container {
            width: 70%;
            height: 100%;
            margin-left: 15%;
            margin-right: 15%;
            background-color: white;
        }

        .fixed [type="checkbox"] + label, .fixed [type="radio"] + label {
            pointer-events: auto;
        }

        .custom-li {
            background: black;
        }

        .custom-li-a {
            color: white !important;
        }


        .activity-detail {
            display: none;
        }

        .custom-input-field {
            float: right !important;
        }

        .showActivityContent {
            display: inline-block;
            width: 100%;
        }

        .row-activity {
            border-radius: 15px;
            border: 1px solid black;
            margin-top: 20px;
            margin-bottom: 20px;
            padding: 20px;
        }

        #add i {
            background: black;
            border: 1px solid black;
            border-radius: 50px;
        }

        #sub i {
            background: black;
            border: 1px solid black;
            border-radius: 50px;

        }

        #button-container {
            width: 100%;
        }

        #markAsDone-btn {
            background-color: #00468c;
        }

        #markAsUndone-btn {
            background-color: #8c0d0f;
        }

        .myButton {
            display: block;
            width: 40%;
            margin: 30px 30%;
            text-transform: none;
            border-radius: 15px;
            font-weight: bold;
        }

        #progress-bar {
            /*margin: auto;*/
            /*width: 60%;*/
            /*height: 8px;*/
            position: relative;
        }

        #progress-bar > .ui-progressbar-value {
            background: #00acc1;
            margin: 0;
        }

        #progress-text {
            float: right;
        }

        #progress-title {
            margin-top: 10px;
            margin-bottom: 30px;
            font-size: large;
        }

        #modal-progress {
            border-radius: 15px;
            height: 200px;
        }

        #snackbar {
            visibility: hidden;
            min-width: 250px;
            margin-left: -125px;
            background-color: #333;
            color: #fff;
            text-align: center;
            border-radius: 2px;
            padding: 16px;
            position: fixed;
            z-index: 1;
            left: 50%;
            bottom: 30px;
            font-size: 17px;
        }

        #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }
    </style>
</head>
<body>
<?php include 'menu-admin.php'; ?>
<div id="main-container">
    <h4 style="text-align: center">Homestay Receipt</h4>

    <div id="user-div" style="display: none; margin-top: 5px;">
        <h5><b><u>User Details</u></b></h5>
        <table>
            <tbody>
            <tr>
                <th style="width: 22.5%;">
                    <img id="user-profilePic" src="img/profile1.jpg" style="width: 100%;height: auto;" onError="this.onerror=null;this.src='img/profile1.jpg';"/>
                </th>
                <td style="width: 81.5%;">
                    <table>
                        <tr><td id="user-name"></td></tr>
                        <tr><td id="user-email"></td></tr>
                        <tr><td id="user-contactNo"></td></tr>
                        <tr><td id="user-country"></td></tr>
                        <tr id="user-pickuptime-tr"><td>Pickup Time: <span id="user-pickuptime"></span></td></tr>
                        <tr id="user-pickuplocation-tr"><td>Pickup Location: <span id="user-pickuplocation"></span></td></tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div id="host-div" style="display: none; margin-top: 5px;">
        <h5><b><u>Host Details</u></b></h5>
        <table>
            <tbody>
            <tr>
                <th style="width: 22.5%;">
                    <img id="host-profilePic" src="img/profile1.jpg" style="width: 100%;height: auto;" onError="this.onerror=null;this.src='img/profile1.jpg';"/>
                </th>
                <td style="width: 81.5%;">
                    <table>
                        <tr><td id="host-name"></td></tr>
                        <tr><td id="host-email"></td></tr>
                        <tr><td id="host-contactNo"></td></tr>
                        <tr><td id="host-country"></td></tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div id="homestay-div" style="display: none; margin-top: 10px;">
        <h5><b><u>Paid Homestay Details</u></b></h5>
        <table>
            <tbody>
            <tr id="homestay-name">
                <th>Name</th>
            </tr>
            <tr id="homestay-price">
                <th>Price per night</th>
            </tr>
            <tr id="homestay-date">
                <th>Travel Date</th>
            </tr>
            <tr id="homestay-location">
                <th>Location Name</th>
            </tr>
            <tr id="homestay-grandTotal">
                <th>Grand Total</th>
            </tr>
            <tr id="homestay-datePaid">
                <th>Date Paid</th>
            </tr>
            </tbody>
        </table>
    </div>

    <div id="travelKit-div" style="display: none;">
        <h5><b><u>All Purchased Travel Kit Details</u></b></h5>
    </div>

    <div id="button-container">
        <a id="markAsDone-btn" class="waves-effect waves-light btn-large myButton"
           onclick="markAsDone(true);">Mark as Done</a>
        <a id="markAsUndone-btn" class="waves-effect waves-light btn-large myButton"
           onclick="markAsDone(false);">Mark as Undone</a>
    </div>


</div>

<!-- Modal Structure -->
<div id="modal-progress" class="modal">
    <div class="modal-content">
        <div id="progress-title" class="col s8 no-padding">Fetching receipt details...</div>
        <div id="progress-bar" class="col s8 no-padding"></div>
        <div id="progress-text" class="col s8 no-padding"></div>
    </div>
</div>

<div id="snackbar"></div>

<script>
    let paymentResultId;
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    $( document ).ready(function() {
        paymentResultId = getParameterByName('id');
        getPaymentResult();

        $('#markAsDone-btn').hide();
        $('#markAsUndone-btn').hide();
    });

    function getPaymentResult(){
        firebase.auth().onAuthStateChanged(function(user) {
            if (user && paymentResultId) {
                console.log("User is login with receipt id");

                let docRef = db.collection("paymentResult")
                                .doc(paymentResultId);

                docRef.get().then(function(doc) {
                    if (doc.exists) {
                        populateUserDetails(doc);
                        populateHostDetails(doc);
                        populateReceiptDetails(doc);
                        populateTravelKits(doc);
                    } else {
                        // doc.data() will be undefined in this case
                        console.log("Error: no such document!");
                    }
                }).catch(function(error) {
                    console.log("Error:", error);
                });
            }
            else{
                console.log("User is logout or empty receipt id");
            }
        });
    }

    function populateUserDetails(doc){
        let paymentResult = doc.data();

        if(paymentResult.pickupTime){
            let pickupTime = new Date(paymentResult.pickupTime);
            let year = pickupTime.getUTCFullYear();
            let month = monthNames[pickupTime.getUTCMonth()];
            let day = pickupTime.getUTCDate();
            let hour = getUTCHour(pickupTime);
            let minute = getUTCMinute(pickupTime);
            let meridian = "AM";
            if(parseInt(getUTCHour(pickupTime)) >= 12){
                meridian = "PM";
                hour = format_two_digits(parseInt(hour) - 12);
            }

            if(hour === "00"){
                hour = "12";
            }

            $('#user-pickuptime').append(day + "/" + month + "/" + year + " @ " + hour + ":" + minute + meridian);
            $('#user-pickuptime-tr').show();
        }else{
            $('#user-pickuptime-tr').hide();
        }

        if(paymentResult.pickupLocationName && paymentResult.pickupLocationType){
            $('#user-pickuplocation').append(paymentResult.pickupLocationName + " @ " + paymentResult.pickupLocationType);
            $('#user-pickuplocation-tr').show();
        }else{
            $('#user-pickuplocation-tr').hide();
        }

        if(paymentResult.userId){
            let docRef = db.collection("user")
                .doc(paymentResult.userId);

            docRef.get().then(function(doc) {
                if (doc.exists) {
                    let user = doc.data();
                    if(user){
                        $('#user-div').show();

                        if(user.profilePicUrl && user.profilePicUrl !== ""){
                            $('#user-profilePic').attr('src',user.profilePicUrl);
                        }
                        $('#user-name').append(user.name);
                        $('#user-email').append(user.email);
                        $('#user-contactNo').append(user.countryCode + " " +  user.contactNo);
                        $('#user-country').append(user.country);
                    }else{
                        $('#user-div').hide();
                    }
                } else {
                    // doc.data() will be undefined in this case
                    console.log("Error: no such document!");
                    $('#user-div').hide();
                }
            }).catch(function(error) {
                console.log("Error:", error);
                $('#user-div').hide();
            });
        }
    }

    function populateHostDetails(doc){
        let paymentResult = doc.data();
        if(paymentResult.bookingHistory && paymentResult.bookingHistory.hostId){
            let docRef = db.collection("user")
                .doc(paymentResult.bookingHistory.hostId);

            docRef.get().then(function(doc) {
                if (doc.exists) {
                    let user = doc.data();
                    if(user){
                        $('#host-div').show();

                        if(user.profilePicUrl && user.profilePicUrl !== ""){
                            $('#host-profilePic').attr('src',user.profilePicUrl);
                        }
                        $('#host-name').append(user.name);
                        $('#host-email').append(user.email);
                        $('#host-contactNo').append(user.countryCode + " " +  user.contactNo);
                        $('#host-country').append(user.country);
                    }else{
                        $('#host-div').hide();
                    }
                } else {
                    // doc.data() will be undefined in this case
                    console.log("Error: no such document!");
                    $('#host-div').hide();
                }
            }).catch(function(error) {
                console.log("Error:", error);
                $('#host-div').hide();
            });
        }
    }

    function populateTravelKits(doc){
        let paymentResult = doc.data();
        if(paymentResult.travelKits && paymentResult.travelKits.length > 0){
            $('#travelKit-div').show();

            let count = 0;
            paymentResult.travelKits.forEach(function(travelKit) {
                $('#travelKit-div').append(
                        '<table style="margin-top: 10px; margin-bottom: 5px; border: 2px black solid;">'
                        + ' <tbody>'
                        + '     <tr id="tk-name-' + count + '">'
                        + '         <th>Name</th>'
                        + '         <td><b>' + travelKit.name + '</b></td>'
                        + '     </tr>'
                        + '     <tr id="tk-price-' + count + '">'
                        + '         <th>Price</th>'
                        + '     </tr>'
                        + '     <tr id="tk-products-' + count + '">'
                        + '         <th>Products</th>'
                        + '         <td>'
                        + '             <table>'
                        + '                 <tbody id="tk-products-body-' + count + '">'
                        + '                     <tr>'
                        + '                         <th>Name</th>'
                        + '                         <th>Price</th>'
                        + '                         <th>Quantity</th>'
                        + '                         <th>Remark</th>'
                        + '                     </tr>'
                        + '                 </tbody>'
                        + '             </table>'
                        + '         </td>'
                        + '     </tr>');

                if(travelKit.productList){
                    let tkPrice = 0;

                    for(let name in travelKit.productList) {
                        if (travelKit.productList.hasOwnProperty(name)) {
                            let price = 0;
                            let quantity = 0;
                            let remark = "";

                            price = travelKit.productList[name];
                            if(travelKit.productList[name]){
                            }

                            if(travelKit.quantityMap[name]){
                                quantity = travelKit.quantityMap[name];
                            }
                            tkPrice += (price * quantity);

                            if(travelKit.productRemark[name]){
                                remark = travelKit.productRemark[name];
                            }

                            $('#tk-products-body-' + count).append(
                                  ' <tr>'
                                + '     <td style="width: 30%;">' + name + '</td>'
                                + '     <td style="width: 20%;">' + travelKit.currencyCode + price + '</td>'
                                + '     <td style="width: 15%;">' + quantity + '</td>'
                                + '     <td style="width: 35%;">' + remark + '</td>'
                                + ' </tr>'
                            );
                        }
                    }
                    $('#tk-price-' + count).append('<td>' + travelKit.currencyCode + tkPrice.toFixed(2) + '</td>');
                }

                count++;
            });
        }else{
            $('#travelKit-div').hide();
        }
    }

    function populateReceiptDetails(doc){
        let paymentResult = doc.data();

        if(paymentResult.isReadByAdmin){
            showMarkDoneButton(false);
        }else{
            showMarkDoneButton(true);
        }

        if(paymentResult.bookingHistory && paymentResult.bookingHistory.targetId){

            let docRef = db.collection("accommodation")
                .doc(paymentResult.bookingHistory.targetId);

            docRef.get().then(function(doc2) {
                if (doc2.exists) {
                    let bookingHistory = paymentResult.bookingHistory;
                    let room = doc2.data();

                    if(room){
                        $('#homestay-div').show();
                    }else{
                        $('#homestay-div').hide();
                    }
                    let departDate = new Date(bookingHistory.checkinDate);
                    let departDateYear = departDate.getUTCFullYear();
                    let departDateMonth = monthNames[departDate.getUTCMonth()];
                    let departDateDay = departDate.getUTCDate();
                    let formattedDepartDate = departDateDay + "/" + departDateMonth + "/" + departDateYear;

                    let returnDate = new Date(bookingHistory.checkoutDate);
                    let returnDateYear = returnDate.getUTCFullYear();
                    let returnDateMonth = monthNames[returnDate.getUTCMonth()];
                    let returnDateDay = returnDate.getUTCDate();
                    let formattedReturnDate = returnDateDay + "/" + returnDateMonth + "/" + returnDateYear;

                    let datePaid = paymentResult.dateUpdated.toDate();
                    let datePaidYear = datePaid.getUTCFullYear();
                    let datePaidMonth = monthNames[datePaid.getUTCMonth()];
                    let datePaidDay = datePaid.getUTCDate();
                    let formattedDatePaid = datePaidDay + "/" + datePaidMonth + "/" + datePaidYear;

                    $('#homestay-name').append('<td>' + room.accommodationName + '</td>');
                    $('#homestay-price').append('<td>' + room.currencyType + room.currencyValue + '</td>');
                    $('#homestay-date').append('<td>' + formattedDepartDate + ' - ' + formattedReturnDate + '</td>');
                    $('#homestay-location').append('<td>' + room.location + '</td>');
                    $('#homestay-grandTotal').append('<td>' + paymentResult.paymentAmount.currencyCode + paymentResult.paymentAmount.grandTotal + '</td>');
                    $('#homestay-datePaid').append('<td>' + formattedDatePaid + '</td>');
                } else {
                    // doc2.data() will be undefined in this case
                    console.log("Error: no such document!");
                    $('#homestay-div').hide();
                }
            }).catch(function(error) {
                console.log("Error:", error);
                $('#homestay-div').hide();
            });

        }else{
            $('#homestay-div').hide();
        }
    }

    function getHour(date){
        return format_two_digits(date.getHours());
    }

    function getMinute(date){
        return format_two_digits(date.getMinutes());
    }

    function getUTCHour(date){
        return format_two_digits(date.getUTCHours());
    }

    function getUTCMinute(date){
        return format_two_digits(date.getUTCMinutes());
    }

    function getSecond(date){
        return format_two_digits(date.getSeconds());
    }

    function format_two_digits(n) {
        return n < 10 ? '0' + n : n;
    }

    function markAsDone(isDone){
        let receipt = {};
        if(isDone){
            receipt['isReadByAdmin'] = true;
        }else{
            receipt['isReadByAdmin'] = false;
        }

        db.collection("paymentResult")
            .doc(paymentResultId)
            .update(receipt)
            .then(function () {
                if(isDone){
                    showMarkDoneButton(false);
                    openSnackbar("Successfully marked as done!");
                }else{
                    showMarkDoneButton(true);
                    openSnackbar("Successfully marked as undone!");
                }
            }).catch(function(error) {
                if(isDone){
                    showMarkDoneButton(true);
                    openSnackbar("Failed to mark as done!, Please try again");
                }else{
                    showMarkDoneButton(false);
                    openSnackbar("Failed to mark as undone!, Please try again");
                }
                console.log("Error getting document:", error);
            });
    }

    function showMarkDoneButton(isShow){
        if(isShow){
            $('#markAsDone-btn').show();
            $('#markAsUndone-btn').hide();
        }else{
            $('#markAsDone-btn').hide();
            $('#markAsUndone-btn').show();
        }
    }

    function openSnackbar(message) {
        var x = document.getElementById("snackbar");
        x.innerText = message;
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    //To check the current user is admin or not, put at the last paragraph will be better
    authAdmin();
</script>

<?php include 'footer.php'; ?>
</body>
</html>
