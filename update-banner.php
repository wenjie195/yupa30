
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Update Banner</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-loggedin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
       <div class="experience-div">
            <p class="experience-p center">Update Banner</p>
       

            <p class="spacing-p2"></p>     
            <p class="upload-cover">Upload Banner 1</p>
            <!---  Only the cover photo of the article need to crop. Other photos uploaded for the article, don’t need to crop. The size for cover photo is 1600px (width) X 640px (height). Ratio 2.5 :1.-->
            <p class="spacing-p2"></p>   
            <div class="input-field">
            <input  id="banner-link1" type="text" class="input-font">
            <label for="banner-link1" class="input-font">Link</label>
            </div>
            <form action="#">
               <div class="file-field input-field pointer">
                 <div class="btn ow-btn pointer darker-bg-hover">
                  <span class="white-text photo-span pointer">Photo</span>
                  <input type="file" accept="image/*" onchange="preview_image(event)">
                </div>
                <div class="file-path-wrapper">
                 <input class="file-path validate" type="text">
                </div>
              </div>
           </form>
           <div class="img-preview-div">
              <img id="output_image" class="preview-img"/>
           </div>

            <p class="spacing-p4"></p>     
            <p class="upload-cover">Upload Banner 2</p>
            <!---  Only the cover photo of the article need to crop. Other photos uploaded for the article, don’t need to crop. The size for cover photo is 1600px (width) X 640px (height). Ratio 2.5 :1.-->
            <p class="spacing-p2"></p>   
            <div class="input-field">
            <input  id="banner-link2" type="text" class="input-font">
            <label for="banner-link2" class="input-font">Link</label>
            </div>
            <form action="#">
               <div class="file-field input-field pointer">
                 <div class="btn ow-btn pointer darker-bg-hover">
                  <span class="white-text photo-span pointer">Photo</span>
                  <input type="file" accept="image/*"  onchange="preview_image2(event)">
                </div>
                <div class="file-path-wrapper">
                 <input class="file-path validate" type="text">
                </div>
              </div>
           </form>
           <div class="img-preview-div">
              <img id="output_image2" class="preview-img"/>
           </div>           
       

            <p class="spacing-p4"></p>     
            <p class="upload-cover">Upload Banner 3</p>
            <!---  Only the cover photo of the article need to crop. Other photos uploaded for the article, don’t need to crop. The size for cover photo is 1600px (width) X 640px (height). Ratio 2.5 :1.-->
            <p class="spacing-p2"></p>   
            <div class="input-field">
            <input  id="banner-link3" type="text" class="input-font">
            <label for="banner-link3" class="input-font">Link</label>
            </div>
            <form action="#">
               <div class="file-field input-field pointer">
                 <div class="btn ow-btn pointer darker-bg-hover">
                  <span class="white-text photo-span pointer">Photo</span>
                  <input type="file" accept="image/*"  onchange="preview_image3(event)">
                </div>
                <div class="file-path-wrapper">
                 <input class="file-path validate" type="text">
                </div>
              </div>
           </form>                      
           <div class="img-preview-div">
              <img id="output_image3" class="preview-img"/>
           </div>      
          <!--- Insert the textarea tool here-->





         <p class="spacing-p2"></p>  
         <div class="centerise spacing2"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave confirm-a div-100" href="submission.php">Submit</a></button></div>
           
       </div>






   </div>
</div>

<script type='text/javascript'>
function preview_image(event) 
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}
</script>
<script type='text/javascript'>
function preview_image2(event) 
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image2');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}
</script>
<script type='text/javascript'>
function preview_image3(event) 
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image3');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}
</script>
<?php include 'footer.php'; ?>
</body>
</html>
