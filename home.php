<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Travel _ Free</title>
<link rel="canonical" href="https://yupa.asia/" />
    <!-- Add additional services that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-functions.js"></script>
</head>

<body>
<?php include 'menu.php'; ?>

<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
   
     <!--Repeated CMS Part---> 
     <div class="cms1-box">
         <a href="article.php"><img src="img/cms0.jpg" class="img-width-100 cms1-img"></a>
         <div class="cms1-inner-box">
           <p class="cms1-date"><a href="article.php" class="cms1-a">07/05/2018   12:00    Travel</a></p>
           <p class="cms1-title"><a href="article.php" class="cms1-a">Top 20 Places for Mountain Hiking</a></p>
           <ul class="cms1-ul">
             <li class="cms1-li like-li"><button class="button-a"><img src="img/like.png" class="icon-png like1"><img src="img/like2.png" class="icon-png like2"> <span class="cms1-span like-span">12k</span></button></li>
             <li class="cms1-li comment-li" onclick="toggleComment()" id="commentButton"><img src="img/comment.png" class="icon-png comment1"><img src="img/comment2.png" class="icon-png comment2">  <span class="cms1-span comment-span">20k</span></li>
             <li class="cms1-li bookmark-li"><button class="button-a"><img src="img/star.png" class="icon-png bookmark1"><img src="img/star2.png" class="icon-png bookmark2">  <span class="cms1-span bookmark-span">3k</span></button></li>
           </ul>
           
         </div>
         
         
          <div id="comment" class="comment-div overflow-hidden">
          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php"><span class="username-p hover-turn-blue">Joe March</span></a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span class="reply-p" onclick="toggleReply()" id="replyButton">View More Replies</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><a href="#modal1" class="modal-trigger"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></a></span>
            </div>
            </div> 

           
           
           <div class="reply-comment" id="reply" >
           
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php"><span class="username-p hover-turn-blue">John Smith</span></a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
          </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><a href="#modal1" class="modal-trigger"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></a></span>
            </div>
            
           </div> 
           
           <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php"><span class="username-p hover-turn-blue">Joe March</span></a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
          </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><a href="#modal1" class="modal-trigger"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></a></span>
            </div>
            
           </div> 
           <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>
           </div>
           
          </div>          
         </div>

          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php"><span class="username-p hover-turn-blue">Joe March</span></a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span onclick="toggleReplytwo()" id="replyButtontwo" class="reply-p">Reply</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><a href="#modal1" class="modal-trigger"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></a></span>
            </div>
            </div> 


           <div class="reply-comment" id="replytwo" >      
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>           
            </div>      
           </div>
         </div>

          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php"><span class="username-p hover-turn-blue">Joe March</span></a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span onclick="toggleReplythree()" id="replyButtonthree" class="reply-p">Reply</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <a href="#modal1" class="modal-trigger"><span class="flag-span"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></span></a>
            </div>
            </div> 


           <div class="reply-comment" id="replythree" >      
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>           
            </div>      
           </div>
         </div>


         
           <div class="one-comment-div" id="replyfour" >
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2">
            <p class="username-p"><a href="profile.php"><span class="username-p hover-turn-blue">Joe March</span></a></p>
            <p class="commentdate-p">09/05/2018  12:00</p>
            <p class="comment-p">I love this. It’s very informative.</p>
            <p><span onclick="toggleReplyfive()" id="replyButtonfive" class="reply-p">Reply</span></p>
            </div>
            <div class="comment-div3">
             <span class="like-span2">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a"><img src="img/like2.png" class="like-span-png like2a"></button></span><br>
             <span class="flag-span"><a href="#modal1" class="modal-trigger"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1"><img src="img/flag2.png" class="flag-height flag2"></button></a></span>
            </div>
            </div> 


           <div class="reply-comment" id="replyfive" >      
            <div class="reply-comment-set">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input reply-comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment reply-submit-button">Submit</button>
            </div>           
            </div>      
           </div>
         </div>        
         <p class="toggle-p"><span onclick="toggleReplyfour()" id="replyButtonfour" class="reply-p">View More Comments</span></p>
         
          <div class="one-comment-div">
           <div class="one-comment">
            <div class="comment-div1">
             <a href="profile.php" class="hover-a"><img src="img/profile1.jpg" class="img-width-100"></a>
            </div>
            <div class="comment-div2a">
              <input class="comment-input" type="text" placeholder="Share your opinion!">
            </div>
            <div class="comment-div3a">
              <button class="submit-comment">Submit</button>
            </div>
            </div> 
      
         </div>
         
         
        </div> 
         

       
       </div>
   <!-- End of Repeated Part -->
  
   
   
   </div>
</div>

<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title">Reason of Report this Comment</h4>
        <p>
    <form action="#" id="flag-comment" name="flag-comment">
     <label>
      <input class="with-gap" name="report-comment" type="radio" checked />
      <span class="form-span">Choose Your Reason</span>
    </label>   
    
      <div class="comment-select-div form-input2">
       <select id="report-comment" name="report-comment" >
       
        <option value="It is a spam.">It is a spam.</option>
        <option value="It contains offensive words and profanity.">It contains offensive words and profanity.</option>        
        <option value="It is cyberbullying or harassment.">It is cyberbullying or harassment.</option>
        <option value="It is a hate speech.">It is a hate speech.</option> 
        <option value="It is a fake statement.">It is a fake statement.</option>
        <option value="it is a scam or misleading.">it is a scam or misleading.</option> 
        <option value="It is a personal safety threat.">It is a personal safety threat.</option>
       </select>
       </div>
       
     <label>
      <input class="with-gap" name="report-comment" type="radio" id="textareaa" />
      <span class="form-span">Write a reason</span>
      <textarea class="modal-textarea" name="report-comment"  disabled id="textareab" ></textarea>
    </label>        
       
     </form>

     <div class="centerise"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave div-100" href="#">Submit</a></button></div>
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave modal-close new-div-width text-transform-none" href="#">Cancel</a></div>  
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
    </div>

<script>
    $(document).ready(function(){
// Initialize Firebase

        var config = {
            apiKey: "AIzaSyClABGbMDQlo5XtA2QxAisefXL9a6-Z8xc",
            authDomain: "yupa-app.firebaseapp.com",
            databaseURL: "https://yupa-app.firebaseio.com",
            projectId: "yupa-app",
            storageBucket: "yupa-app.appspot.com",
            messagingSenderId: "25656510649"
        };
        firebase.initializeApp(config);

    });
</script>

<script>
function toggleComment() { 
    var thecomment = document.getElementById('comment');
    var displaySetting = thecomment.style.display;
    var commentButton = document.getElementById('commentButton');				
    if (displaySetting == 'block') { 
      thecomment.style.display = 'none';
      commentButton.innerHTML = '<img src="img/comment.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
    else { 
      thecomment.style.display = 'block';
      commentButton.innerHTML = '<img src="img/comment2.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
  }  
</script>

<script>
function toggleReply() { 
    var thereply = document.getElementById('reply');
    var displaySetting = thereply.style.display;
    var replyButton = document.getElementById('replyButton');				
    if (displaySetting == 'block') { 
      thereply.style.display = 'none';
      replyButton.innerHTML = 'View More Replies';
    }
    else { 
      thereply.style.display = 'block';
      replyButton.innerHTML = 'Hide the Replies';
    }
  }  
</script>

<script>
function toggleReplytwo() { 
    var thereplytwo = document.getElementById('replytwo');
    var displaySetting = thereplytwo.style.display;
    var replyButtontwo = document.getElementById('replyButtontwo');				
    if (displaySetting == 'block') { 
      thereplytwo.style.display = 'none';
    }
    else { 
      thereplytwo.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplythree() { 
    var thereplythree = document.getElementById('replythree');
    var displaySetting = thereplythree.style.display;
    var replyButtonthree = document.getElementById('replyButtonthree');				
    if (displaySetting == 'block') { 
      thereplythree.style.display = 'none';
    }
    else { 
      thereplythree.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplyfour() { 
    var thereplyfour = document.getElementById('replyfour');
    var displaySetting = thereplyfour.style.display;
    var replyButtonfour = document.getElementById('replyButtonfour');				
    if (displaySetting == 'block') { 
      thereplyfour.style.display = 'none';
      replyButtonfour.innerHTML = 'View More Comments';    }
    else { 
      thereplyfour.style.display = 'block';
      replyButtonfour.innerHTML = 'Hide Some Comments';
    }
  }  
</script>


<script>
function toggleReplyfive() { 
    var thereplyfive = document.getElementById('replyfive');
    var displaySetting = thereplyfive.style.display;
    var replyButtonfive = document.getElementById('replyButtonfive');				
    if (displaySetting == 'block') { 
      thereplyfive.style.display = 'none';
    }
    else { 
      thereplyfive.style.display = 'block';
    }
  }  
</script>
<?php include 'footer.php'; ?>
</body>
</html>
