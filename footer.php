<div class="clear"></div>
<div class="darkblue-bg white-text center footer2">@ 2018 YuPa, All Rights Reserved.</div></div>
<script src="js/headroom.js"></script>
<script>
    (function () {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset: 205,
            classes: {
                initial: "animated",
                pinned: "slideDown",
                unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

<script>
    var textareaa = document.querySelectorAll('input[name="report-comment"]');
    var textareab = document.getElementById('textareab');
    var tempAddress = "";

    for (var i = 0; i < textareaa.length; i++) {
        textareaa[i].addEventListener("change", addressHandler);
    }


    function addressHandler() {
        if (this.id == "textareaa") {
            textareab.disabled = false;
            textareab.value = tempAddress;
        } else {
            tempAddress = textareab.value;
            textareab.value = "";
            textareab.disabled = true;
        }
    }
</script>
<script>
    var textareaa2 = document.querySelectorAll('input[name="report-article2"]');
    var textareab2 = document.getElementById('textareab2');
    var tempAddress = "";

    for (var i = 0; i < textareaa2.length; i++) {
        textareaa2[i].addEventListener("change", addressHandler);
    }


    function addressHandler() {
        if (this.id == "textareaa2") {
            textareab2.disabled = false;
            textareab2.value = tempAddress;
        } else {
            tempAddress = textareab2.value;
            textareab2.value = "";
            textareab2.disabled = true;
        }
    }
</script>
<script>
    var textareaa3 = document.querySelectorAll('input[name="report-comment3"]');
    var textareab3 = document.getElementById('textareab3');
    var tempAddress = "";

    for (var i = 0; i < textareaa3.length; i++) {
        textareaa3[i].addEventListener("change", addressHandler);
    }


    function addressHandler() {
        if (this.id == "textareaa3") {
            textareab3.disabled = false;
            textareab3.value = tempAddress;
        } else {
            tempAddress = textareab3.value;
            textareab3.value = "";
            textareab3.disabled = true;
        }
    }
</script>
<script>
    var textareaa4 = document.querySelectorAll('input[name="report-comment4"]');
    var textareab4 = document.getElementById('textareab4');
    var tempAddress = "";

    for (var i = 0; i < textareaa4.length; i++) {
        textareaa4[i].addEventListener("change", addressHandler);
    }


    function addressHandler() {
        if (this.id == "textareaa4") {
            textareab4.disabled = false;
            textareab4.value = tempAddress;
        } else {
            tempAddress = textareab4.value;
            textareab4.value = "";
            textareab4.disabled = true;
        }
    }
</script>
<script>
    var textareaa5 = document.querySelectorAll('input[name="report-comment5"]');
    var textareab5 = document.getElementById('textareab5');
    var tempAddress = "";

    for (var i = 0; i < textareaa5.length; i++) {
        textareaa5[i].addEventListener("change", addressHandler);
    }


    function addressHandler() {
        if (this.id == "textareaa5") {
            textareab5.disabled = false;
            textareab5.value = tempAddress;
        } else {
            tempAddress = textareab5.value;
            textareab5.value = "";
            textareab5.disabled = true;
        }
    }
</script>
<script>
    var textareaa6 = document.querySelectorAll('input[name="report-comment6"]');
    var textareab6 = document.getElementById('textareab6');
    var tempAddress = "";

    for (var i = 0; i < textareaa6.length; i++) {
        textareaa6[i].addEventListener("change", addressHandler);
    }


    function addressHandler() {
        if (this.id == "textareaa6") {
            textareab6.disabled = false;
            textareab6.value = tempAddress;
        } else {
            tempAddress = textareab6.value;
            textareab6.value = "";
            textareab6.disabled = true;
        }
    }
</script>
<script src="js/materialize.js" type="text/javascript"></script>

<script>
    let options = {};
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems, options);
    });

    // Initialize collapsible (uncomment the lines below if you use the dropdown variation)
    // var collapsibleElem = document.querySelector('.collapsible');
    // var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);

    // Or with jQuery

    $(document).ready(function () {
        $('.sidenav').sidenav({
            menuWidth: 250,
            edge: 'right', // <--- CHECK THIS OUT

        });
    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function () {
        $('.collapsible').collapsible();
    });
</script>

<!---Flag--->
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function () {
        $('.modal').modal();
    });

</script>

<script>

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function () {
        $('select').formSelect();
    });
</script>
<script>

    $(document).ready(function () {
        $('.tabs').tabs();
    });

</script>

<script src="js/materialize.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {


        $('.chips-autocomplete').material_chip({
            secondaryPlaceholder: 'Hobby + Enter',
            placeholder: 'Hobby + Enter',
            autocompleteData: {
                'Hiking': null,
                'Swimming': null,
                'Reading': null,
                'Jogging': null,
                'Travelling': null,
                'Dancing': null,
                'Singing': null,
                'Shopping': null,
                'Watching Movie': null,
                'Game': null,
                'Drawing': null,
                'Sleep': null,
                'Sport': null,
                'Football': null,
                'Badminton': null,
            }
        });
    });

</script>
<script>
    $(document).ready(function () {


        $('.chips-autocomplete2').material_chip({
            secondaryPlaceholder: 'Job + Enter',
            placeholder: 'Job + Enter',
            autocompleteData: {
                'Hiking': null,
                'Swimming': null,
                'Reading': null,
                'Jogging': null,
                'Travelling': null,
                'Dancing': null,
                'Singing': null,
                'Shopping': null,
                'Watching Movie': null,
                'Game': null,
                'Drawing': null,
                'Sleep': null,
                'Sport': null,
                'Football': null,
                'Badminton': null,
            }
        });
    });

</script>
