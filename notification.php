
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Notifications" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Notifications</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-loggedin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">


    <div class="new-friend-request-div">
     <p class="friend-title">New Friend Request</p>

     <div class="profile-div">
         <div class="profile-div1">
          <a href="profile.php" class="hover-a">
           <img src="img/rebecca.jpg" class="img-width-100">
         </a>
      
         </div>
         <div class="profile-div2">
           <p class="username-p2 hover-turn-blue"><a href="profile.php"><span class="username-p2 hover-turn-blue">Rebecca Ave</span></a></p>
           <p class="profile-info friend-info hover-a"><a href="profile.php"><span class="profile-info friend-info hover-a">Live in London, United Kingdom</span></a></p>
           <p class="profile-info friend-info"><a href="friend.php" class="blue-hover">200 Friends</a></p>
         </div>
         <button class="button-a accept-button"><div class="add-friend-button pointer darker-bg-hover accept-div"><span class="add-friend-span" onclick="approve_button" >Approve</span></div></button>
         <button class="button-a"><div class="add-friend-button pointer outline-div decline-div"><span class="add-friend-span unfriend-span" onclick="reject_button">Reject</span></div></button>
     </div>



     <div class="profile-div">
         <div class="profile-div1">
          <a href="profile.php" class="hover-a">
           <img src="img/rebecca.jpg" class="img-width-100">
          </a>
      
         </div>
         <div class="profile-div2">
           <p class="username-p2 hover-turn-blue"><a href="profile.php"><span class="username-p2 hover-turn-blue">Rebecca Ave</span></a></p>
           <p class="profile-info friend-info hover-a"><a href="profile.php"><span class="profile-info friend-info hover-a">Live in London, United Kingdom</span></a></p>
           <p class="profile-info friend-info"><a href="friend.php" class="blue-hover">200 Friends</a></p>
         </div>
           <button class="button-a accept-button"><div class="add-friend-button pointer darker-bg-hover accept-div"><span class="add-friend-span" onclick="approve_button" >Approve</span></div></button>
           <button class="button-a"><div class="add-friend-button pointer outline-div decline-div"><span class="add-friend-span unfriend-span" onclick="reject_button">Reject</span></div></button>
     </div>
   </div>
<!---- --->
   <p class="friend-title friend-list notis-title">Notifications</p>  
   <div class="notification-div">
    <a href="article.php">
     <p class="notification-content hover-a pointer">Joe March commented on your article titled ”The Hidden Delicious Mexican Food”.</p>
     <p class="notification-date hover-a pointer">09/05/2018 12:00</p>
    </a>
    
    <a href="article.php">
     <p class="notification-content hover-a pointer">Your article titled “Top 20 Places for Mountain Hiking” is published.</p>
     <p class="notification-date hover-a pointer">07/05/2018 12:00</p> 
    </a>  
    
    <a href="article.php">
     <p class="notification-content hover-a pointer">Your article titled “ Top 10 Places for Mountain Hiking” is rejected because the content is same with another uploaded article.</p>
     <p class="notification-date hover-a pointer">07/05/2018 11:00</p> 
    </a>      
    
  </div>
</div>

<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title">Reason of Report this Comment</h4>
        <p>
    <form action="#" id="flag-comment" name="flag-comment">
     <label>
      <input class="with-gap" name="report-comment" type="radio" checked />
      <span class="form-span">Choose Your Reason</span>
    </label>   
    
      <div class="comment-select-div form-input2">
       <select id="report-comment" name="report-comment" >
       
        <option value="It is a spam.">It is a spam.</option>
        <option value="It contains offensive words and profanity.">It contains offensive words and profanity.</option>        
        <option value="It is cyberbullying or harassment.">It is cyberbullying or harassment.</option>
        <option value="It is a hate speech.">It is a hate speech.</option> 
        <option value="It is a fake statement.">It is a fake statement.</option>
        <option value="it is a scam or misleading.">it is a scam or misleading.</option> 
        <option value="It is a personal safety threat.">It is a personal safety threat.</option>
       </select>
       </div>
       
     <label>
      <input class="with-gap" name="report-comment" type="radio" id="textareaa" />
      <span class="form-span">Write a reason</span>
      <textarea class="modal-textarea" name="report-comment"  disabled id="textareab" ></textarea>
    </label>        
       
     </form>

     <div class="centerise"><a class="waves-effect waves-light btn-large diy-blue-wave" href="#">Submit</a></div>
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave" href="#">Cancel</a></div>  
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
    </div>

<!-- Modal Structure of Confirming Unfriend-->
<div id="modal3" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title unfriend-title">Are you sure want to unfriend with this user?</h4>
        <p>
    

     
     <div class="centerise"><a class="waves-effect waves-light btn-large diy-blue-line-wave" href="#">Yes</a></div> 
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-wave" href="#">No</a></div> 
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
    </div>

<script>
var textareaa = document.querySelectorAll('input[name="report-comment"]');
var textareab = document.getElementById('textareab');
var tempAddress = "";

for(var i = 0; i < textareaa.length; i++) {
    textareaa[i].addEventListener("change", addressHandler);
}


function addressHandler() {
    if(this.id == "textareaa") {
    textareab.disabled = false;
    textareab.value = tempAddress;
  } else {
    tempAddress = textareab.value;
    textareab.value = "";
    textareab.disabled = true;
  }
}
</script>
<script>
function toggleComment() { 
    var thecomment = document.getElementById('comment');
    var displaySetting = thecomment.style.display;
    var commentButton = document.getElementById('commentButton');				
    if (displaySetting == 'block') { 
      thecomment.style.display = 'none';
      commentButton.innerHTML = '<img src="img/comment.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
    else { 
      thecomment.style.display = 'block';
      commentButton.innerHTML = '<img src="img/comment2.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
  }  
</script>



<script>
function toggleReply() { 
    var thereply = document.getElementById('reply');
    var displaySetting = thereply.style.display;
    var replyButton = document.getElementById('replyButton');				
    if (displaySetting == 'block') { 
      thereply.style.display = 'none';
      replyButton.innerHTML = 'View More Replies';
    }
    else { 
      thereply.style.display = 'block';
      replyButton.innerHTML = 'Hide the Replies';
    }
  }  
</script>

<script>
function toggleReplytwo() { 
    var thereplytwo = document.getElementById('replytwo');
    var displaySetting = thereplytwo.style.display;
    var replyButtontwo = document.getElementById('replyButtontwo');				
    if (displaySetting == 'block') { 
      thereplytwo.style.display = 'none';
    }
    else { 
      thereplytwo.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplythree() { 
    var thereplythree = document.getElementById('replythree');
    var displaySetting = thereplythree.style.display;
    var replyButtonthree = document.getElementById('replyButtonthree');				
    if (displaySetting == 'block') { 
      thereplythree.style.display = 'none';
    }
    else { 
      thereplythree.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplyfour() { 
    var thereplyfour = document.getElementById('replyfour');
    var displaySetting = thereplyfour.style.display;
    var replyButtonfour = document.getElementById('replyButtonfour');				
    if (displaySetting == 'block') { 
      thereplyfour.style.display = 'none';
      replyButtonfour.innerHTML = 'View More Comments';    }
    else { 
      thereplyfour.style.display = 'block';
      replyButtonfour.innerHTML = 'Hide Some Comments';
    }
  }  
</script>


<script>
function toggleReplyfive() { 
    var thereplyfive = document.getElementById('replyfive');
    var displaySetting = thereplyfive.style.display;
    var replyButtonfive = document.getElementById('replyButtonfive');				
    if (displaySetting == 'block') { 
      thereplyfive.style.display = 'none';
    }
    else { 
      thereplyfive.style.display = 'block';
    }
  }  
</script>


<?php include 'footer.php'; ?>
</body>
</html>
