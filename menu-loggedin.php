
<header id="header" class="header header--fixed" role="banner">
  <div class="top-div darkblue-bg width-100 overflow-hidden">
    <div class="left-logo">
      <a href="index.php"><img src="img/logo.png" class="logo-img img-width-100 "></a>
    </div>
    <div class="search-div">
      <input type="text" class="search-input menu-search-bar">
      <!--- Go to search.php-->
      <button type="submit" class="png-submit-button"><img src="img/search-white.png" class="search-png img-width-100 hover-a pointer"></button>
    </div>
    <div class="menu-link-div2 ow-normal-menu-div web-menu">
     <!---Display Publish Article, Flag, Settings and Logout only if the admin already login-->
    <!--
     
     
     <div class="link-div link-div-last"><a href="" class="white-text a-link"><img src="img/logout.png" class="no-notification"></a></div>
     <div class="link-div link-div-last"><a href="" class="white-text a-link"><img src="img/settings.png" class="no-notification"></a></div> 
     <div class="link-div link-div-last"><a href="" class="white-text a-link"><img src="img/flag.png" class="no-notification"><img src="img/got-flag.png" class="notification"></a></div> 
     <div class="link-div link-div-last"><a href="" class="white-text a-link">Publish Article</a></div> -->

     <!---Display Profile and Notification only if the user already login--> 
   
     <div class="link-div ow-link-div notis-div"><a href="notification.php" class="white-text a-link"><img src="img/no-notification.png" class="no-notification menu-icon"><img src="img/notification.png" class="notification display-none"></a></div>
     <div class="link-div ow-link-div"><a href="profile.php" class="white-text a-link">Profile</a></div> 
      


     <!---Display Register/Login only if the user not yet login-->
     <!--- <div class="link-div ow-link-div"><a href="" class="white-text a-link">Register/ Login</a></div> -->
     <div class="link-div ow-link-div"><a href="home.php" class="white-text a-link">Job</a></div>
     <div class="link-div ow-link-div"><a href="home.php" class="white-text a-link">Food</a></div>
     <div class="link-div ow-link-div"><a href="home.php" class="white-text a-link">Stay</a></div>
     <div class="link-div ow-link-div"><a href="home.php" class="white-text a-link">Travel</a></div>
     <div class="link-div ow-link-div"><a href="home.php" class="white-text a-link">Home</a></div>
    <!-- Hide this right-app div if admin login-->
    </div>
    <div class="right-app web-menu hover-a">
      <div class="mobile-div center"><a href="#"><img src="img/mobile.png" class="mobile-img"></a></div>
       <div class="mobile-text-div white-text center"><a href="#" class="white-text"> Get the App</a></div>
    </div>
    
    
    <div class="mobile-menu">
    
     <ul id="slide-out" class="sidenav">
      <li><a class="sidenav-close" href="#!"><img src="img/close.png" class="close-img"></a></li>
      <li><a class="" href="home.php">Home</a></li>
      <li><a class="" href="home.php">Travel</a></li>
      <li><a class="" href="home.php">Stay</a></li>
      <li><a class="" href="home.php">Food</a></li>  
      <li><a class="" href="home.php">Job</a></li>
      <!---Hide it if the user already logged in--->
      <!--- <li><a class="" href="login.php">Register/ Login</a></li>
      <!---Hide it if the user not yet login or the user is admin--->    
      <li><a class="" href="profile.php">Profile</a></li>
      <li><a class="" href="notification.php">Notifications</a></li>
      <!---Hide it if the user is not an admin--->
      <!--- <li><a class="" href="article-list.php">Publish Article</a></li>
      <li><a class="" href="flag.php">Flag</a></li>
      <li><a class="" href="admin-settings.php">Settings</a></li> -->  
      <li><a class="" href="">Logout</a></li>    
        
     </ul>
     <a href="#" data-target="slide-out" class="sidenav-trigger"><img src="img/menu.png" class="menu-img"></a>
    </div>
    
  </div>
</header>