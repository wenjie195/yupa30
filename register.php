
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Travel _ Free</title>
<link rel="canonical" href="https://yupa.asia/" />
    <!-- Add additional services that you want to use -->
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-functions.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>

</head>

<body>
<?php include 'menu.php'; ?>
<div class="grey-bg overflow-hidden">
    <div class="white-bg overflow-hidden">
        <div class="login-div">
            <div class="centerise"><a href="index.php" class="hover-a"><img src="img/big-logo.png" class="big-logo"></a></div>
            <p class="login-p bold">Register</p>

       <div>
           <div class="input-field input-div2">
               <input  id="first_name" type="text" class="input-font icon-input" required><img src="img/pencil1.png" class="pencil-position pencil-size pencil1">
               <label for="first_name" class="input-font">Name</label>
               <div id="div1"></div>
           </div>

           <div class="input-field input-div2">
               <input  id="email" type="email" class="input-font" required>
               <label for="email" class="input-font">Email</label>
               <div id="div2"></div>
           </div>

           <div class="input-field input-div2">
               <input  id="password" type="password" class="input-font" required>
               <label for="password" class="input-font">Password</label>
               <div id="div3"></div>
           </div>

           <div class="input-field input-div2">
               <input  id="contact_number" type="number" name="number" class="input-font" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>


               <label for="contact_number" class="input-font">Contact Number</label>
               <div id="div4"></div><br>
           </div>

           <div class="input-field input-div2">
               <label for="gender">
               </label><select id="gender">
                   <option value="Male" selected>Male</option>
                   <option value="Female">Female</option>
               </select>
               <label>Gender</label>
           </div>
  <!--   <div class="input-field input-div2">          
         <input id="dob" type="text" class="datepicker" value="01-05-1991">
         <label for="dob" class="input-font">Birthday</label>  
         </div>         -->

           <p class="spacing-p"></p>
           <div class="input-field input-div2">
               <input type="date" id="birthday"  class="icon-input" placeholder="dd/mm/yyyy" required><img src="img/cake.png" class="pencil-position pencil-size pencil1">
               <label for="birthday" class="input-font">Birthday</label>
               <div id="div5"></div>
           </div>

           <p class="spacing-p2"></p>
           <div class="input-field input-div2">
               <label for="country" class="country"></label>
               <select id="country" name="country" onchange="getState(this.value);" >
                   <?php
                   require_once('dbconn.php');
                   $sql = "SELECT * FROM countries";
                   $result = mysqli_query($conn, $sql);
                   echo "<option disabled selected>Please Select Country</option>";
                   while($row = mysqli_fetch_assoc($result)){
                   ?>
                       <option id="<?php echo $row['id'] ?>" value="<?php echo $row['name'] ?>"><?php echo $row['name'] ?></option><?php } ?>

               </select>
               <label>Country</label>
           </div>
           <div id="div5"></div>

           <p class="spacing-p"></p>

           <div class="input-field input-div2">
               <label for="state"></label>
               <select id="state" name="state">
                   <?php
                   echo "<option disabled selected>Please Select State</option>";
                   ?>
               </select>
               <label>State</label>
           </div>
           <div id="div6"></div>

           <p class="spacing-p3"></p>

           <div class="input-div2">
               <p>
                   <label>
                       <input id="checkbox" type="checkbox" class="filled-in"  />
                       <span>I want to be contacted with more information about your company's offering marketing services and consulting.</span>
                   </label>
               </p>
           </div>

           <div class="input-div2">
               <p class="agree-p center">By signing up you agree with our <a href="" class="blue-hover">Terms of Service</a> and <a href="" class="blue-hover">Privacy Policy</a></p>
           </div>
           <button class="login-input-width blue-login pointer darker-bg-hover"  onclick="Register_btn();">Register</button>

           <p class="register-here-p centerise">Already register? Login <a href="login.php" class="here-a blue-hover">here.</a></p>
       </div>
        </div>
    </div>
    <script>

        $(document).ready(function()
        {
            var config = {
                apiKey: "AIzaSyClABGbMDQlo5XtA2QxAisefXL9a6-Z8xc",
                authDomain: "yupa-app.firebaseapp.com",
                databaseURL: "https://yupa-app.firebaseio.com",
                storageBucket: "yupa-app.appspot.com",
                messagingSenderId: "25656510649",
                projectId: "yupa-app"
            };
            firebase.initializeApp(config);

        });

        function getState(){
            var countryId = $('#country option:selected').attr("id");
//            alert(countryId);
            clearState();
            $.ajax({
                type: "POST",
                url: "SearchState.php",
                data: {countryId : countryId},
                success :  function(data){
                    console.log(data);
                    var result = jQuery.parseJSON(data);
                    for(var i = 0; i<result.length;i++){
                        console.log(result[i]);
                        $('#state').append('<option value="'+result[i]+'">'+result[i]+'</option>');
                    }
                    initiateSelect();
                    //append state list
                },
                error: function(error){console.log(error);
                }
            });
//            $.ajax({
//                type: "POST",
//                url: "SearchNumber.php",
//                data: {countryId : countryId},
//                success :  function(data){
//                    console.log(data);
//                    var result = jQuery.parseJSON(data);
//                    for(var i = 0; i<result.length;i++){
//                        console.log(result[i]);
//                        $('#contact_number').append('<option value="'+result[i]+'">'+result[i]+'</option>');
//                    }
//                    initiateSelect();
//                    //append state list
//                },
//                error: function(error){console.log(error);
//                }
//            });

        }
        // Different select option, this is a plugin, so after append the option to the select --
        // need to call this function again to initiate, else after append option, the option not show in select
        function initiateSelect(){
            document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('select');
                var instances = M.FormSelect.init(elems, options);
            });
            // Or with jQuery
            $(document).ready(function(){
                $('select').formSelect();
            });
        }
        function clearState() {
            $('#state')
                .find('option')
                .remove()
                .end()
                .append('<option disabled selected>Please Select State</option>');
        }

            function Register_btn() {
                var db;
                db = firebase.firestore();
                db.settings({timestampsInSnapshots: true});

                const name = document.getElementById("first_name").value;
                const email = document.getElementById("email").value;
                const password = document.getElementById("password").value;
                const contact_number = document.getElementById("contact_number").value;
                const gender = document.getElementById("gender").value;
                const birthday = document.getElementById("birthday").value;
                var saperateValue = birthday.split("-");
                const country = document.getElementById("country").value;
                const state = document.getElementById("state").value;
                const check_box = document.getElementById("checkbox").checked;

                var complete = true;

                if (name === "") {
                    document.getElementById("div1").innerHTML = "Enter your username*";
                    document.getElementById("first_name").style.borderColor = "red";
                    document.getElementById("div1").style.color = "red";
                    complete = false;
                    return;
                } else {
                    document.getElementById("div1").style.display = "none";
                    document.getElementById("first_name").style.borderColor = "black";
                }
                if (email === "") {
                    document.getElementById("div2").innerHTML = "Enter your email*";
                    document.getElementById("email").style.borderColor = "red";
                    document.getElementById("div2").style.color = "red";
                    complete = false;
                    return;
                } else {
                    document.getElementById("div2").style.display = "none";
                    document.getElementById("email").style.borderColor = "black";
                }
                if (password === "") {
                    document.getElementById("div3").innerHTML = "Enter your Password*";
                    document.getElementById("password").style.borderColor = "red";
                    document.getElementById("div3").style.color = "red";
                    complete = false;
                    return;
                } else if (password.length < 5) {
                    document.getElementById("div3").innerHTML = "Password at least must have 5 words*";
                    document.getElementById("password").style.borderColor = "red";
                    document.getElementById("div3").style.color = "red";
                    complete = false;
                    return;
                } else {
                    document.getElementById("div3").style.display = "none";
                    document.getElementById("password").style.borderColor = "black";
                }

                if (contact_number === "") {
                    document.getElementById("div4").innerHTML = "Enter your contact number*";
                    document.getElementById("contact_number").style.borderColor = "red";
                    document.getElementById("div4").style.color = "red";
                    complete = false;
                    return;
                } else if (contact_number.length < 10) {
                    document.getElementById("div4").innerHTML = "Contact number at least must have 10 numbers!*";
                    document.getElementById("contact_number").style.borderColor = "red";
                    document.getElementById("div4").style.color = "red";
                    complete = false;
                    return;
                } else {
                    document.getElementById("div4").style.display = "none";
                    document.getElementById("contact_number").style.borderColor = "black";
                }
                if (birthday === "") {
                    document.getElementById("div5").innerHTML = "Enter your birthday date*";
                    document.getElementById("birthday").style.borderColor = "red";
                    document.getElementById("div5").style.color = "red";
                    complete = false;
                    return;
                } else {
                    document.getElementById("div5").style.display = "none";
                    document.getElementById("birthday").style.borderColor = "black";
                }
//--------------------------------------------------------------------------------------------------------------------------//
                if (complete) {
                    $.ajax({
                        type: "POST",
                        url: "check_country.php",
                        data: {country_name : country},
                        dataType:"json",
                        success :  function(data){
                           console.log(data);
                           if(data === "Exist"){
//                               alert("country Yes");

                               //****************add need ajax for check_state*********************//
                               $.ajax({
                                   type: "POST",
                                   url: "check_state.php",
                                   data: {state_name : state},
                                   dataType: "json",
                                   success: function(data){
                                       console.log(data);
                                       if(data === "Exist"){
//                                           alert("state Yes");
                                           storeUserDetailToFirestore(db,name,email,password,contact_number,gender,saperateValue[2],saperateValue[1],saperateValue[0],country,state,check_box);
                                       }else{
                                           alert("state no");
                                       }
                                   }
                               });
                               //*****************************************************************//
                           }else{
                               alert("no");
                           }
                        },
                        error: function(error){console.log(error);
                        }
                    });
                }
            }

//---------------------------------------------------------------------------------------------------------------------------//

            function storeUserDetailToFirestore(db,name,email,password,contact_number,gender,dobDay,dobMonth,dobYear,country,state,check_box){
                firebase.auth().onAuthStateChanged(function (isUserExist) {
                    var user;
                    if (!isUserExist) {
                        console.log("go to login page");
                        alert("User is not signed in");
                    } else {
                        user = firebase.auth().currentUser;

                        console.log(user.uid);
                        alert(user.uid);

                        console.log("User is signed in");
                        alert("sign-in");

                        firebase.auth().currentUser.sendEmailVerification().then(function () {
                            var actionCodeSettings = {
                                url: 'https://yupa-app.firebaseio.com',
                                handleCodeInApp: true,
                                iOS: {
//                                bundleId: 'com.example.ios'
                                    bundleId: 'com.apple.mobilemail'
                                },
                                android: {
                                    packageName: 'com.vidatechft.yupa',
                                    installApp: true,
                                    minimumVersion: '12'
                                }
                            };
                            firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings).then(function () {
                                window.localStorage.setItem('emailForSignIn', email);
                            })
                                .catch(function (error) {
                                });
                        });
                        db.collection("tester").doc(user.uid).set(
                            {
                                name: name,
                                email: email,
                                contactNo: contact_number,
                                gender: gender,
                                dobDay: dobDay,
                                dobMonth: dobMonth,
                                dobYear: dobYear,
                                country: country,
                                state: state,
                                canContact: check_box,
                                id: user.uid,
                                dateCreated: firebase.firestore.FieldValue.serverTimestamp(),
                                dateUpdated: firebase.firestore.FieldValue.serverTimestamp()
                            })
                            .then(function () {
                                console.log("Document successfully written!");

                                if (user) {
                                     user = firebase.auth().currentUser;
                                    firebase.auth().signOut().then(function() {
                                        alert("Account have been Log-Out");
                                        alert("Please Check your Email for verify to Active Your Account");
                                        window.location = 'login.php';
                                    }).catch(function(error) {
                                        alert(error);
                                    });
                                }
                            })
                            .catch(function (error) {
                                console.error("Error writing document: ", error);
                            });
                        alert('Please verify your E-mail! Verification code have Sent!');
                    }
                });
                firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    if (errorCode === 'auth/weak-password') {
                        alert('The password is too weak.');
                    } else {
                        alert(errorMessage);
                    }
                });
            }

    </script>

</div>
<?php include 'footer.php'; ?>

</body>
</html>
