//from here https://stackoverflow.com/questions/14343965/google-places-library-without-map

var serviceForGooglePlacesApi;

//********************************************init the google places library with callback**************************************************//
//this is old way of getting the service api, sometimes got error 1 on 1st time load
// window.getPlaceDetailsCallback = function(){
//     serviceForGooglePlacesApi = new google.maps.places.PlacesService($('#serviceForGooglePlacesApi-helper').get(0)); // note that it removes the content inside div with tag '#serviceForGooglePlacesApi-helper'
//
//     // Create the autocomplete object, restricting the search to geographical
//     // location types.
// };

//this is new way
function getPlaceDetailsCallback() {
    let map = new google.maps.Map(document.createElement('div'));
    serviceForGooglePlacesApi = new google.maps.places.PlacesService(map);
}
google.maps.event.addDomListener(window, "load", getPlaceDetailsCallback);

//*******************************************this is the unify place address functions*****************************************************//

function getAddressComponents(placeAddress,addressComponents){
    for(var x = 0; x < addressComponents.length; x++){
        for(var y = 0; y < addressComponents[x].types.length; y++){
            var eachAddressComponentsTypes = addressComponents[x].types[y];
            switch(eachAddressComponentsTypes){
                case "country":
                    placeAddress['country'] = addressComponents[x].long_name;
                    break;
                case "administrative_area_level_1":
                    placeAddress['administrative_area_level_1'] = addressComponents[x].long_name;
                    break;
                case "administrative_area_level_2":
                    placeAddress['administrative_area_level_2'] = addressComponents[x].long_name;
                    break;
                case "administrative_area_level_3":
                    placeAddress['administrative_area_level_3'] = addressComponents[x].long_name;
                    break;
                case "administrative_area_level_4":
                    placeAddress['administrative_area_level_4'] = addressComponents[x].long_name;
                    break;
                case "administrative_area_level_5":
                    placeAddress['administrative_area_level_5'] = addressComponents[x].long_name;
                    break;
                case "locality":
                    placeAddress['locality'] = addressComponents[x].long_name;
                    break;
                case "sublocality":
                    if( eachAddressComponentsTypes.sublocality_level_1 ||
                        eachAddressComponentsTypes.sublocality_level_2 ||
                        eachAddressComponentsTypes.sublocality_level_3 ||
                        eachAddressComponentsTypes.sublocality_level_4 ||
                        eachAddressComponentsTypes.sublocality_level_5
                    ){
                        //if any one of it has, dont put sublocality
                    }else{
                        //if all also dont have then put into sublocality
                        placeAddress['sublocality'] = addressComponents[x].long_name;
                    }
                    break;
                case "sublocality_level_1":
                    placeAddress['sublocality_level_1'] = addressComponents[x].long_name;
                    break;
                case "sublocality_level_2":
                    placeAddress['sublocality_level_2'] = addressComponents[x].long_name;
                    break;
                case "sublocality_level_3":
                    placeAddress['sublocality_level_3'] = addressComponents[x].long_name;
                    break;
                case "sublocality_level_4":
                    placeAddress['sublocality_level_4'] = addressComponents[x].long_name;
                    break;
                case "sublocality_level_5":
                    placeAddress['sublocality_level_5'] = addressComponents[x].long_name;
                    break;
                case "street_number":
                    placeAddress['street_number'] = addressComponents[x].long_name;
                    break;
                case "route":
                    placeAddress['route'] = addressComponents[x].long_name;
                    break;
                case "street_address":
                    placeAddress['street_address'] = addressComponents[x].long_name;
                    break;
                case "premise":
                    placeAddress['premise'] = addressComponents[x].long_name;
                    break;
                case "subpremise":
                    placeAddress['subpremise'] = addressComponents[x].long_name;
                    break;
                case "postal_code":
                    placeAddress['postal_code'] = addressComponents[x].long_name;
                    break;
            }
        }
    }
    return placeAddress;
}

function getPlaceDetails(index){
    if(allPlans[index].placeAddress && allPlans[index].placeAddress.placeId && allPlans[currentPlanIndex]["day"] <= currentLastDay){
        serviceForGooglePlacesApi.getDetails({
            placeId: allPlans[index].placeAddress.placeId  // get a placeId using https://developers.google.com/places/web-service/place-id
        }, function(place, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                // var resultcontent = '';
                // for (i=0; i<place.reviews.length; ++i) {
                //     //window.alert('Name:' + place.name + '. ID: ' + place.place_id + '. address: ' + place.formatted_address);
                //     resultcontent += '<li class="reviews__item">'
                //     resultcontent += '<div class="reviews__review-er">' + place.reviews[i].author_name + '</div>';
                //     var reviewDate = new Date(place.reviews[i].time * 1000);
                //     var reviewDateMM = reviewDate.getMonth() + 1;
                //     var reviewDateFormatted = reviewDate.getDate() + '/' + reviewDateMM + '/' + reviewDate.getFullYear();
                //     resultcontent += '<div class="reviews__review-date">' + reviewDateFormatted + '</div>';
                //     resultcontent += '<div class="reviews__review-rating reviews__review-rating--' + place.reviews[i].rating +'"></div>';
                //     if (!!place.reviews[i].text){
                //         resultcontent += '<div class="reviews__review-comment">' + place.reviews[i].text + '</div>';
                //     }
                //     resultcontent += '</li>'
                // }
                // $('#reviews__content').append(resultcontent);

                allPlans[index]['placeAddress']['placeName'] = place.name;
                allPlans[index]['placeAddress']['lat'] = place.geometry.location.lat();
                allPlans[index]['placeAddress']['lng'] = place.geometry.location.lng();
                allPlans[index]['placeAddress'] = getAddressComponents(allPlans[index]['placeAddress'],place.address_components);

                getSublocality(index);
                getLocality(index);
                getAdministrativeArea(index);
            }else{
                allPlans[index]['hasError'] = true;
                allPlans[index]['isAllPlaceAddressUnified'][0] = true;
                allPlans[index]['isAllPlaceAddressUnified'][1] = true;
                allPlans[index]['isAllPlaceAddressUnified'][2] = true;
                console.log("getDetails: ",status);
                alert("Some error occurred while getting the place details");
            }
        });
    }else{
        allPlans[index]['hasError'] = false;
        allPlans[index]['isAllPlaceAddressUnified'][0] = true;
        allPlans[index]['isAllPlaceAddressUnified'][1] = true;
        allPlans[index]['isAllPlaceAddressUnified'][2] = true;

        console.log("this index: " + index + " is null");
    }

}

function getSublocality(index){
    var placeAddress = allPlans[index].placeAddress;

    var combinedSublocality = "";

    if(placeAddress.sublocality){
        combinedSublocality += " ";
        combinedSublocality += placeAddress.sublocality;
        combinedSublocality += " ";
    }

    if(placeAddress.sublocality_level_1){
        combinedSublocality += " ";
        combinedSublocality += placeAddress.sublocality_level_1;
        combinedSublocality += " ";
    }

    if(placeAddress.sublocality_level_2){
        combinedSublocality += " ";
        combinedSublocality += placeAddress.sublocality_level_2;
        combinedSublocality += " ";
    }

    if(placeAddress.sublocality_level_3){
        combinedSublocality += " ";
        combinedSublocality += placeAddress.sublocality_level_3;
        combinedSublocality += " ";
    }

    if(placeAddress.sublocality_level_4){
        combinedSublocality += " ";
        combinedSublocality += placeAddress.sublocality_level_4;
        combinedSublocality += " ";
    }

    if(placeAddress.sublocality_level_5){
        combinedSublocality += " ";
        combinedSublocality += placeAddress.sublocality_level_5;
        combinedSublocality += " ";
    }

    if(combinedSublocality.trim().length > 0){
        if(placeAddress.administrative_area_level_1){
            combinedSublocality += " ";
            combinedSublocality += placeAddress.administrative_area_level_1;
            combinedSublocality += " ";
        }

        if(placeAddress.country){
            combinedSublocality += " ";
            combinedSublocality += placeAddress.country;
            combinedSublocality += " ";
        }

        //HERE GET THE PLACE ID FIRST
        serviceForGooglePlacesApi.findPlaceFromQuery({
            query: combinedSublocality,
            fields: ['place_id']
        }, function(results,status){
            if (status === google.maps.places.PlacesServiceStatus.OK && results[0]) {
                //HERE USE THE PLACE ID TO GET THE ADDRESS COMPONENTS
                serviceForGooglePlacesApi.getDetails({
                    placeId: results[0].place_id  // get a placeId using https://developers.google.com/places/web-service/place-id
                }, function(place, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK && place && place.address_components) {
                        var tempPlaceAddress = {};
                        tempPlaceAddress = getAddressComponents(tempPlaceAddress,place.address_components);//findPlaceFromQuery
                        allPlans[index]['placeAddress']['sublocality'] = tempPlaceAddress.sublocality;
                        allPlans[index]['placeAddress']['sublocality_level_1'] = tempPlaceAddress.sublocality_level_1;
                        allPlans[index]['placeAddress']['sublocality_level_2'] = tempPlaceAddress.sublocality_level_2;
                        allPlans[index]['placeAddress']['sublocality_level_3'] = tempPlaceAddress.sublocality_level_3;
                        allPlans[index]['placeAddress']['sublocality_level_4'] = tempPlaceAddress.sublocality_level_4;
                        allPlans[index]['placeAddress']['sublocality_level_5'] = tempPlaceAddress.sublocality_level_5;

                        allPlans[index]['isAllPlaceAddressUnified'][0] = true;
                    }else{
                        console.log("getDetails Sublocality: ",status);
                        getSublocalityRetryWithAutoComplete(index,combinedSublocality);
                    }
                });
            }else{
                console.log("findPlaceFromQuery sublocality: ",status);
                getSublocalityRetryWithAutoComplete(index,combinedSublocality);
            }
        });
    }else{
        allPlans[index]['isAllPlaceAddressUnified'][0] = true;
    }
}

function getSublocalityRetryWithAutoComplete(index,combinedSublocality){
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getQueryPredictions({
        input: combinedSublocality
    }, function(predictions,status){
        if (status === google.maps.places.PlacesServiceStatus.OK && predictions[0]) {
            serviceForGooglePlacesApi.getDetails({
                placeId: predictions[0].place_id  // get a placeId using https://developers.google.com/places/web-service/place-id
            }, function(place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK && place && place.address_components) {
                    var tempPlaceAddress = {};
                    tempPlaceAddress = getAddressComponents(tempPlaceAddress,place.address_components);//findPlaceFromQuery
                    allPlans[index]['placeAddress']['sublocality'] = tempPlaceAddress.sublocality;
                    allPlans[index]['placeAddress']['sublocality_level_1'] = tempPlaceAddress.sublocality_level_1;
                    allPlans[index]['placeAddress']['sublocality_level_2'] = tempPlaceAddress.sublocality_level_2;
                    allPlans[index]['placeAddress']['sublocality_level_3'] = tempPlaceAddress.sublocality_level_3;
                    allPlans[index]['placeAddress']['sublocality_level_4'] = tempPlaceAddress.sublocality_level_4;
                    allPlans[index]['placeAddress']['sublocality_level_5'] = tempPlaceAddress.sublocality_level_5;

                    allPlans[index]['isAllPlaceAddressUnified'][0] = true;
                }else{
                    //HAS ERROR
                    console.log("getDetails Sublocality retry: ",status);
                    allPlans[index]['hasError'] = true;
                    allPlans[index]['isAllPlaceAddressUnified'][0] = true;
                }
            });
        }else{
            //HAS ERROR
            console.log("getAutocomplete Sublocality retry: ",status);
            allPlans[index]['hasError'] = true;
            allPlans[index]['isAllPlaceAddressUnified'][0] = true;
        }
    });
}

function getLocality(index){
    var placeAddress = allPlans[index].placeAddress;

    if(placeAddress.locality && placeAddress.locality.trim().length > 0){
        var combinedLocality = " ";
        combinedLocality += placeAddress.locality;

        if(placeAddress.administrative_area_level_1){
            combinedLocality += " ";
            combinedLocality += placeAddress.administrative_area_level_1;
            combinedLocality += " ";
        }

        if(placeAddress.country){
            combinedLocality += " ";
            combinedLocality += placeAddress.country;
            combinedLocality += " ";
        }

        //HERE GET THE PLACE ID FIRST
        serviceForGooglePlacesApi.findPlaceFromQuery({
            query: combinedLocality,
            fields: ['place_id']
        }, function(results,status){
            if (status === google.maps.places.PlacesServiceStatus.OK && results[0]) {
                //HERE USE THE PLACE ID TO GET THE ADDRESS COMPONENTS
                serviceForGooglePlacesApi.getDetails({
                    placeId: results[0].place_id  // get a placeId using https://developers.google.com/places/web-service/place-id
                }, function(place, status) {
                    // console.log("locality");
                    // console.log(place);
                    if (status === google.maps.places.PlacesServiceStatus.OK && place && place.address_components) {
                        var tempPlaceAddress = {};
                        tempPlaceAddress = getAddressComponents(tempPlaceAddress,place.address_components);//findPlaceFromQuery
                        allPlans[index]['placeAddress']['locality'] = tempPlaceAddress.locality;

                        allPlans[index]['isAllPlaceAddressUnified'][1] = true;
                    }else{
                        console.log("getDetails locality: ",status);
                        getLocalityRetryWithAutoComplete(index,combinedLocality);
                    }
                });
            }else{
                console.log("findPlaceFromQuery locality: ",status);
                getLocalityRetryWithAutoComplete(index,combinedLocality);
            }
        });
    }else{
        allPlans[index]['isAllPlaceAddressUnified'][1] = true;
    }
}

function getLocalityRetryWithAutoComplete(index,combinedLocality){
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getQueryPredictions({
        input: combinedLocality
    }, function(predictions,status){
        if (status === google.maps.places.PlacesServiceStatus.OK && predictions[0]) {
            serviceForGooglePlacesApi.getDetails({
                placeId: predictions[0].place_id  // get a placeId using https://developers.google.com/places/web-service/place-id
            }, function(place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK && place && place.address_components) {
                    var tempPlaceAddress = {};
                    tempPlaceAddress = getAddressComponents(tempPlaceAddress,place.address_components);//findPlaceFromQuery
                    allPlans[index]['placeAddress']['locality'] = tempPlaceAddress.locality;

                    allPlans[index]['isAllPlaceAddressUnified'][1] = true;
                }else{
                    //HAS ERROR
                    console.log("getDetails locality retry: ",status);
                    allPlans[index]['hasError'] = true;
                    allPlans[index]['isAllPlaceAddressUnified'][1] = true;
                }
            });
        }else{
            //HAS ERROR
            console.log("getAutocomplete locality retry: ",status);
            allPlans[index]['hasError'] = true;
            allPlans[index]['isAllPlaceAddressUnified'][1] = true;
        }
    });
}

function getAdministrativeArea(index){
    var placeAddress = allPlans[index].placeAddress;

    var combinedAdminstrativeArea = "";

    if(placeAddress.administrative_area_level_1){
        combinedAdminstrativeArea += " ";
        combinedAdminstrativeArea += placeAddress.administrative_area_level_1;
        combinedAdminstrativeArea += " ";
    }

    if(placeAddress.administrative_area_level_2){
        combinedAdminstrativeArea += " ";
        combinedAdminstrativeArea += placeAddress.administrative_area_level_2;
        combinedAdminstrativeArea += " ";
    }

    if(placeAddress.administrative_area_level_3){
        combinedAdminstrativeArea += " ";
        combinedAdminstrativeArea += placeAddress.administrative_area_level_3;
        combinedAdminstrativeArea += " ";
    }

    if(placeAddress.administrative_area_level_4){
        combinedAdminstrativeArea += " ";
        combinedAdminstrativeArea += placeAddress.administrative_area_level_4;
        combinedAdminstrativeArea += " ";
    }

    if(placeAddress.administrative_area_level_5){
        combinedAdminstrativeArea += " ";
        combinedAdminstrativeArea += placeAddress.administrative_area_level_5;
        combinedAdminstrativeArea += " ";
    }

    if(combinedAdminstrativeArea.trim().length > 0){

        if(placeAddress.country){
            combinedAdminstrativeArea += " ";
            combinedAdminstrativeArea += placeAddress.country;
            combinedAdminstrativeArea += " ";
        }

        //HERE GET THE PLACE ID FIRST
        serviceForGooglePlacesApi.findPlaceFromQuery({
            query: combinedAdminstrativeArea,
            fields: ['place_id']
        }, function(results,status){
            if (status === google.maps.places.PlacesServiceStatus.OK && results[0]) {
                //HERE USE THE PLACE ID TO GET THE ADDRESS COMPONENTS
                serviceForGooglePlacesApi.getDetails({
                    placeId: results[0].place_id  // get a placeId using https://developers.google.com/places/web-service/place-id
                }, function(place, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK && place && place.address_components) {
                        var tempPlaceAddress = {};
                        tempPlaceAddress = getAddressComponents(tempPlaceAddress,place.address_components);//findPlaceFromQuery
                        allPlans[index]['placeAddress']['administrative_area_level_1'] = tempPlaceAddress.administrative_area_level_1;
                        allPlans[index]['placeAddress']['administrative_area_level_2'] = tempPlaceAddress.administrative_area_level_2;
                        allPlans[index]['placeAddress']['administrative_area_level_3'] = tempPlaceAddress.administrative_area_level_3;
                        allPlans[index]['placeAddress']['administrative_area_level_4'] = tempPlaceAddress.administrative_area_level_4;
                        allPlans[index]['placeAddress']['administrative_area_level_5'] = tempPlaceAddress.administrative_area_level_5;
                        allPlans[index]['placeAddress']['country'] = tempPlaceAddress.country;

                        allPlans[index]['isAllPlaceAddressUnified'][2] = true;
                    }else{
                        getAdministrativeAreaRetryWithAutoComplete(index,combinedAdminstrativeArea);
                        console.log("getDetails administrative area: ",status);
                    }
                });
            }else{
                console.log("findPlaceFromQuery administrative area: ",status);
                getAdministrativeAreaRetryWithAutoComplete(index,combinedAdminstrativeArea);
            }
        });
    }else{
        allPlans[index]['isAllPlaceAddressUnified'][2] = true;
    }
}

function getAdministrativeAreaRetryWithAutoComplete(index,combinedAdminstrativeArea){
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getQueryPredictions({
        input: combinedAdminstrativeArea
    }, function(predictions,status){
        if (status === google.maps.places.PlacesServiceStatus.OK && predictions[0]) {
            serviceForGooglePlacesApi.getDetails({
                placeId: predictions[0].place_id  // get a placeId using https://developers.google.com/places/web-service/place-id
            }, function(place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK && place && place.address_components) {
                    var tempPlaceAddress = {};
                    tempPlaceAddress = getAddressComponents(tempPlaceAddress,place.address_components);//findPlaceFromQuery
                    allPlans[index]['placeAddress']['administrative_area_level_1'] = tempPlaceAddress.administrative_area_level_1;
                    allPlans[index]['placeAddress']['administrative_area_level_2'] = tempPlaceAddress.administrative_area_level_2;
                    allPlans[index]['placeAddress']['administrative_area_level_3'] = tempPlaceAddress.administrative_area_level_3;
                    allPlans[index]['placeAddress']['administrative_area_level_4'] = tempPlaceAddress.administrative_area_level_4;
                    allPlans[index]['placeAddress']['administrative_area_level_5'] = tempPlaceAddress.administrative_area_level_5;
                    allPlans[index]['placeAddress']['country'] = tempPlaceAddress.country;

                    allPlans[index]['isAllPlaceAddressUnified'][2] = true;
                }else{
                    //HAS ERROR
                    console.log("getDetails administrative area retry: ",status);
                    allPlans[index]['hasError'] = true;
                    allPlans[index]['isAllPlaceAddressUnified'][2] = true;
                }
            });
        }else{
            //HAS ERROR
            console.log("getAutocomplete administrative area retry: ",status);
            allPlans[index]['hasError'] = true;
            allPlans[index]['isAllPlaceAddressUnified'][2] = true;
        }
    });
}

function getPlaceIndex(placeAddress){
    var placeIndex = {};

    if(placeAddress.country){
        placeIndex[placeAddress.country] = true;
    }

    if(placeAddress.administrative_area_level_1){
        placeIndex[placeAddress.administrative_area_level_1] = true;
    }

    if(placeAddress.administrative_area_level_2){
        placeIndex[placeAddress.administrative_area_level_2] = true;
    }

    if(placeAddress.administrative_area_level_3){
        placeIndex[placeAddress.administrative_area_level_3] = true;

    }

    if(placeAddress.administrative_area_level_4){
        placeIndex[placeAddress.administrative_area_level_4] = true;

    }

    if(placeAddress.administrative_area_level_5){
        placeIndex[placeAddress.administrative_area_level_5] = true;

    }

    if(placeAddress.locality){
        placeIndex[placeAddress.locality] = true;
    }

    if(placeAddress.sublocality){
        placeIndex[placeAddress.sublocality] = true;
    }

    if(placeAddress.sublocality_level_1){
        placeIndex[placeAddress.sublocality_level_1] = true;
    }

    if(placeAddress.sublocality_level_2){
        placeIndex[placeAddress.sublocality_level_2] = true;
    }

    if(placeAddress.sublocality_level_3){
        placeIndex[placeAddress.sublocality_level_3] = true;
    }

    if(placeAddress.sublocality_level_4){
        placeIndex[placeAddress.sublocality_level_4] = true;
    }

    if(placeAddress.sublocality_level_5){
        placeIndex[placeAddress.sublocality_level_5] = true;
    }

    if(placeAddress.street_number){
        placeIndex[placeAddress.street_number] = true;
    }

    if(placeAddress.route){
        placeIndex[placeAddress.route] = true;
    }

    if(placeAddress.street_address){
        placeIndex[placeAddress.street_address] = true;
    }

    if(placeAddress.premise){
        placeIndex[placeAddress.premise] = true;
    }

    if(placeAddress.subpremise){
        placeIndex[placeAddress.subpremise] = true;
    }

    if(placeAddress.postal_code){
        placeIndex[placeAddress.postal_code] = true;
    }

    return placeIndex;
}

function findPlaceFromQuery(){
    //i think this is same as this findplacefromtext in android
    serviceForGooglePlacesApi.findPlaceFromQuery({
        query: "akihabara japan",
        fields: ['place_id']
    }, function(results,status){
        if (status === google.maps.places.PlacesServiceStatus.OK && results[0]) {
            console.log(results);
        }else{
            console.log("findPlaceFromQuery: ",status);
        }
    });
}

function searchRecommendedPlace(index){
    serviceForGooglePlacesApi.textSearch({
        query: 'penang famous'
    }, function(results,status){
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                var place = results[i];
                // console.log("find place from text");
                // console.log(place);
                // console.log(status);
            }
        }else{
            console.log("textSearch: ",status);
        }
    });
}

function searchAutoComplete(index){
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getQueryPredictions({
        input: 'Bandar Bayan Baru, Penang Malaysia'
    }, function(predictions,status){
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < predictions.length; i++) {
                var prediction = predictions[i];
                // console.log("predictions");
                // console.log(prediction);
                // console.log(status);
            }
            console.log(predictions);
        }else{
            console.log("textSearch: ",status);
        }
    });
}

function checkForAllPlaceAddressUnificationDoneEveryThreeSec(){
    console.log("still looking" + currentPlanIndex);

    var isAllDone = true;

    if(allPlans[currentPlanIndex]){
        var isCurrentPlanDone = true;
        for(var x = 0; x < 3; x++){
            //means this plan havent finished yet
            if(allPlans[currentPlanIndex]['isAllPlaceAddressUnified'][x] === true){

            }else{
                isCurrentPlanDone = false;
                isAllDone = false;
                break;
            }
        }

        if(isCurrentPlanDone){
            currentPlanIndex++;
            if(allPlans[currentPlanIndex]){
                isAllDone = false;
            }else{
                isAllDone = true;
            }
        }else{
            isAllDone = false;
        }
    }else{
        isAllDone = true;
    }

    if(isAllDone){
        for(var i = 0; i < allPlans.length; i++){
            if(allPlans[i].hasError === true){
                alert("Failed to get place details of this plan: " + allPlans[i].activityName + " but it is still added to the database");
                //or dont let him upload to the database of affected plans that failed to get place address and the plans that successfully get the place address should be added to the database
            }

            allPlans[i]["placeIndex"] = getPlaceIndex(allPlans[i]["placeAddress"]);
            delete allPlans[i]["isAllPlaceAddressUnified"];
            delete allPlans[i]["hasError"];

            if(! allPlans[i]["placeAddress"]["country"] ){
                allPlans[i]["placeAddress"]["country"] = null;
            }

            if(! allPlans[i]["placeAddress"]["administrative_area_level_1"] ){
                allPlans[i]["placeAddress"]["administrative_area_level_1"] = null;
            }

            if(! allPlans[i]["placeAddress"]["administrative_area_level_2"] ){
                allPlans[i]["placeAddress"]["administrative_area_level_2"] = null;
            }

            if(! allPlans[i]["placeAddress"]["administrative_area_level_3"]){
                allPlans[i]["placeAddress"]["administrative_area_level_3"] = null;

            }

            if(! allPlans[i]["placeAddress"]["administrative_area_level_4"] ){
                allPlans[i]["placeAddress"]["administrative_area_level_4"] = null;

            }

            if(! allPlans[i]["placeAddress"]["administrative_area_level_5"] ){
                allPlans[i]["placeAddress"]["administrative_area_level_5"] = null;

            }

            if(! allPlans[i]["placeAddress"]["locality"] ){
                allPlans[i]["placeAddress"]["locality"] = null;
            }

            if(! allPlans[i]["placeAddress"]["sublocality"] ){
                allPlans[i]["placeAddress"]["sublocality"] = null;
            }

            if(! allPlans[i]["placeAddress"]["sublocality_level_1"] ){
                allPlans[i]["placeAddress"]["sublocality_level_1"] = null;
            }

            if(! allPlans[i]["placeAddress"]["sublocality_level_2"] ){
                allPlans[i]["placeAddress"]["sublocality_level_2"] = null;
            }

            if(! allPlans[i]["placeAddress"]["sublocality_level_3"] ){
                allPlans[i]["placeAddress"]["sublocality_level_3"] = null;
            }

            if(! allPlans[i]["placeAddress"]["sublocality_level_4"] ){
                allPlans[i]["placeAddress"]["sublocality_level_4"] = null;
            }

            if(! allPlans[i]["placeAddress"]["sublocality_level_5"] ){
                allPlans[i]["placeAddress"]["sublocality_level_5"] = null;
            }

            if(! allPlans[i]["placeAddress"]["street_number"]){
                allPlans[i]["placeAddress"]["street_number"] = null;
            }

            if(! allPlans[i]["placeAddress"]["route"]){
                allPlans[i]["placeAddress"]["route"] = null;
            }

            if(! allPlans[i]["placeAddress"]["street_address"] ){
                allPlans[i]["placeAddress"]["street_address"] = null;
            }

            if(! allPlans[i]["placeAddress"]["premise"] ){
                allPlans[i]["placeAddress"]["premise"] = null;
            }

            if(! allPlans[i]["placeAddress"]["subpremise"] ){
                allPlans[i]["placeAddress"]["subpremise"] = null;
            }

            if(! allPlans[i]["placeAddress"]["postal_code"] ){
                allPlans[i]["placeAddress"]["postal_code"] = null;
            }

            unifiedAllPlans = allPlans;
        }

        if(placeInterval){
            clearInterval(placeInterval);
        }
    }else{
        setTimeout(function () {
            if(isThisPlanExecuted[currentPlanIndex] && isThisPlanExecuted[currentPlanIndex] === true){

            }else{
                isThisPlanExecuted[currentPlanIndex] = true;
                if(allPlans[currentPlanIndex]){
                    getPlaceDetails(currentPlanIndex);
                }
            }
        }, 3000);
    }
}

//**************************************************************************************************************************************//

//*********************************************this is the auto complete text box's function********************************************//

onPlaceAutocompleteItemSelected = function (autocomplete,element,getTheDay) {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    console.log("Element id : " + element.id + " ");
    var index = element.id.split('-')[1]; // has to be location-[index]-day-[day]

    // if (typeof allPlans === "undefined") {
    //     allPlans = [];
    //     allPlans[index] = [];
    // }

    console.log(index);
    console.log(allPlans);
    // allPlans[index] = [];
    allPlans[index]['placeAddress'] = {};
    // allPlans[index]['activityName'] = "akihabara only";
    allPlans[index]['placeAddress']['placeId'] = place.place_id;
    // allPlans[index]['day'] = getTheDay;

    // let latitude = place.geometry.location.lat();
    // let longtitude = place.geometry.location.lng();
    // console.log("Unify "+ latitude + longtitude + " Unify Day : " + getTheDay);
    // console.log(JSON.stringify(allPlans[index]['placeIndex']));
};

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(function(position) {
    //         var geolocation = {
    //             lat: position.coords.latitude,
    //             lng: position.coords.longitude
    //         };
    //         var circle = new google.maps.Circle({
    //             center: geolocation,
    //             radius: position.coords.accuracy
    //         });
    //         autocomplete.setBounds(circle.getBounds());
    //     });
    // }
}


//**************************************************************************************************************************************//