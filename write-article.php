
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Share Your Experience</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-loggedin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
       <div class="experience-div">
            <p class="experience-p center">Share Your Experience</p>
            <div class="input-field">
            <input  id="article-title" type="text" class="input-font icon-input" required><img src="img/pencil1.png" class="pencil-position pencila-size pencil1">
            <label for="article-title" class="input-font">Title</label>
            </div>
            
            <p class="spacing-p2"></p>         
            <div class="input-field">
             <select>
             <option value="Travel" selected>Travel</option>
             <option value="Stay">Stay</option>
             <option value="Food">Food</option>
             <option value="Job">Job</option>
             </select>
             <label>Category</label>
           </div>         

            <p class="spacing-p2"></p>     
            <p class="upload-cover">Upload Cover Photo</p>
            <!---  Only the cover photo of the article need to crop. Other photos uploaded for the article, don’t need to crop. The size for cover photo is 1600px (width) X 640px (height). Ratio 2.5 :1.-->
            <form action="#">
               <div class="file-field input-field pointer">
                 <div class="btn ow-btn pointer darker-bg-hover">
                  <span class="white-text photo-span pointer">Photo</span>
                  <input type="file" accept="image/*" onchange="preview_image4(event)">
                </div>
                <div class="file-path-wrapper">
                 <input class="file-path validate" type="text">
                </div>
              </div>
           </form>
           <div class="img-preview-div">
              <img id="output_image4" class="preview-img"/>
           </div>            
          <p class="spacing-p2"></p>             
          <p class="upload-cover">Content</p>           
          <!--- Insert the textarea tool here-->





         <p class="spacing-p2"></p>  
         <div class="centerise spacing2"><button class="button-a"><a class="waves-effect waves-light btn-large diy-blue-wave confirm-a" href="submission.php">Submit</a></button></div>
           
       </div>






   </div>
</div>

<script type='text/javascript'>
function preview_image4(event) 
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image4');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}
</script>

<?php include 'footer.php'; ?>
</body>
</html>
