
<header id="header" class="header header--fixed" role="banner">
  <div class="top-div darkblue-bg width-100 overflow-hidden">
    <div class="left-logo">
      <a href="itineraryCMS.php"><img src="img/logo.png" class="logo-img img-width-100 "></a>
    </div>
<!--    <div class="search-div admin-search-div">-->
<!--      <!--- Go to search.php-->-->
<!--      <input type="text" class="search-input menu-search-bar">-->
<!--      <button type="submit" class="png-submit-button"><img src="img/search-white.png" class="search-png img-width-100 hover-a pointer"></button>-->
<!--    </div>-->
    <div class="menu-link-div2 ow-menu-link-div-admin web-menu">
          <!---Display Publish Article, Flag, Settings and Logout only if the admin already login-->



        <div class="link-div ow-link-div display-none" id="logout" onclick="logout()"><a href="" class="white-text a-link"><img src="img/logout.png" class="menu-icon"></a></div>
        <script>

            function logout() {

                firebase.auth().signOut().then(function() {
                    alert("Logged Out");

                    window.location = 'login.php';

                }).catch(function(error) {
                    alert(error);
                });
            }
        </script>
<!--         <div class="link-div ow-link-div ow-icon-margin"><a href="admin-settings.php" class="white-text a-link"><img src="img/settings.png" class="menu-icon"></a></div>-->
<!--         <div class="link-div ow-link-div ow-icon-margin"><a href="flag.php" class="white-text a-link"><img src="img/flag.png" class="no-notification menu-icon"><img src="img/got-flag.png" class="notification display-none menu-icon"></a></div>-->
<!--         <div class="link-div ow-link-div"><a href="article-list.php" class="white-text a-link">Publish Article</a></div>-->
     <!---Display Profile and Notification only if the user already login--> 
    <!--
     <div class="link-div link-div-last"><a href="" class="white-text a-link"><img src="img/no-notification.png" class="no-notification"><img src="img/notification.png" class="notification"></a></div>
     <div class="link-div link-div-last"><a href="" class="white-text a-link">Profile</a></div> 
      


     <!---Display Register/Login only if the user not yet login-->
     <!--<div class="link-div ow-link-div"><a href="" class="white-text a-link">Register/ Login</a></div> -->
        <div class="link-div ow-link-div"><a href="merchantCMS.php" class="white-text a-link">Merchant</a></div>
        <div class="link-div ow-link-div"><a href="employerCMS.php" class="white-text a-link">Employer</a></div>
        <div class="link-div ow-link-div"><a href="hostCMS.php" class="white-text a-link">Host</a></div>
<!--        <div class="link-div ow-link-div"><a href="bookHomestayCMS.php" class="white-text a-link">Homestay</a></div>-->
        <div class="link-div ow-link-div"><a href="itineraryCMS.php" class="white-text a-link">Itinerary</a></div>
        <div class="link-div ow-link-div white-text a-link">MYR to USD Rate:<input type="text" id="changeUsdRateInput" style="width: 40px; height: auto; text-align: center; color: white; margin-left: 10px;"></div>
    </div>

      <script>
          document.querySelector('#changeUsdRateInput').addEventListener('keypress', function (e) {
              let key = e.which || e.keyCode;
              let newRate = $('#changeUsdRateInput').val();

              if (key === 13) { // 13 is enter
                  if(!newRate || Number(newRate).toString() === "NaN"){
                      $.alert({
                          title: 'Error!!',
                          content: "Please enter in numbers only"
                      });
                      return;
                  }

                  db.collection("currencyRates").doc("USD")
                      .update({
                          value: Number(newRate),
                          dateCreated: timeStamp
                      })
                      .then(function () {
                          $.alert({
                              title: 'Success',
                              content: "USD Rate has been changed to " + newRate + " with MYR as the base rate"
                          });
                      }).catch(function(error) {
                          $.alert({
                              title: 'An Error Occurred!!',
                              content: error
                          });
                      });

              }
          });

          $(document).ready(function() {
              db.collection("currencyRates")
                  .doc("USD")
                  .get()
                  .then(function (doc) {
                      $('#changeUsdRateInput').val(doc.data().value);
                  });
          });
      </script>
    <!-- Hide this right-app div if admin login-->
     <!---<div class="right-app">
      <div class="mobile-div center"><a href="#"><img src="img/mobile.png" class="mobile-img"></a></div>
       <div class="mobile-text-div white-text center"><a href="#" class="white-text"> Get the App</a></div>
    </div>-->

<!--      got problem in itineraryCMS.. hide first-->
<!--    <div class="mobile-menu">-->
<!--    -->
<!--     <ul id="slide-out" class="sidenav">-->
<!--      <li><a class="sidenav-close" href="#!"><img src="img/close.png" class="close-img"></a></li>-->
<!--      <li><a class="" href="home.php">Home</a></li>-->
<!--      <li><a class="" href="home.php">Travel</a></li>-->
<!--      <li><a class="" href="home.php">Stay</a></li>-->
<!--      <li><a class="" href="home.php">Food</a></li>  -->
<!--      <li><a class="" href="home.php">Job</a></li>-->
<!--      <!---Hide it if the user already logged in--->-->
<!--     <!-- <li><a class="login.php" href="#!">Register/ Login</a></li>-->
<!--      <!---Hide it if the user not yet login or the user is admin--->    -->
<!--      <!--<li><a class="profile.php" href="#!">Profile</a></li>-->
<!--      <li><a class="notification.php" href="#!">Notifications</a></li>-->
<!--      <!---Hide it if the user is not an admin--->-->
<!--      <li><a class="" href="article-list.php">Publish Article</a></li>-->
<!--      <li><a class="" href="flag.php">Flag</a></li>-->
<!--      <li><a class="" href="admin-settings.php">Settings</a></li>-->
<!--      <li><a class="" href="#!">Logout</a></li>       -->
<!--        -->
<!--     </ul>-->
<!--     <a href="#" data-target="slide-out" class="sidenav-trigger"><img src="img/menu.png" class="menu-img"></a>-->
<!--    </div>-->
    
  </div>
</header>