
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Settings" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Settings</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-admin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
     <div class="edit-profile-div">
         <p class="thankyou-p center">Change Password</p>

         <div class="input-field"> 
          <input  id="currentpassword" type="password" class="input-font" required>
          <label for="currentpassword" class="input-font">Current Password</label>          
         </div>     
         <div class="input-field"> 
          <input  id="newpassword" type="password" class="input-font" required>
          <label for="newpassword" class="input-font">New Password</label>          
         </div>           

   
     <div class="centerise gap"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave confirm-a div-100 width-400" href="#">Submit</a></button></div>
     <!---ignore this div if you not going to create the update banner feature-->
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave new-div-width width-400" href="update-banner.php">Update Homepage Banner</a></div> 
     <p class="centerise logout-p"><button class="button-a"><a href="login.php" class="confirm-a blue-hover">Logout</a></button></p>

        
        </div>
       
       </div>






   </div>
</div>


<?php include 'footer.php'; ?>
</body>
</html>
