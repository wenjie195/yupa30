
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<meta property="og:title" content="YuPa - Travel _ Free" />
<meta name="description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
<meta property="og:description" content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<title>YuPa - Article List</title>
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-admin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden">
     
     <div class="tab-div flag-tab-div">
      <div class="tab-menu">
       <ul class="tabs tab-menu-ul">
        <li class="tab tab-menu-li pen-li"><a class="active tab-a" href="#pending-article"><img src="img/pending1.png" class="tab-pen1 tab-icon"><img src="img/pending2.png" class="tab-pen2 tab-icon"><span class="tab-span"> Pending</span></a></li>
        <li class="tab tab-menu-li star-li"><a class="tab-a" href="#rejected"><img src="img/close2.png" class="tab-star1 tab-icon"><img src="img/close2.png" class="tab-star2 tab-icon"><span class="tab-span"> Rejected</span></a></li>
       </ul>
      </div>
      
      <!----Tab 1--->
      <div id="pending-article" class="contribution-div tab-small-div">
          <input type="text" class="friend-search-input flag-search-input" placeholder="Search"><button class="button-a search-button-a"><img src="img/friend-search.png" class="friend-search-icon pointer hover-a"></button>
          <div class="category-div article-cate">
            <span class="category-span">Category: </span>
            <select class="category-select">
             <option value="All">All</option>
             <option value="Travel">Travel</option>
             <option value="Stay">Stay</option>
             <option value="Food">Food</option>
             <option value="Job">Job</option>
            </select>
         </div>
         <div class="sortby-div article-sortby">         
           <span class="sortby-span article-sortby-span view-small">Sort by: </span>  
           <select class="category-sort">
             <option value="Latest Flag">Latest Submission</option>
             <option value="Oldest Flag">Oldest Submission</option>
           </select> 
           <span class="sortby-span view-big">Sort by: </span>        
        </div>

        <div class="notification-div flag-div clear">
        <div class="cms1-box">
         <a href="article-approve.php"><img src="img/cms0.jpg" class="img-width-100 cms1-img"></a>
         <div class="cms1-inner-box">
           <p class="cms1-date"><a href="article-approve.php" class="cms1-a">07/05/2018   12:00    Travel</a></p>
           <p class="cms1-title"><a href="article-approve.php" class="cms1-a">Top 20 Places for Mountain Hiking</a></p>
         </div>
    
      </div>
      
        <div class="cms1-box">
         <a href="article-edit.php"><img src="img/cms0.jpg" class="img-width-100 cms1-img"></a>
         <div class="cms1-inner-box">
           <p class="cms1-date"><a href="article-edit.php" class="cms1-a">07/05/2018   12:00    Travel</a></p>
           <p class="cms1-title"><a href="article-edit.php" class="cms1-a">Top 20 Places for Mountain Hiking</a></p>
         </div>
    
      </div>



      
      </div>
      </div>
      <!--- Tab 2 --->
      <div id="rejected" class="bookmark-div tab-small-div">
          <input type="text" class="friend-search-input flag-search-input" placeholder="Search"><button class="button-a search-button-a"><img src="img/friend-search.png" class="friend-search-icon pointer hover-a"></button>
          <div class="category-div article-cate">
            <span class="category-span">Category: </span>
            <select class="category-select">
             <option value="All">All</option>
             <option value="Travel">Travel</option>
             <option value="Stay">Stay</option>
             <option value="Food">Food</option>
             <option value="Job">Job</option>
            </select>
         </div>
         <div class="sortby-div article-sortby">         
           <span class="sortby-span article-sortby-span view-small">Sort by: </span>  
           <select class="category-sort">
             <option value="Latest Flag">Latest Submission</option>
             <option value="Oldest Flag">Oldest Submission</option>
           </select> 
           <span class="sortby-span view-big">Sort by: </span>        
        </div>


        <div class="notification-div flag-div clear">
         <div class="cms1-box">
         <a href="article-approve.php"><img src="img/cms0.jpg" class="img-width-100 cms1-img"></a>
         <div class="cms1-inner-box">
           <p class="cms1-date"><a href="article-approve.php" class="cms1-a">07/05/2018   12:00    Travel</a></p>
           <p class="cms1-title"><a href="article-approve.php" class="cms1-a">Top 20 Places for Mountain Hiking</a></p>
         </div>
    
      </div> 
         <div class="cms1-box">
         <a href="article-approve.php"><img src="img/cms0.jpg" class="img-width-100 cms1-img"></a>
         <div class="cms1-inner-box">
           <p class="cms1-date"><a href="article-approve.php" class="cms1-a">07/05/2018   12:00    Travel</a></p>
           <p class="cms1-title"><a href="article-approve.php" class="cms1-a">Top 20 Places for Mountain Hiking</a></p>
         </div>
    
      </div>     
      </div>

      
      </div>
    </div>
 
  
   
   
   </div>
</div>




<script>
function toggleComment() { 
    var thecomment = document.getElementById('comment');
    var displaySetting = thecomment.style.display;
    var commentButton = document.getElementById('commentButton');				
    if (displaySetting == 'block') { 
      thecomment.style.display = 'none';
      commentButton.innerHTML = '<img src="img/comment.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
    else { 
      thecomment.style.display = 'block';
      commentButton.innerHTML = '<img src="img/comment2.png" class="icon-png">  <span class="cms1-span comment-span">20k</span>';
    }
  }  
</script>



<script>
function toggleReply() { 
    var thereply = document.getElementById('reply');
    var displaySetting = thereply.style.display;
    var replyButton = document.getElementById('replyButton');				
    if (displaySetting == 'block') { 
      thereply.style.display = 'none';
      replyButton.innerHTML = 'View More Replies';
    }
    else { 
      thereply.style.display = 'block';
      replyButton.innerHTML = 'Hide the Replies';
    }
  }  
</script>

<script>
function toggleReplytwo() { 
    var thereplytwo = document.getElementById('replytwo');
    var displaySetting = thereplytwo.style.display;
    var replyButtontwo = document.getElementById('replyButtontwo');				
    if (displaySetting == 'block') { 
      thereplytwo.style.display = 'none';
    }
    else { 
      thereplytwo.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplythree() { 
    var thereplythree = document.getElementById('replythree');
    var displaySetting = thereplythree.style.display;
    var replyButtonthree = document.getElementById('replyButtonthree');				
    if (displaySetting == 'block') { 
      thereplythree.style.display = 'none';
    }
    else { 
      thereplythree.style.display = 'block';
    }
  }  
</script>

<script>
function toggleReplyfour() { 
    var thereplyfour = document.getElementById('replyfour');
    var displaySetting = thereplyfour.style.display;
    var replyButtonfour = document.getElementById('replyButtonfour');				
    if (displaySetting == 'block') { 
      thereplyfour.style.display = 'none';
      replyButtonfour.innerHTML = 'View More Comments';    }
    else { 
      thereplyfour.style.display = 'block';
      replyButtonfour.innerHTML = 'Hide Some Comments';
    }
  }  
</script>


<script>
function toggleReplyfive() { 
    var thereplyfive = document.getElementById('replyfive');
    var displaySetting = thereplyfive.style.display;
    var replyButtonfive = document.getElementById('replyButtonfive');				
    if (displaySetting == 'block') { 
      thereplyfive.style.display = 'none';
    }
    else { 
      thereplyfive.style.display = 'block';
    }
  }  
</script>
<?php include 'footer.php'; ?>
</body>
</html>
