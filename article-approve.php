
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include 'header.php'; ?>
<meta property="og:url" content="https://yupa.asia/" />
<!--- Make the image be a variable that fill in the article's cover photo-->
<meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg" />
<!--- Make the title be a variable that fill in the real article title-->
<meta property="og:title" content="YuPa - Article Title" />
<!--- Make the description be a variable that fill in the first paragraph of the article.-->
<meta name="description" content="Article first paragraph.">
<meta property="og:description" content="Article first paragraph." />
<meta name="keywords" content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
<!--- Make the title be a variable that fill in the real article title-->
<title>YuPa - Article Title</title>
<!--- Make the title be a variable that fill in the real web address-->
<link rel="canonical" href="https://yupa.asia/" />
</head>

<body>
<?php include 'menu-admin.php'; ?>
<div class="grey-bg overflow-hidden">
   <div class="white-bg overflow-hidden ow-article-white-bg">
      <img src="img/cms0.jpg" class="img-width-100">
      <div class="article-start-div">
        <div class="artitle-date-div">  
          <p class="article-date"><span class="article-date-span article-span">07/05/2018</span>   <span class="article-time-span article-span">11:00</span>    <a href="food.php" class="category-a"><span class="blue-hover article-span">Food</span></a></p>
        </div>
      
        <div class="artitle-icon-div">  
           <span class="like-span2 article-span-icon">151515k <button class="button-a"><img src="img/like.png" class="like-span-png like1a article-icon-img"><img src="img/like2.png" class="like-span-png like2a article-icon-img"></button></span>
           <span class="article-span-icon article-bookmark-span"><span class="cms1-span bookmark-span article-bookmark-span">3k </span><button class="button-a"><img src="img/star.png" class="icon-png bookmark1 article-icon-img"><img src="img/star2.png" class="icon-png bookmark2 article-icon-img"></button></span>
           <!-- Only display this icon when it is not author's view-->
           <!--<a href="#modal2" class="modal-trigger"><span class="flag-span article-span-icon article-flag-span"><button class="button-a"><img src="img/flag-visible.png" class="flag-height flag1 article-icon-img"><img src="img/flag2.png" class="flag-height flag2 article-icon-img"></button></span></a>-->

           <span class="article-span-icon author-pen-span"><a href="write-article.php"><img src="img/pen.png" class="icon-png pen1 pen-icon"><img src="img/pen2.png" class="icon-png pen2 pen-icon"></a></span>
        
        </div>
        
        <div class="clear"></div>
        
        <p class="article-title-p">Article Title</p>
        <p class="article-author-p">by <a href="profile.php" class="pointer blue-hover">Jucy Lim</a></p>
        
        <div class="article-content-div">
            <p class="article-content-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum ante a turpis laoreet pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed et pharetra elit, quis vehicula lorem. Ut eleifend, libero sit amet pellentesque aliquet, dui lorem convallis ligula, vitae porttitor lacus purus eu leo. Praesent a tempus ante. Integer sed aliquet est, nec aliquam massa. Morbi dictum dictum diam, eu convallis tortor finibus sit amet. Integer ante leo, auctor eu consectetur eu, accumsan at justo. Vestibulum a vulputate metus, sit amet aliquam arcu. Vivamus arcu orci, dapibus nec nibh ut, rhoncus tempus sem. Proin rhoncus aliquet tellus, in ultrices felis fermentum at. Fusce vulputate nulla et diam placerat tempus.
            </p>
            <img src="img/cms-content.jpg" class="img-width-100 article-content-img">
            <p class="image-description center">Image description.</p>
            <img src="img/cms-content2.jpg" class="img-width-100 article-content-img">
            <p class="image-description center">Image description.</p>            
            <p class="article-content-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum ante a turpis laoreet pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed et pharetra elit, quis vehicula lorem. Ut eleifend, libero sit amet pellentesque aliquet, dui lorem convallis ligula, vitae porttitor lacus purus eu leo. Praesent a tempus ante. Integer sed aliquet est, nec aliquam massa. Morbi dictum dictum diam, eu convallis tortor finibus sit amet. Integer ante leo, auctor eu consectetur eu, accumsan at justo. Vestibulum a vulputate metus, sit amet aliquam arcu. Vivamus arcu orci, dapibus nec nibh ut, rhoncus tempus sem. Proin rhoncus aliquet tellus, in ultrices felis fermentum at. Fusce vulputate nulla et diam placerat tempus.
            </p>
            <p class="article-content-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vestibulum ante a turpis laoreet pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed et pharetra elit, quis vehicula lorem. Ut eleifend, libero sit amet pellentesque aliquet, dui lorem convallis ligula, vitae porttitor lacus purus eu leo. Praesent a tempus ante. Integer sed aliquet est, nec aliquam massa. Morbi dictum dictum diam, eu convallis tortor finibus sit amet. Integer ante leo, auctor eu consectetur eu, accumsan at justo. Vestibulum a vulputate metus, sit amet aliquam arcu. Vivamus arcu orci, dapibus nec nibh ut, rhoncus tempus sem. Proin rhoncus aliquet tellus, in ultrices felis fermentum at. Fusce vulputate nulla et diam placerat tempus.
            </p>
        


          <div class="clear"></div>
         
          <div class="centerise spacing2"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave div-100" href="#">Publish</a></button></div>
          <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave modal-trigger text-transform-none new-div-width"  href="#modal2">Reject</a></div>            
        </div>      
       </div>





   </div>
</div>



<!-- Modal Structure -->
<div id="modal2" class="modal">
    <div class="modal-content">
    <img src="img/close-black.png" class="modal-close cross-img">
      <h4 class="modal-title">Reason of Rejecting this Article</h4>
        <p>
    <form action="#" id="flag-comment5" name="flag-comment5">
     <label>
      <input class="with-gap" name="report-comment5" type="radio" checked />
      <span class="form-span">Choose Your Reason</span>
    </label>   
    
      <div class="comment-select-div form-input2">
       <select id="report-comment5" name="report-comment5" >
       
        <option value="It is repated with other similar article.">It is repated with other similar article.</option>
        <option value="It contains offensive words and profanity.">It contains offensive words and profanity.</option>        
        <option value="It is cyberbullying or harassment.">It is cyberbullying or harassment.</option>
        <option value="The content is unsuitable to publish on YuPa website.">The content is unsuitable to publish on YuPa website.</option>   
        <option value="It has a copyright issue.">It has a copyright issue.</option>   
        <option value="It contains too much advertisements.">It contains too much advertisements.</option>              
        <option value="It is a hate speech.">It is a hate speech.</option> 
        <option value="It is a fake statement.">It is a fake statement.</option>
        <option value="it is a scam or misleading.">it is a scam or misleading.</option> 
        <option value="It is a personal safety threat.">It is a personal safety threat.</option>
        
       </select>
       </div>
       
     <label>
      <input class="with-gap" name="report-comment5" type="radio" id="textareaa5" />
      <span class="form-span">Write a reason</span>
      <textarea class="modal-textarea" name="report-comment5"  disabled id="textareab5" ></textarea>
    </label>        
       
     </form>

     <div class="centerise"><button class="button-a new-div-width"><a class="waves-effect waves-light btn-large diy-blue-wave div-100" href="#">Submit</a></button></div>
     <div class="centerise spacing"><a class="waves-effect waves-light btn-large diy-blue-line-wave modal-close new-div-width text-transform-none" href="#">Cancel</a></div>  
     <!--- After the user click submit button, show the user a message Thanks for the action. We will review it soon.-->
      </div>
  </p>
    </div>

<?php include 'footer.php'; ?>
</body>
</html>
