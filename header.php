<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121215869-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121215869-1');
</script>
<meta name="author" content="YuPa">
<meta property="og:type" content="website" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<link rel="icon" href="img/favicon.ico"  type="image/x-icon"   />
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<!--<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script>-->
<script src="js/jquery-3.3.1.min.js"></script>
