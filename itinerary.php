<?php
require_once('dbconn.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php include 'header.php'; ?>
    <meta property="og:url" content="https://yupa.asia/"/>
    <meta property="og:image" content="https://yupa.asia/img/fb-preview.jpg"/>
    <meta property="og:title" content="YuPa - Travel _ Free"/>
    <meta name="description"
          content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers.">
    <meta property="og:description"
          content="Yupa is an online application service that allows you to plan and create your own trips by using our suggested itineraries which can be edited or customized easily. You can book and enjoy travelling information or tips from travel experts, stay connected with the locals, look for travel buddies as well as share your incredible journey and adventures with the community. Yupa can be accessed via mobile application which is both user-friendly and accessible from any smart device, anywhere and anytime at the tip of your fingers."/>
    <meta name="keywords"
          content="YuPa, travel, travelling, app, transportation, stay, job, hotel, itinerary, tourism, attraction, sport, spot, planning, translate, translator, happy, worry, free, 游吧, 旅行, 旅游">
    <title>Add or edit itinerary</title>
    <link rel="canonical" href="https://yupa.asia/"/>
    <!--    <link href="css/jquery-confirm.css" />-->
    <!-- Add additional services that you want to use -->
    <!--    <script src="js/jquery-confirm.js"></script>-->

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="stylesheet" href="css/timepicki.css">
    <script type="text/javascript" src="js/timepicki.js?version=1.0.2"></script>

    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-storage.js"></script>
    <!--    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>-->
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>

    <!--    Initiate firestore connection-->
    <script src="js/db.js?version=1.0.2"></script>
    <script src="js/FixImageFileClearWhenNotSelect.js?version=1.0.2"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/date.js?version=1.0.2"></script>
    <!-- add to document <head> -->
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet">

    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>

    <script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>

    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.js"></script>


    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css"
          rel="stylesheet">
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-crop/dist/filepond-plugin-image-crop.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>

    <script src="https://unpkg.com/filepond-plugin-image-transform/dist/filepond-plugin-image-transform.js"></script>

    <!--this 2 is needed if want to use google places library-->

    <!--    //this is old way of getting the service api, sometimes got error 1 on 1st time load-->
    <!--    <script async defer-->
    <!--            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBtpTbwMZLT36cR2BrIRefIYpitoBKqA0&libraries=places&callback=getPlaceDetailsCallback">-->
    <!--    </script>-->
    <!--    <script src="UnifyPlaceAddress/UnifyPlaceAddress.js"></script>-->

    <!--    //this is new way-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBtpTbwMZLT36cR2BrIRefIYpitoBKqA0&libraries=places"></script>
    <script src="UnifyPlaceAddress/UnifyPlaceAddress.js?version=1.0.2"></script>

    <!--this 2 is needed if want to use google places library-->
    <script src="js/authUser.js?version=1.0.2"></script>


    <style>
        body {
            background-color: #f7f7f7;
        }

        #main-container-itinerary {
            width: 70%;
            height: 100%;
            margin-left: 15%;
            margin-right: 15%;
            background-color: white;
        }

        #image-upload-container-itinerary {
            width: 100%;
        }

        #image-upload {
            /*width: 100%;*/
            /*object-fit: cover;*/
            /*height: 300px;*/
            /*max-width: 100%;*/
            display: block;
            max-width: 100%;
            max-height: 350px;
            width: 100%;
            height: auto;
        }

        #image-upload:hover {
            cursor: pointer;
        }

        #image-upload-input {
            /*display: none;*/
        }

        /*.footer2{*/
        /*bottom: 0;*/
        /*position: fixed;*/
        /*width: 100%;*/
        /*}*/

        #itinerary-detail-container {
            width: 90%;
            margin: 50px 5%;
        }

        .fixed [type="checkbox"] + label, .fixed [type="radio"] + label {
            pointer-events: auto;
        }

        .custom-li {
            background: black;
        }

        .custom-li-a {
            color: white !important;
        }

        #button-container {
            width: 100%;
        }

        .myButton {
            display: block;
            width: 40%;
            margin: 30px 30%;
            text-transform: none;
            border-radius: 15px;
            font-weight: bold;
        }

        #publish-btn {
            background-color: #00468c;
        }

        #saveAsDraft-btn {
            background-color: white;
            color: #00468c;
            border: 1px solid #00468c;
        }

        .activity-detail {
            display: none;
        }

        .custom-input-field {
            float: right !important;
        }

        .custom-add-day-btn {
            padding-left: 50px !important;
        }

        .custom-add-plan-btn {
            float: right !important;
            display: none;
        }

        .showActivityContent {
            display: inline-block;
            width: 100%;
        }

        .hideActivityContent {
            display: none;
        }

        .add-plan {
            display: none;
        }

        .showBtnAddPlan {
            display: inline !important;
        }

        .row-activity {
            border-radius: 15px;
            border: 1px solid black;
            margin-top: 20px;
            margin-bottom: 20px;
            padding: 20px;
        }

        .close {
            display: block;
            float: right;
            width: 4%;
            margin-left: 96%;
            height: 29px;
            padding: 5px;
            background: url(img/close_icon.png) no-repeat center center;
        }

        .transition {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
        }

        .close {
            -webkit-transition: all .1s ease-in-out;
            -moz-transition: all .1s ease-in-out;
            -o-transition: all .1s ease-in-out;
            -ms-transition: all .1s ease-in-out;
        }

        .close:hover {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        /*custom*/
        .jconfirm-buttons {
            width: 100%;
            float: right;
        }

        .btn-default {
            float: right;
        }

        .container {
            width: 40% !important;
        }

        i.icon-white {
            color: white;
        }

        #add i {
            background: black;
            border: 1px solid black;
            border-radius: 50px;
        }

        #sub i {
            background: black;
            border: 1px solid black;
            border-radius: 50px;

        }

        #add:hover {
            cursor: pointer;
        }

        #sub:hover {
            cursor: pointer;
        }

        .horizontal-align-center {
            float: none !important;
            margin-left: auto;
            margin-right: auto;
        }

        .timepicker_wrap {
            width: 146%;
        }

        #progress-bar {
            /*margin: auto;*/
            /*width: 60%;*/
            /*height: 8px;*/
            position: relative;
        }

        #progress-bar > .ui-progressbar-value {
            background: #00acc1;
            margin: 0;
        }

        #progress-text {
            float: right;
        }

        #progress-title {
            margin-top: 10px;
            margin-bottom: 30px;
            font-size: large;
        }

        #modal-progress {
            border-radius: 15px;
            height: 200px;
        }

        #snackbar {
            visibility: hidden;
            min-width: 250px;
            margin-left: -125px;
            background-color: #333;
            color: #fff;
            text-align: center;
            border-radius: 2px;
            padding: 16px;
            position: fixed;
            z-index: 1;
            left: 50%;
            bottom: 30px;
            font-size: 17px;
        }

        #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        .filepond--file-action-button > svg > path {
            color: white;
        }

        .filepond--file-status-main {
            color: white;
        }

        .filepond--file-status-sub {
            color: white;
        }

        .filepond--file-info-main {
            color: white;
        }

        .filepond--file-info-sub {
            color: white;
        }

        .filepond--panel-root {
            background-color: #fff;
        }

        /*for gallery*/
        #gallery-upload-container-itinerary {
            width: 100%;
        }

        .gallery-upload {
            /*width: 100%;*/
            /*object-fit: cover;*/
            /*height: 300px;*/
            /*max-width: 100%;*/
            display: block;
            max-width: 100%;
            max-height: 350px;
            width: 100%;
            height: auto;
        }

        .gallery-upload:hover {
            cursor: pointer;
        }

        .gallery-upload-input {
            /*display: none;*/
        }

        .gallery-upload-delete{
            position: relative;
            top: 0;
            right: 0;
            float: right;
            padding: 5px 10px;
            margin: 5px 5px -50px 5px;
            width: 3.5%;
            min-width: 30px;
            height: auto;
            display:none;
            background-color: rgba(214, 200, 233, 0.6);
        }
    </style>
</head>
<body>
<?php include 'menu-admin.php'; ?>

<div id="serviceForGooglePlacesApi-helper"></div>

<div id="main-container-itinerary">
    <div id="image-upload-container-itinerary" style="position: relative;">
        <input id="image-upload-delete" type="image" src="img/close2.png" onclick="removeGalleryPic(this,'image-upload-input');" class="gallery-upload-delete"/>
        <img id="image-upload" src="img/ic_upload_placeholder.png" onclick="uploadImage();">
    </div>
    <input id="image-upload-input" type="file" accept="image/*">

    <h6><u>Picture Gallery</u></h6>
    <div id="gallery-upload-container-itinerary" style="position: relative;">

    </div>

    <div id="" class="col s12 no-padding" style="float: right; text-align: right; margin-right: 50px;">
        <a id="add-gallery" class="waves-effect waves-light tab custom-add-day-btn" onclick="addMoreGalleryPic(undefined,0);">+Add More Gallery Pictures</a>
    </div>


    <div id="itinerary-detail-container">

        <div class="input-field input-div2" id="quotation-details" style="padding-bottom: 10px; display: none;">
            <div class="card">
                <div class="row" style="padding-top: 10px; padding-bottom: 10px; border: 2.5px solid red;">
                    <div class="card-container">
                        <div class="col s12">
                            <h6><b><u>Trip Details</u></b></h6>
                        </div>
                        <div class="col s12">
                            <b>Budget: </b><span id="quotation-budget"></span>
                        </div>
                        <div class="col s12">
                            <b>Depart Date: </b>
                            <input id="quotation-departDate" type="text" style="width:30%;"/>
                        </div>
                        <div class="col s12">
                            <b>Return Date: </b>
                            <input id="quotation-returnDate" type="text" style="width:30%;"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="input-field input-div2">
            <input id="itinerary_title" placeholder="Enter a title" type="text" class="input-font icon-input" required>
            <label for="itinerary_title" class="input-font">Title</label>
        </div>

        <div class="input-field input-div2">
            <input id="itinerary_price_range" placeholder="Enter Price" type="number" class="input-font icon-input"
                   required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            <label for="itinerary_price_range" class="input-font">Price</label>
        </div>

        <div class="input-field input-div2">
            <input id="location-0-day-0" placeholder="Enter Place to Visit" type="text"
                   class="input-font icon-input input-location" required>
            <label for="location-0-day-0">Place to visit</label>
        </div>

        <div class="input-field input-div2">
            <input id="itinerary-duration-hour" placeholder="Duration Hour" type="number"
                   class="input-font icon-input input-location" required
                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            <label for="itinerary-duration-hour">Duration Hour</label>
        </div>

        <div class="input-field input-div2">
            <label for="itinerary-desc">Description</label>
            <textarea id="itinerary-desc" class="materialize-textarea" placeholder="Description"></textarea>
        </div>

        <label for="itinerary_what_to_eat">What to eat</label>
        <div class="input-field input-div2">
            <div class="row">


                <?php
                $itineraryCuisineSql = "SELECT name
                FROM itinerary 
                WHERE type = 'cuisine'";

                if ($itineraryCuisineStmt = $conn->prepare($itineraryCuisineSql)) {
                    /* execute query */
                    $itineraryCuisineStmt->execute();

                    $itineraryCuisineStmt->bind_result($name);

                    $postArray = array();
                    while ($itineraryCuisineStmt->fetch()) {
                        $object = new stdClass();
                        $object->name = $name;

                        $postArray[] = $object;
                    }
                    $itineraryCuisineStmt->close();

                    foreach ($postArray as $value) {
                        echo '
            <div class="col s6">
                <p>
                    <label>
                    <input type="checkbox" class="filled-in" id="' . $value->name . '" name="' . $value->name . '" value="' . $value->name . '"/><span>' . $value->name . '</span><br>
  
                    </label>
                </p>
            </div>
                       
                        ';
                    }
                }
                ?>
            </div>
        </div>

        <label for="itinerary_what_to_do">What to do</label>
        <div class="input-field input-div2">
            <div class="row">
                <?php
                $itineraryActivitySql = "SELECT name
                FROM itinerary 
                WHERE type = 'activity'";

                if ($itineraryActivityStmt = $conn->prepare($itineraryActivitySql)) {
                    /* execute query */
                    $itineraryActivityStmt->execute();

                    $itineraryActivityStmt->bind_result($activityName);

                    $activityArray = array();
                    while ($itineraryActivityStmt->fetch()) {
                        $object = new stdClass();
                        $object->activityName = $activityName;

                        $activityArray[] = $object;
                    }
                    $itineraryActivityStmt->close();

                    foreach ($activityArray as $value) {
                        echo '
            <div class="col s6">
                <p>
                    <label>
                    <input type="checkbox" class="filled-in" id="' . $value->activityName . '" name="' . $value->activityName . '" value="' . $value->activityName . '"/><span>' . $value->activityName . '</span><br>
  
                    </label>
                </p>
            </div>      
                       
                        ';
                    }
                }
                ?>
            </div>
        </div>

        <label for="itinerary_label">Itinerary</label>

        <div id="custom-nav-extended" class="row custom-nav-extended">
            <div class="col s10 no-padding">
                <ul class="tabs" id="myUl" style="overflow-x:hidden;">
                    <li id="tab-mLi-0" class="tab myLi custom-li" value="1"><a class="custom-li-a">Day 1</a></li>
                    <!--                        <li class="tab"><a href="#test2">Test 2</a></li>-->
                </ul>
            </div>
            <div id="" class="col s2 no-padding">
                <div class="tabs">
                    <a id="add-day" class="waves-effect waves-light tab custom-add-day-btn" onclick="newAddDay();">+Add
                        Day</a>
                </div>
            </div>
            <div id="activity-detail-day1" class="activity-detail showActivityContent">
                <div id="row-activity-day1-1" class="row-activity">
                    <a class="close" onclick="removePlan(1,1);"></a>
                    <div class="row">
                        <div class="input-field col s3">
                            <input placeholder="Time" id="activityTime-1-day-1" type="text" name="timepicker"
                                   class="validate timepicker">
                            <!--                                <select id="activity-time-1-day1" class="timepicker">-->
                            <!--                                    <option value="" disabled selected>Pick a time</option>-->
                            <!--                                </select>-->
                        </div>
                        <div class="input-field col s9">
                            <input id="activityName-1-day-1" type="text" class="validate plan-activity-name"
                                   placeholder="Activity">
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s9 custom-input-field">
                            <input id="location-1-day-1" type="text" class="validate input-location"
                                   placeholder="Location">
                        </div>
                    </div>
                </div>
            </div>

            <div id="add-plan-1" class="col s2 no-padding custom-add-plan-btn add-plan showBtnAddPlan">
                <div class="tabs">
                    <a id="add-plan-1" class="waves-effect waves-light tab custom-add-plan-btn" onclick="btnAddPlan();">+Add
                        Plan</a>
                </div>
            </div>
        </div>

        <div id="button-container">
            <a id="publish-btn" class="waves-effect waves-light btn-large myButton"
               onclick="submitItinerary(true);">Publish</a>
            <a id="saveAsDraft-btn" class="waves-effect waves-light btn-large myButton"
               onclick="submitItinerary(false);">Save as Draft</a>
        </div>

    </div>
</div>

<!-- Modal Structure -->
<div id="modal-progress" class="modal">
    <div class="modal-content">
        <div id="progress-title" class="col s8 no-padding">Uploading itinerary...</div>
        <div id="progress-bar" class="col s8 no-padding"></div>
        <div id="progress-text" class="col s8 no-padding"></div>
    </div>
</div>

<div id="snackbar"></div>

<!--<script>-->
<!--    firebase.auth().signOut().then(function() {-->
<!--        alert("Logged Out");-->
<!---->
<!--        window.location = 'home.php';-->
<!---->
<!--    }).catch(function(error) {-->
<!--        alert(error);-->
<!--    });-->
<!--</script>-->

<script>
    //gallery
    const GALLERY_ID = "gallery-upload-";
    const GALLERY_DELETE_ID = "gallery-upload-delete-";
    const GALLERY_INPUT_ID = "gallery-upload-input-";
    let galleryPondList = [];
    //for cover photo
    let isCoverUploaded = false;
    let coverUrl = "";
    let myImage;
    let openImageDialog;

    let collectionName = "itinerary";

    let itineraryRef = db.collection(collectionName).doc();
    let currentMaxDay = 1;//becuz day 1 already got. THIS IS FOR OUR OWN USE ONLY. to store the max day that user has put in, wont be put into database
    let currentLastDay = 1;//becuz day 1 already got. THIS IS THE NUMBER OF DAYS SET BY USER AND IS FINAL. will be set to database
    let currentLastIndex = 0;

    let allPlans = [];
    let unifiedAllPlans = [];
    let placeInterval = null;
    let currentPlanIndex = 0;
    let isThisPlanExecuted = {};

    let allTime = {};
    let allActivity = {};

    allTime[0] = {};
    allTime[0]['activityTime'] = undefined;
    allTime[0]['day'] = undefined;

    allActivity[0] = {};
    allActivity[0]['activityName'] = undefined;
    allActivity[0]['day'] = undefined;

    // var plan = {};
    // plan['placeAddress'] = {};
    // plan['hasError'] = false;
    // plan['isAllPlaceAddressUnified'] = {};
    // plan['activityName'] = "";
    // plan['placeAddress']['placeId'] = "";
    // plan['day'] = 0;
    //
    // allPlans.push(plan);

    // plan = {};
    // plan['placeAddress'] = {};
    // plan['hasError'] = false;
    // plan['isAllPlaceAddressUnified'] = {};
    // plan['activityName'] = "";
    // plan['placeAddress']['placeId'] = "";
    // plan['day'] = 0;
    // allPlans.push(plan);

    let myLat, myLng, myPlaceId;

    //Materailizecss Tab onlick custom
    $('ul.tabs').on('click', 'li', function () {
        var idValue = $(this).val();
        $('.tab').removeClass('custom-li');
        $('.tab a').removeClass('custom-li-a');
        $('.activity-detail').removeClass('showActivityContent');
        $(this).addClass('custom-li');
        $(this).find('a').addClass('custom-li-a');
        $('#activity-detail-day' + idValue).addClass('showActivityContent');
    });


    // To scroll materailizecss
    // Found here https://stackoverflow.com/questions/11700927/horizontal-scrolling-with-mouse-wheel-in-a-div
    (function () {
        function scrollHorizontally(e) {
            e = window.event || e;
            var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
            document.getElementById('myUl').scrollLeft -= (delta * 40); // Multiplied by 40
            e.preventDefault();
        }

        if (document.getElementById('myUl').addEventListener) {
            // IE9, Chrome, Safari, Opera
            document.getElementById('myUl').addEventListener("mousewheel", scrollHorizontally, false);
            // Firefox
            document.getElementById('myUl').addEventListener("DOMMouseScroll", scrollHorizontally, false);
        } else {
            // IE 6/7/8
            document.getElementById('myUl').attachEvent("onmousewheel", scrollHorizontally);
        }
    })();

    function uploadImage() {
        openImageDialog.browse();
    }

    function customSetTab() {
        $('.tab').removeClass('custom-li');
        $('.tab a').removeClass('custom-li-a');
        $('.add-plan').removeClass('showBtnAddPlan');
        $('.activity-detail').removeClass('showActivityContent');
        $('.activity-detail').removeClass('showActivityContent');
        $('#myUl .tab:last').addClass('custom-li');
        $('#myUl .tab:last').find('a').addClass('custom-li-a');
        $('.activity-detail:last').addClass('showActivityContent');
        $('.add-plan:last').addClass('showBtnAddPlan');

        $('#tab-mLi-' + (currentLastDay - 1)).click();
    }

    function btnAddPlan() {
        var numItems = $('.row-activity').length;
        //boolean
        // var getId = $('.tab').hasClass('custom-li');
        var getId = $('#myUl').find('.custom-li');
        var getCurentValue = getId.val();
        var targetVal = numItems + 1;
        $('#activity-detail-day' + getCurentValue + '').append('<div id="row-activity-day' + getCurentValue + '-' + targetVal + '" class="row-activity">\n' +
            '                        <a class="close" onclick="removePlan(' + getCurentValue + ',' + targetVal + ');"></a>\n ' +
            '                        <div class="row">\n' +
            '                            <div class="input-field col s3">\n' +
            '                                <input placeholder="Time" id="activityTime-' + targetVal + '-day-' + getCurentValue + '" type="text" class="validate timepicker">\n' +
            '                            </div>\n' +
            '                            <div class="input-field col s9">\n' +
            '                                <input id="activityName-' + targetVal + '-day-' + getCurentValue + '" type="text" class="validate plan-activity-name" placeholder="Activity">\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '\n' +
            '                        <div class="row">\n' +
            '                            <div class="input-field col s9 custom-input-field">\n' +
            '                                <input id="location-' + targetVal + '-day-' + getCurentValue + '" type="text" class="validate input-location" placeholder="Location">\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>');
        intializeTimePicker();
        // initialGoogleLocationAutoCompleteForAddPlan();
        initialGoogleLocationAutoComplete();

        // var plan = {};
        // plan['placeAddress'] = {};
        // plan['hasError'] = false;
        // plan['isAllPlaceAddressUnified'] = {};
        // plan['activityName'] = "";
        // plan['placeAddress']['placeId'] = "";
        // plan['day'] = parseInt(getCurentValue) - 1;
        //
        // allPlans.push(plan);
    }

    function removePlan(day, value) {
        $.confirm({
            title: 'Alert',
            content: 'Do you want to remove this plan?',
            buttons: {
                confirm: function () {
                    $('#row-activity-day' + day + '-' + value + '').hide();
                    if (allActivity[value]) {
                        allActivity[value]["activityName"] = undefined;
                    }

                    if (allTime[value]) {
                        allTime[value]['activityTime'] = undefined;
                    }

                    if (allPlans[value]) {
                        allPlans[value]['activityName'] = "";
                        allPlans[value]['startTime'] = 0;
                        allPlans[value]['placeAddress'] = {};
                        allPlans[value]['placeAddress']['placeId'] = "";
                        allPlans[value]['day'] = 999999;
                    }
                },
                cancel: function () {
                }
            }
        });
    }

    function newAddDay() {
        $.confirm({
            title: 'Alert',
            content: '<div id="field1">Add Day\n' +
            '    <div class="row valign-wrapper horizontal-align-center">\n' +
            '    <div class="input-field col s2 horizontal-align-center">\n' +
            // '    <button type="button" id="sub" class="sub" onclick="btnSub();">-</button>\n' +
            '    <a id="sub" class="sub horizontal-align-center" onclick="btnSub();"><i class="material-icons icon-white">remove</i></a>' +
            '    </div>\n' +
            '    <div class="input-field col s4 horizontal-align-center">\n' +
            '      <input class="horizontal-align-center" value="1" id="numQty" type="number" min="1" max="100">\n' +
            '    </div>\n' +
            '    <div class="input-field col s2 horizontal-align-center">\n' +
            // '    <button type="button" id="add" class="add" onclick="btnAdd();">+</button>\n' +
            '    <a id="add" class="add horizontal-align-center" onclick="btnAdd();"><i class="material-icons icon-white">add</i></a>' +
            '    </div>\n' +
            '    </div>' +
            '</div>',
            buttons: {
                confirm: function () {
                    confirmAddNewDay(null);
                },
                cancel: function () {
                }
            }
        });
    }

    function confirmAddNewDay(day){
        // $('.myLi').remove();
        // $('.activity-detail').remove();
        // $('.add-plan').remove();
        let getCurentValue;

        if(day){
            getCurentValue = day;
        }else{
            getCurentValue = $('#numQty').val();
        }

        getCurentValue = parseInt(getCurentValue);
        currentLastDay = getCurentValue;

        var latestIndexCount = 0;
        for (var i = currentMaxDay; i < getCurentValue; i++) {
            let newLatestIndex = allPlans.length + latestIndexCount;
            var thisElementDay = i + 1;
            $('#myUl').append('<li id = "tab-mLi-' + i + '" class="tab myLi" value="' + (thisElementDay) + '"><a>Day ' + (thisElementDay) + '</a></li>');
            $('#custom-nav-extended').append('<div id="activity-detail-day' + (thisElementDay) + '" class="activity-detail">\n' +
                '                    <div id="row-activity-day' + (thisElementDay) + '-' + (newLatestIndex) + '" class="row-activity">\n' +
                '                       <a class="close" onclick="removePlan(' + (thisElementDay) + ',' + (newLatestIndex) + ');"></a>\n' +
                '                       <div class="row">\n' +
                '                            <div class="input-field col s3">\n' +
                '                                <input placeholder="Time" id="activityTime-' + (newLatestIndex) + '-day-' + (thisElementDay) + '" type="text" class="validate timepicker">\n' +
                '                            </div>\n' +
                '                            <div class="input-field col s9">\n' +
                '                                <input id="activityName-' + (newLatestIndex) + '-day-' + (thisElementDay) + '" type="text" class="validate plan-activity-name" placeholder="Activity">\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '\n' +
                '                        <div class="row">\n' +
                '                            <div class="input-field col s9 custom-input-field">\n' +
                '                                <input id="location-' + (newLatestIndex) + '-day-' + (thisElementDay) + '" type="text" class="validate input-location" placeholder="Location">\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>' +
                ' <div id="add-plan-' + (thisElementDay) + '" class="col s2 no-padding custom-add-plan-btn add-plan showBtnAddPlan">\n' +
                '                    <div class="tabs">\n' +
                '                        <a class="waves-effect waves-light tab custom-add-plan-btn" onclick="btnAddPlan();">+Add Plan</a>\n' +
                '                    </div>\n' +
                '                </div>');

            latestIndexCount++;
        }

        if (getCurentValue > currentMaxDay) {
            currentMaxDay = getCurentValue;
        }

        customSetTab();
        intializeTimePicker();
        initialGoogleLocationAutoComplete();
    }

    function btnSub() {
        var numQty = $('#numQty').val();
        if (numQty > 1) {
            $('#numQty').val(--numQty);
        }
    }

    function btnAdd() {
        var numQty = $('#numQty').val();
        $('#numQty').val(++numQty);
    }

    // $( document ).ready(function() {
    //     initialGoogleLocationAutoComplete();
    // });

    initialGoogleLocationAutoComplete();

    function initialGoogleLocationAutoComplete() {
        // var geolocation = {
        //     lat: 35.6894875,
        //     lng: 139.69170639999993
        // };
        // var circle = new google.maps.Circle({
        //     center: geolocation,
        //     radius: 3052
        // });

        let inputLocation = $('.input-location');

        inputLocation.each(function () {
            // let placeToVisit = inputLocation[dayLocationCount];
            let n = $(this).attr('id');
            let getTheDay = parseInt(getDayForPlan(n));
            let currentId = $('#' + n)[0];
            if (currentLastIndex <= n.split('-')[1]) {
                console.log("Location input Id : " + n + " The location of which day " + currentId);
                let autocomplete = new google.maps.places.Autocomplete((currentId));
                // let autocomplete = new google.maps.places.Autocomplete((currentId),
                //     {types: ['geocode']});
                // autocomplete.setBounds(circle.getBounds());

                autocomplete.addListener('place_changed', function () {
                    let place = autocomplete.getPlace();
                    let placeId = place.place_id;
                    let latitude = place.geometry.location.lat();
                    let longtitude = place.geometry.location.lng();

                    myLat = latitude;
                    myLng = longtitude;
                    myPlaceId = placeId;

                    onPlaceAutocompleteItemSelected(autocomplete, currentId, getTheDay);

                    console.log("May Latitude : " + myLat + " My longtitude :" + myLng);
                });

                var plan = {};
                plan['placeAddress'] = {};
                plan['hasError'] = false;
                plan['isAllPlaceAddressUnified'] = {};
                plan['activityName'] = "";
                plan['placeAddress']['placeId'] = "";
                plan['day'] = getTheDay;

                allPlans.push(plan);

                currentLastIndex++;
            }

            if (getTheDay > currentLastDay) {
                $(this).hide();
                $('#tab-mLi-' + (getTheDay - 1)).hide();
            } else {
                $(this).show();
                $('#tab-mLi-' + (getTheDay - 1)).show();
            }
        });
    }

    let itinerary = {};
    let isEdit = false;
    let isQuotationRequested = false;

    let myProgress = 0;

    function submitItinerary(isSetToPublic) {
        getAllData(isSetToPublic);
    }

    intializeTimePicker();

    function intializeTimePicker() {

        $('.timepicker').timepicki({
            show_meridian: true,
            min_hour_value: 0,
            max_hour_value: 12,
            step_size_minutes: 15,
            overflow_minutes: true,
            increase_direction: 'up',
            input_writable: false,
            disable_keyboard_mobile: false,
            start_time: ["12", "00", "PM"]
        });
    }

    function showAlert(message) {
        $.alert({
            title: 'Alert!',
            content: message
        });
    }

    // from here https://github.com/pqina/filepond
    initializeFilepondPlugin();

    function initializeFilepondPlugin() {
        FilePond.registerPlugin(
            // encodes the file as base64 data
            FilePondPluginFileEncode,

            // validates files based on input type
            FilePondPluginFileValidateType,

            // corrects mobile image orientation
            FilePondPluginImageExifOrientation,

            // previews the image
            FilePondPluginImagePreview,

            // crops the image to a certain aspect ratio
            FilePondPluginImageCrop,

            // resizes the image to fit a certain size
            FilePondPluginImageResize,

            // applies crop and resize information on the client
            FilePondPluginImageTransform
        );


        const pond = FilePond.create(
            document.querySelector('#image-upload-input'),
            {
                labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
                // imagePreviewHeight: 170,
                imageCropAspectRatio: '16:9',
                imageResizeTargetWidth: 1280,
                imageResizeTargetHeight: 720,
                imageTransformOutputQuality: 70,
                acceptedFileTypes: ['image/*'],
                allowImagePreview: false,
                labelFileProcessingError: 'Error During Compressing',
                labelFileProcessingAborted: 'Compressing Cancelled',
                labelFileProcessing: 'Compressing Image',
                labelFileProcessingComplete: 'Finish Compress Image',
                server: {
                    process: (fieldName, file, metadata, load, error, progress, abort) => {
                        console.log(error);
                        //Below CODE is use to PREVIEW image
                        const image = new Image();
                        image.src = URL.createObjectURL(file);
                        // document.body.appendChild(image);
                        $('#image-upload').attr("src", image.src);
                        $('#image-upload-input').attr("src", image.src);
                        $('#image-upload-delete').show();
                        isCoverUploaded = false;

                        myImage = file;
                        load();
                    }
                }
            }
        );
        //Remove listener
        $("#image-upload-input").on('FilePond:removefile', e => {
            $('#image-upload').attr("src", "img/ic_upload_placeholder.png");
            $('#image-upload-input').attr("src", "");
            myImage = undefined;
        });

        openImageDialog = pond;
    }

    function addMoreGalleryPic(oriGalleryObj,oriIndex){
        let index = galleryPondList.length;
        if(oriGalleryObj){
            index = oriIndex;
        }
        let newGalleryId = GALLERY_ID + index;
        let newGalleryDeleteId = GALLERY_DELETE_ID + index;
        let newGalleryInputId = GALLERY_INPUT_ID + index;

        $('#gallery-upload-container-itinerary').append(
              '<input id="' + newGalleryDeleteId + '" type="image" src="img/close2.png" onclick=\'removeGalleryPic(this,"'+newGalleryInputId+'");\' class="gallery-upload-delete" />'
            + '<img id="' + newGalleryId + '" class="gallery-upload" src="img/ic_upload_placeholder.png">'
            + '<input id="' + newGalleryInputId + '" class="gallery-upload-input" type="file" accept="image/*">'
        );
        initializeFilepondPluginGallery(newGalleryId,newGalleryDeleteId,newGalleryInputId,oriGalleryObj,index);
    }

    function removeGalleryPic(thisElem,elemId){
        $(thisElem).hide();
        $('#' + elemId).trigger("FilePond:removefile");
    }

    function initializeFilepondPluginGallery(galleryId,galleryDeleteId,galleryInputId,oriGalleryObj,index) {
        FilePond.registerPlugin(
            // encodes the file as base64 data
            FilePondPluginFileEncode,

            // validates files based on input type
            FilePondPluginFileValidateType,

            // corrects mobile image orientation
            FilePondPluginImageExifOrientation,

            // previews the image
            FilePondPluginImagePreview,

            // crops the image to a certain aspect ratio
            FilePondPluginImageCrop,

            // resizes the image to fit a certain size
            FilePondPluginImageResize,

            // applies crop and resize information on the client
            FilePondPluginImageTransform
        );


        const pond = FilePond.create(
            document.querySelector('#'+galleryInputId),
            {
                labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
                // imagePreviewHeight: 170,
                imageCropAspectRatio: '16:9',
                imageResizeTargetWidth: 1280,
                imageResizeTargetHeight: 720,
                imageTransformOutputQuality: 70,
                acceptedFileTypes: ['image/*'],
                allowImagePreview: false,
                labelFileProcessingError: 'Error During Compressing',
                labelFileProcessingAborted: 'Compressing Cancelled',
                labelFileProcessing: 'Compressing Image',
                labelFileProcessingComplete: 'Finish Compress Image',
                server: {
                    process: (fieldName, file, metadata, load, error, progress, abort) => {
                        console.log(error);
                        //Below CODE is use to PREVIEW image
                        const image = new Image();
                        image.src = URL.createObjectURL(file);
                        // document.body.appendChild(image);
                        $('#' + galleryId).attr("src", image.src);
                        $('#' + galleryInputId).attr("src", image.src);

                        galleryPondList[index]['file'] = file;
                        galleryPondList[index]['src'] = image.src;
                        $('#' + galleryDeleteId).show();

                        load();
                    }
                }
            }
        );

        $("#" + galleryInputId).on('FilePond:removefile', e => {
            $('#' + galleryId).attr("src", "img/ic_upload_placeholder.png");
            $('#' + galleryInputId).attr("src", "");
            galleryPondList[index]['file'] = undefined;
            galleryPondList[index]['src'] = undefined;
        });

        $("#" + galleryId).on('dragenter', function (e){
            e.preventDefault();
            $(this).css('background', '#BBD5B8');
        });

        $("#" + galleryId).on('dragover', function (e){
            e.preventDefault();
        });

        $("#" + galleryId).on('drop', function (e){
            $(this).css('background', '#D8F9D3');
            e.preventDefault();
            let image = e.originalEvent.dataTransfer.files;
            console.log(image[0]);
            galleryPondList[index]['pond'].addFile(image[0]);
        });

        $("#" + galleryId).on('click', function (e){
            galleryPondList[index]['pond'].browse();
        });

        let galleryObj = {};

        galleryObj['pond'] = pond;
        galleryObj['file'] = undefined;
        galleryObj['url'] = undefined;
        galleryObj['src'] = undefined;
        galleryObj['galleryId'] = galleryId;
        galleryObj['galleryInputId'] = galleryInputId;

        if(oriGalleryObj){
            galleryObj['file'] = oriGalleryObj['file'];
            galleryObj['url'] = oriGalleryObj['url'];
            $('#' + galleryId).attr("src", galleryObj['url']);
            $('#' + galleryDeleteId).show();
        }else{
            $('#' + galleryDeleteId).hide();
        }

        galleryPondList.push(galleryObj);
    }

    function getAllData(isSetToPublic) {
        var itineraryImage = $('#image-upload');
        var itineraryTitle = $('#itinerary_title');
        var itineraryPriceRange = $('#itinerary_price_range');
        var itineraryDurationHour = $('#itinerary-duration-hour');
        var itineraryPlaceToVisit = $('#location-0-day-0');

        var complete = true;

        if (itineraryImage.attr('src') === "" || itineraryImage.attr('src') === "undefined" || itineraryImage.attr('src') === "img/ic_upload_placeholder.png") {
            if(!myImage){
                showAlert("Please upload itinerary cover photo.");
                complete = false;
                itineraryImage.focus();
                return false;
            }

            // if(!isEdit){
            //     showAlert("Please upload itinerary cover photo.");
            //     complete = false;
            //     itineraryImage.focus();
            //     return false;
            // }
        }

        if(isEdit && itinerary["departDate"] > itinerary['returnDate']){
            showAlert("Depart date cannot be more than return date");
            complete = false;
            $('#quotation-departDate').focus();
        }

        if (itineraryTitle.val() === "" || itineraryTitle.val() === "undefined") {
            showAlert("Please enter itinerary title.");
            complete = false;
            itineraryTitle.focus();
            return false;
        }


        if (itineraryPriceRange.val() === "" || itineraryPriceRange.val() === "undefined") {

            showAlert("Please enter price.");
            complete = false;
            itineraryPriceRange.focus();
            return false;
        }
        else {
            let reg = /^\d+$/;
            let postalResult = reg.test(itineraryPriceRange.val());

            if (!postalResult) {
                showAlert("Only number allow enter on price.");
                complete = false;
                itineraryPriceRange.focus();
                return false;
            }
        }

        if (itineraryPlaceToVisit.val() === "" || itineraryPlaceToVisit.val() === "undefined" || !allPlans[0]['placeAddress'] || !allPlans[0]['placeAddress']['placeId'] || allPlans[0]['placeAddress']['placeId'].trim() === '') {
            showAlert("Please enter place to visit.");
            complete = false;
            itineraryPlaceToVisit.focus();
            return false;
        }

        if (itineraryDurationHour.val() === "" || itineraryDurationHour.val() === "undefined") {

            showAlert("Please enter duration hour.");
            complete = false;
            itineraryDurationHour.focus();
            return false;
        }
        else {
            let reg = /^\d+$/;
            let postalResult = reg.test(itineraryDurationHour.val());

            if (!postalResult) {
                showAlert("Only number allow enter on price.");
                complete = false;
                itineraryDurationHour.focus();
                return false;
            }
        }

        if (complete) {
            let mModalProgress = $('#modal-progress');
            let iniModalProgress = M.Modal.getInstance(mModalProgress);
            //initiate again for the progress
            myProgress = 0;

            iniModalProgress.open();
            // const file = $('#image-upload-input').get(0).files[0];
            // if (!$('#image-upload-input').val()) {
            //     iniModalProgress.close();
            //     showAlert("Please upload an image file!");
            // }

            // if(myImage.name){
            //     showAlert("Error image, Please upload your image again.");
            //     return;
            // }

            //new
            prepareToUploadGalleryAndCover(iniModalProgress,isSetToPublic);
            // //old
            // if(isEdit){
            //     if(myImage){
            //         uploadCoverPic(iniModalProgress,isSetToPublic);
            //     }else{
            //         myProgress += 50;
            //         $("#progress-bar").progressbar({
            //             value: 50
            //         });
            //         $('#progress-text').text("50%");
            //         onUploadPicFinished(itinerary['itineraryPicUrl'],iniModalProgress,isSetToPublic);
            //     }
            // }else{
            //     uploadCoverPic(iniModalProgress,isSetToPublic);
            // }

        }
    }

    function prepareToUploadGalleryAndCover(iniModalProgress,isSetToPublic){
        myProgress = 0;

        for(let i = 0; i < galleryPondList.length; i++){
            let gallery = galleryPondList[i];

            if(gallery && gallery['file'] && gallery['src'] && gallery['src'].trim() !== ''){
                gallery['isUploaded'] = false;
                uploadCoverPic(iniModalProgress,isSetToPublic,gallery['file'],i);
            }else{
                gallery['isUploaded'] = true;
            }
        }

        if(!isCoverUploaded && myImage){
            uploadCoverPic(iniModalProgress,isSetToPublic,myImage,-1);
        }

        onEachPicFinished(iniModalProgress,isSetToPublic);
    }

    function uploadCoverPic(iniModalProgress,isSetToPublic,file,i){
        const name = (+new Date()) + '-' + file.name;
        const imageRef = storage.ref().child("itinerary/" + uid + "/" + itineraryRef.id + "/" + name);
        const task = imageRef.put(file);
        task.on('state_changed', function (snapshot) {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            myProgress = ( (((snapshot.bytesTransferred / snapshot.totalBytes) * 100)/2) / (galleryPondList.length + 1) ) + myProgress;
            console.log('Upload is ' + myProgress + '% done');
            $("#progress-bar").progressbar({
                value: parseInt(myProgress)
            });
            $('#progress-text').text(myProgress.toFixed(0) + "%");

            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                    console.log('Upload is running');
                    break;
            }
        }, function (error) {
            showAlert("Error uploading: " + error);
            console.log(error);
            iniModalProgress.close();
            console.log(imageRef);
            // Handle unsuccessful uploads
        }, function () {
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            task.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                if(i === -1){
                    coverUrl = downloadURL;
                    isCoverUploaded = true;
                }else{
                    galleryPondList[i]['url'] = downloadURL;
                    galleryPondList[i]['isUploaded'] = true;
                }
                onEachPicFinished(iniModalProgress,isSetToPublic);
            });
        });
    }

    function onEachPicFinished(iniModalProgress,isSetToPublic){
        let newGalleryList = [];

        if(!isCoverUploaded){
            return;
        }

        for(let i = 0; i < galleryPondList.length; i++){
            if(galleryPondList[i]['isUploaded']){
                let gallery = galleryPondList[i];

                if(gallery && gallery.file && gallery.url && gallery.url.trim() !== ''){
                    let newGalleryObj = {};
                    newGalleryObj['name'] = gallery.file.name;
                    newGalleryObj['desc'] = "";
                    newGalleryObj['url'] = gallery.url;

                    newGalleryList.push(newGalleryObj);
                }
            }else{
                return;
            }
        }

        onAllUploadPicFinished(coverUrl,iniModalProgress,isSetToPublic,newGalleryList)
    }

    function onAllUploadPicFinished(downloadURL,iniModalProgress,isSetToPublic,newGalleryList){
        var itineraryTitle = $('#itinerary_title');
        var itineraryDesc = $('#itinerary-desc');
        var itineraryPriceRange = $('#itinerary_price_range');
        var itineraryDurationHour = $('#itinerary-duration-hour');
        var itineraryPlaceToVisit = $('#location-0-day-0');

        let arrayGPS = [];
        let gps = new getFirstoreInstance.GeoPoint(myLat, myLng);
        arrayGPS.push(gps);

        itinerary['tripName'] = itineraryTitle.val();
        itinerary['introMsg'] = itineraryDesc.val();
        itinerary['price'] = parseInt(itineraryPriceRange.val());
        itinerary['locationName'] = itineraryPlaceToVisit.val();

        itinerary['itineraryPicUrl'] = downloadURL;
        itinerary['gallery'] = newGalleryList;
        itinerary['gps'] = arrayGPS;
        itinerary['placeId'] = myPlaceId;
        itinerary['durationHour'] = parseInt(itineraryDurationHour.val());
        itinerary['dateUpdated'] = timeStamp;

        if(!isEdit){
            itinerary['currencyCode'] = "MYR";
            itinerary['departDate'] = 1546322870000;
            itinerary['returnDate'] = 4102466870000;
            itinerary['maxBudget'] = parseInt(itineraryPriceRange.val());
            itinerary['minBudget'] = parseInt(itineraryPriceRange.val());
            itinerary['likeCount'] = 0;
            itinerary['introMsg'] = null;
            itinerary['specialRequest'] = null;
            itinerary['isFromAdmin'] = null;
            itinerary['isPaid'] = null;
            itinerary['paymentId'] = null;
            itinerary['quotationStatus'] = null;
            itinerary['isReadByUser'] = null;
            itinerary['oriItiId'] = null;
            itinerary['dateCreated'] = timeStamp;
        }

        if(!isQuotationRequested){
            itinerary['userId'] = uid;
            itinerary['isPublic'] = isSetToPublic;
            itinerary['isPublish'] = isSetToPublic;
            itinerary['noofDays'] = currentLastDay;
        }else{
            //submit quotation
            itinerary['currencyCode'] = "MYR";
            itinerary['isPublic'] = false;
            itinerary['isPublish'] = false;
            itinerary['quotationStatus'] = "done";
            itinerary['isFromAdmin'] = true;
            itinerary['dateQuotationAction'] = timeStamp;
            itinerary['isReadByUser'] = false;
        }

        getSelectOption();

        setToFirebase(itinerary, iniModalProgress, parseInt(myProgress));
    }

    function getSelectOption() {
        /* declare an checkbox array */
        var chkArray = [];

        $("input:checkbox").each(function() {
            delete itinerary[ $(this).val()];
        });

        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        $("input:checked").each(function () {
            chkArray.push($(this).val());
        });


        for (var num = 0; num < chkArray.length; num++) {
            itinerary[chkArray[num]] = true;
        }
        console.log(chkArray);

        // /* we join the array separated by the comma */
        // var selected;
        // selected = chkArray.join(',') ;
        //
        // /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
        // if(selected.length > 0){
        //     alert("You have selected " + selected);
        // }else{
        //     alert("Please at least check one of the checkbox");
        // }
    }

    function setToFirebase(itinerary, iniModalProgress, myProgress) {

        //from here https://stackoverflow.com/questions/8312459/iterate-through-object-properties
        //this is to make all undefined keys become null for the itinerary object
        Object.keys(itinerary).forEach(function(key,index) {
            if(itinerary[key] === undefined){
                itinerary[key] = null;
            }
        });

        itineraryRef.set(itinerary).then(function () {
            // console.log(itineraryRef.id);
            db.collection(collectionName)
                .doc(itineraryRef.id)
                .collection("plan")
                .get()
                .then(function (snapshots) {
                    snapshots.forEach(function (doc) {
                        db.collection(collectionName)
                            .doc(itineraryRef.id)
                            .collection("plan")
                            .doc(doc.id)
                            .delete();
                    });
                    setPlanToFirebase(itineraryRef.id, iniModalProgress, myProgress);
                });
        });
    }

    function setPlanToFirebase(documentId, iniModalProgress, myProgress) {
        currentPlanIndex = 0;
        let allLocation = {};
        var totalActivity = $('.row-activity').length;
        var totalDay = $('.myLi').length;

        let day = "day";
        $('.plan-activity-name').each(function () {
            let n = $(this).attr('id');
            let getTheDay = parseInt(getDayForPlan(n)) - 1;
            var activityIndex = n.split('-')[1];
            allActivity[activityIndex] = {};
            allActivity[activityIndex]['activityName'] = $('#' + n).val();
            allActivity[activityIndex]['day'] = getTheDay;
        });

        $('input[type="text"].timepicker').each(function () {
            let d = new Date('1994-12-13 ' + $(this).val() + '');
            let timepickerId = $(this).attr('id');
            let getTheDay = getDayForPlan(timepickerId);
            var timeIndex = timepickerId.split('-')[1];
            allTime[timeIndex] = {};
            allTime[timeIndex]['activityTime'] = d.getTime();
            allTime[timeIndex]['day'] = parseInt(getTheDay);
            // console.log("Time Picker Id :" + timepickerId + "Current Day : " + totalDay + " Get Time : " + d.getTime());
        });

        // for(let i = 0; i< totalActivity; i++){
        //     let planRef = db.collection(collectionName).doc();
        //     let planId = planRef.id;
        //     console.log(planId);
        // }

        // console.log("All Time " + JSON.stringify(allTime) + " All Activity Name : " + JSON.stringify(allActivity));

        getLocation(documentId, iniModalProgress, myProgress);

    }

    function getLocation(documentId, iniModalProgress, myProgress) {
        $('#progress-title').text("Uploading itinerary plans...");


        if (placeInterval) {
            clearInterval(placeInterval);
        }

        unifiedAllPlans = [];
        currentPlanIndex = 0;
        isThisPlanExecuted = {};

        for (var i = 0; i < allPlans.length; i++) {
            let placeId = "";
            if(allPlans[i]['placeAddress'] && allPlans[i]['placeAddress']['placeId']){
                placeId = allPlans[i]['placeAddress']['placeId'];
            }
            allPlans[i]['placeAddress'] = {};
            allPlans[i]['placeIndex'] = {};
            allPlans[i]['hasError'] = false;
            allPlans[i]['isAllPlaceAddressUnified'] = {};
            allPlans[i]['placeAddress']['placeId'] = placeId;
        }

        getPlaceDetails(currentPlanIndex);

        //loop every 3 second to check whether the place has finished downloaded/unified
        placeInterval = window.setInterval(function () {
            let isModalClosed = false;

            let updateProgressPlan = (100 / parseInt(allPlans.length))/2;
            myProgress = myProgress + parseInt(updateProgressPlan);

            console.log("currentPlanIndex: " + currentPlanIndex);

            for (let i = 0; i < currentPlanIndex; i++) {

            }
            if(myProgress >= 100){
                myProgress = 100;
            }
            $("#progress-bar").progressbar({
                value: myProgress
            });
            $('#progress-text').text(myProgress.toFixed(0) + "%");

            if(myProgress === 100 || myProgress >= 100){
                console.log("Is 100 or more than 100");
                myProgress = 0;
                // $("#progress-bar").progressbar({
                //     value: 0
                // });
                // $('#progress-text').text(0 + "%");
                // $('#progress-title').text("Uploading itinerary...");
                // iniModalProgress.close();
                // isModalClosed = true;
                // showAlert("Successfully uploaded");
            }

            console.log("I want to see My Progress : " + myProgress);
            //
            // if (myProgress === 100 || myProgress > 100) {
            //     myProgress = 0;
            //     $("#progress-bar").progressbar({
            //         value: 0
            //     });
            //     $('#progress-text').text(0 + "%");
            //     $('#progress-title').text("Upload itinerary.");
            //     iniModalProgress.close();
            //     showAlert("Successfully uploaded");
            // }

            checkForAllPlaceAddressUnificationDoneEveryThreeSec();


            if (unifiedAllPlans && unifiedAllPlans.length > 0) {
                console.log("put into database");

                // if(!isModalClosed){
                //     myProgress = 0;
                //     $('#progress-title').text("Uploading itinerary...");
                //     iniModalProgress.close();
                //     showAlert("Successfully uploaded");
                // }

                for (let i = 0; i < unifiedAllPlans.length; i++) {
                    if (i === 0) {
                        let updateItiRef = db.collection(collectionName).doc(documentId);
                        let placeAddressList = [];
                        placeAddressList.push(unifiedAllPlans[i]["placeAddress"]);
                        let placeIndex = unifiedAllPlans[i]["placeIndex"];

                        updateItiRef
                            .update({
                                placeAddressList: placeAddressList,
                                placeIndex: placeIndex
                            }).then(function () {

                        });
                    }
                    else {
                        if (allActivity[i] && allActivity[i]["activityName"] && allTime[i] && allTime[i]['activityTime'] && unifiedAllPlans[i]["day"] <= currentLastDay) {
                            let setPlanRef = db.collection(collectionName).doc(documentId).collection("plan").doc();
                            let activityName = allActivity[i]["activityName"];
                            let activityDay = allActivity[i]["day"];
                            let activityGps;
                            if (unifiedAllPlans[i]["placeAddress"]["lat"] && unifiedAllPlans[i]["placeAddress"]["lng"]) {
                                activityGps = new getFirstoreInstance.GeoPoint(unifiedAllPlans[i]["placeAddress"]["lat"], unifiedAllPlans[i]["placeAddress"]["lng"]);
                            } else {
                                activityGps = null;
                            }

                            let activityTime = allTime[i]['activityTime'];
                            setPlanRef
                                .set({
                                    activityName: activityName,
                                    dateCreated: timeStamp,
                                    day: activityDay,
                                    eventId: 0,
                                    gps: activityGps,
                                    placeAddress: unifiedAllPlans[i]["placeAddress"],
                                    placeIndex: unifiedAllPlans[i]["placeIndex"],
                                    startTime: activityTime,
                                    transportMode: "car"

                                })
                                .then(function () {

                                });
                        }
                    }
                    // if(myProgress === 100){
                    //     iniModalProgress.close();
                    // }
                }

                myProgress = 0;
                $('#progress-title').text("Uploading itinerary...");
                iniModalProgress.close();
                $.alert({
                    title: 'Alert!',
                    content: 'Successfully uploaded',
                    buttons: {
                        Ok: function () {
                            window.location = 'itineraryCMS.php';
                        }
                    },
                });
                console.log(unifiedAllPlans);
            }
        }, 3000);//every 3 second

        // // allPlans = [];
        // // unifiedAllPlans = [];
        // // placeInterval = null;
        // // currentPlanIndex = 0;
        // // isThisPlanExecuted = {};
        //
        // if(allPlans && allPlans.length > 0){
        //     currentPlanIndex = 0;
        //
        //     for(var i = 0; i < allPlans.length; i++){
        //         allPlans[i]['hasError'] = false;
        //         allPlans[i]['isAllPlaceAddressUnified'] = {};
        //
        //         var plan = {};
        //         plan['placeAddress'] = {};
        //         plan['hasError'] = false;
        //         plan['activityName'] = "";
        //         plan['placeAddress']['placeId'] = "";
        //         plan['day'] = 1;
        //
        //         allPlans.push(plan);
        //     }
        // }

        console.log("sttttart");
    }

    function getDayForPlan(words) {
        let index = words.split('-')[3]; // has to be itinerary-0-autocomplete-0 or plan-0-autocomplete-0
        return index;
    }

    $(document).ready(function()
    {
        $("#image-upload").on('dragenter', function (e){
            e.preventDefault();
            $(this).css('background', '#BBD5B8');
        });

        $("#image-upload").on('dragover', function (e){
            e.preventDefault();
        });

        $("#image-upload").on('drop', function (e){
            $(this).css('background', '#D8F9D3');
            e.preventDefault();
            var image = e.originalEvent.dataTransfer.files;
            console.log(image[0]);
            dragAndDropImage(image[0]);
        });

        populateOriIti();
    });

    function dragAndDropImage(image){
        openImageDialog.addFile(image);

    }

    //***********************************************THIS IS FOR EDIT FEATURE START********************************************/
    function populateOriIti(){
        //only the cover photo dont have to upload again unless user chose a new photo to upload.
        //all place address id (itinerary and all plans) will be used to fetch and process the unified place address again
        const oriItiId = getParameterByName('itineraryId');
        if(!oriItiId || oriItiId == null || oriItiId === ""){
            return;
        }

        isEdit = true;

        let mModalProgress = $('#modal-progress');
        const iniModalProgress = M.Modal.getInstance(mModalProgress);

        $('#progress-title').text("Fetching itinerary details...");
        $('#progress-text').text(myProgress.toFixed(0) + "%");

        setTimeout(function () {
            //dont know why when on load or ready, the progress modal wont open, so i use this hacky way to delay the open function then it can open
            iniModalProgress.open();
        }, 1);

        db.collection(collectionName).doc(oriItiId).get()
            .then(function(doc) {
                myProgress += 50;
                updateProgressBar(myProgress,iniModalProgress);

                if(doc.exists) {
                    let tempIti = doc.data();

                    $("input:checkbox").each(function() {
                        let checkboxValue = $(this).val();
                        if(checkboxValue && tempIti[checkboxValue] === true){
                            $(this).prop('checked', true);
                        }
                    });

                    let tripName = tempIti.tripName;
                    if(tripName == null){
                        tripName = tempIti.locationName;
                    }
                    $('#itinerary_title').val(tripName);
                    if(tempIti.introMsg){
                        $('#itinerary-desc').val(tempIti.introMsg);
                        $('#itinerary-desc').trigger('autoresize');
                    }
                    $('#itinerary_price_range').val(tempIti.price);
                    $('#location-0-day-0').val(tempIti.locationName);
                    allPlans[0]['placeAddress'] = tempIti.placeAddressList[0];
                    $('#itinerary-duration-hour').val(tempIti.durationHour);
                    itinerary['itineraryPicUrl'] = tempIti.itineraryPicUrl;
                    if(tempIti.gallery){
                        itinerary['gallery'] = tempIti.gallery.concat();
                    }else{
                        itinerary['gallery'] = undefined;
                    }
                    if(tempIti.itineraryPicUrl && tempIti.itineraryPicUrl !== null){
                        $('#image-upload').attr("src", tempIti.itineraryPicUrl);
                        $('#image-upload-delete').show();
                    }
                    itinerary['currencyCode'] = tempIti.currencyCode;
                    itinerary['departDate'] = tempIti.departDate;
                    itinerary['returnDate'] = tempIti.returnDate;
                    itinerary['maxBudget'] = tempIti.maxBudget;
                    itinerary['minBudget'] = tempIti.minBudget;
                    itinerary['introMsg'] = tempIti.introMsg;
                    itinerary['specialRequest'] = tempIti.specialRequest;
                    itinerary['isPaid'] = tempIti.isPaid;
                    itinerary['paymentId'] = tempIti.paymentId;
                    itinerary['quotationStatus'] = tempIti.quotationStatus;
                    itinerary['isReadByUser'] = tempIti.isReadByUser;
                    itinerary['likeCount'] = tempIti.likeCount;
                    itinerary['isFromAdmin'] = tempIti.isFromAdmin;
                    itinerary['oriItiId'] = tempIti.oriItiId;
                    itinerary['noofDays'] = tempIti.noofDays;
                    itinerary['dateCreated'] = tempIti.dateCreated;
                    itineraryRef = db.collection(collectionName).doc(doc.id);

                    myLat = tempIti.gps[0]._lat;
                    myLng = tempIti.gps[0]._long;
                    myPlaceId = tempIti.placeAddressList[0].placeId;

                    //quotations type
                    if(tempIti.quotationStatus && tempIti.quotationStatus === "pending" && tempIti.dateQuotationRequested){
                        try{
                            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                            ];

                            let minBudget = tempIti.minBudget;
                            let maxBudget = tempIti.maxBudget;

                            let departDate = new Date(tempIti.departDate);
                            let departYear = departDate.getUTCFullYear();
                            let departMonth = monthNames[departDate.getUTCMonth()];
                            let departDay = departDate.getUTCDate();

                            let returnDate = new Date(tempIti.returnDate);
                            let returnYear = returnDate.getUTCFullYear();
                            let returnMonth = monthNames[returnDate.getUTCMonth()];
                            let returnDay = returnDate.getUTCDate();

                            let departDateEle = $('#quotation-departDate');
                            let returnDateEle = $('#quotation-returnDate');
                            departDateEle.val(departDay + " " + departMonth + " " + departYear);
                            returnDateEle.val(returnDay + " " + returnMonth + " " + returnYear);
                            $( "#quotation-departDate" ).datepicker({
                                format: 'dd mmm yyyy',
                                defaultDate: new Date(departDate),
                                onSelect:function(selectedDate) {
                                    itinerary['departDate'] = selectedDate.getTime() - (selectedDate.getTimezoneOffset() * 60000);
                                }
                            });

                            $( "#quotation-returnDate" ).datepicker({
                                format: 'dd mmm yyyy',
                                defaultDate: new Date(returnDate),
                                onSelect:function(selectedDate) {
                                    itinerary['returnDate'] = selectedDate.getTime() - (selectedDate.getTimezoneOffset() * 60000);
                                }
                            });

                            let budgetEle = $('#quotation-budget');
                            if(minBudget === maxBudget){
                                budgetEle.html(itinerary['currencyCode'] + minBudget);
                            }else{
                                budgetEle.html(itinerary['currencyCode'] + minBudget + " - " + itinerary['currencyCode'] + maxBudget);
                            }

                            $('#quotation-details').show();

                            $('#add-day').hide();
                            $('#publish-btn').html("Submit Quotation");
                            $('#saveAsDraft-btn').hide();

                            itinerary['userId'] = tempIti.userId;
                            itinerary['quotationRemark'] = tempIti.quotationRemark;
                            itinerary['dateQuotationRequested'] = tempIti.dateQuotationRequested;

                            isQuotationRequested = true;
                        }catch (err){
                            throw(err);
                        }
                    }

                    coverUrl = itinerary['itineraryPicUrl'];
                    if(coverUrl && coverUrl.trim() !== ''){
                        isCoverUploaded = true;
                    }else{
                        isCoverUploaded = false;
                        coverUrl = "";
                    }

                    if(itinerary['gallery'] && itinerary['gallery'].length > 0){
                        for(let index = 0; index < itinerary['gallery'].length; index++){
                            let gallery = itinerary['gallery'][index];
                            let galleryObj = {};
                            let tempFile = {};
                            tempFile['name'] = gallery.name;

                            galleryObj['pond'] = undefined;
                            galleryObj['file'] = tempFile;
                            galleryObj['src'] = undefined;
                            galleryObj['url'] = gallery.url;
                            galleryObj['galleryId'] = undefined;
                            galleryObj['galleryInputId'] = undefined;

                            addMoreGalleryPic(galleryObj,index);
                        }
                    }

                    populateOriPlans(oriItiId,iniModalProgress);
                }else{//doc.exists == false
                    isEdit = false;
                }
            }).catch(function(error) {
                isEdit = false;
                myProgress += 50;
                updateProgressBar(myProgress,iniModalProgress);

                showAlert("Error getting itinerary: " + error);
                console.log("Error getting document:", error);
            });
    }

    function populateOriPlans(oriItiId,iniModalProgress){
        db.collection(collectionName).doc(oriItiId).collection("plan").orderBy("day").orderBy("startTime").get()
            .then(function(querySnapshot) {
                myProgress += 50;
                updateProgressBar(myProgress,iniModalProgress);

                if(querySnapshot != null && !querySnapshot.isEmpty) {
                    let currentDay = 0;
                    let inBetweenDaysEmptyPlanCount = 0;

                    for(let i = 0; i < querySnapshot.docs.length; i++){
                        let plan = querySnapshot.docs[i].data();

                        let currentLoopDay = plan.day; //current loop day need to + 1 because the html one they start from 1

                        if(i === 0 && plan.day >= 1){
                            inBetweenDaysEmptyPlanCount = 1;
                        }
                        console.log(inBetweenDaysEmptyPlanCount);

                        if(currentDay === currentLoopDay){
                            if(i > 0){ //because the first plan already initialized
                                btnAddPlan();
                            }
                        }else{
                            if(currentLoopDay - currentDay > 1){
                                inBetweenDaysEmptyPlanCount += currentLoopDay - currentDay - 1;
                            }
                            confirmAddNewDay(currentLoopDay + 1);

                            currentDay = currentLoopDay;
                        }

                        //here i+1 because the first plan already populated by itinerary
                        let currentPlanIndex = i + 1 + inBetweenDaysEmptyPlanCount; // the in between is because there might be empty plans between the previous day and current day
                        let activityTimeElem = $('#activityTime-'+ currentPlanIndex.toString() +'-day-' + (currentLoopDay + 1).toString());
                        let date = new Date(plan.startTime);

                        let hour = getHour(date);
                        let minute = getMinute(date);
                        let meridian = "AM";
                        if(parseInt(getHour(date)) >= 12){
                            meridian = "PM";
                            hour = format_two_digits(parseInt(hour) - 12);
                        }

                        if(hour === "00"){
                            hour = "12";
                        }

                        activityTimeElem.val(hour + ":" + minute + " " + meridian);
                        activityTimeElem.attr("data-timepicki-tim", hour);
                        activityTimeElem.attr("data-timepicki-mini", minute);
                        activityTimeElem.attr("data-timepicki-meri", meridian);
                        $('#activityName-'+ currentPlanIndex.toString() +'-day-' + (currentLoopDay + 1).toString()).val(plan.activityName);
                        if(plan.placeAddress && plan.placeAddress.placeName){
                            $('#location-'+ currentPlanIndex.toString() +'-day-' + (currentLoopDay + 1).toString()).val(plan.placeAddress.placeName);
                        }

                        allPlans[currentPlanIndex]['placeAddress'] = plan.placeAddress;
                    }
                }

                if(isQuotationRequested){
                    confirmAddNewDay(itinerary['noofDays']);
                }

            }).catch(function(error) {
            myProgress += 50;
            updateProgressBar(myProgress,iniModalProgress);

            showAlert("Error getting itinerary: " + error);
            console.log("Error getting document:", error);
        });
    }

    function getHour(date){
        return format_two_digits(date.getHours());
    }

    function getMinute(date){
        return format_two_digits(date.getMinutes());
    }

    function getSecond(date){
        return format_two_digits(date.getSeconds());
    }

    function format_two_digits(n) {
        return n < 10 ? '0' + n : n;
    }

    function updateProgressBar(myProgress,iniModalProgress){
        console.log(myProgress);
        $("#progress-bar").progressbar({
            value: myProgress
        });
        $('#progress-text').text(myProgress.toFixed(0) + "%");

        if(myProgress === 100 || myProgress >= 100){
            iniModalProgress.close();
            openSnackbar("Done fetching itinerary details");
        }
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
    //***********************************************THIS IS FOR EDIT FEATURE END********************************************/

    function openSnackbar(message) {
        var x = document.getElementById("snackbar");
        x.innerText = message;
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }


    //To check the current user is admin or not, put at the last paragraph will be better
    authAdmin();

</script>

<?php include 'footer.php'; ?>
</body>
</html>
